/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.enums;

/**
 *
 * @author Hi_XV
 */
public enum StatusOrder {
    Ordered("Đã đặt hàng"), 
    Shipping("Đang chuyển hàng"), Payment("Thanh toán"), 
    Return("Trở về ban đầu"), Done("Hoàn thành"), Cancel("Hủy bỏ");
    
    private String statusOrder;

    private StatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }
    

    public String getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }
    
}
