/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.enums;

/**
 *
 * @author Hi_XV
 */
public enum Gender {
    Man("Nam"), Women("Nữ");
    
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private Gender(String gender) {
        this.gender = gender;
    }
    
    
}
