/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.common;

/**
 *
 * @author HOANGBAO
 */
public class AjaxProduct {
    private int id;
    private String image;
    private String name;
    private String price;
    private String pricePromotion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPricePromotion() {
        return pricePromotion;
    }

    public void setPricePromotion(String pricePromotion) {
        this.pricePromotion = pricePromotion;
    }

    public AjaxProduct() {
    }

    public AjaxProduct(int id, String image, String name, String price, String pricePromotion) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.price = price;
        this.pricePromotion = pricePromotion;
    }
    
    
}
