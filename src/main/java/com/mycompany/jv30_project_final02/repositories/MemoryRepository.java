/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.MemoryEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface MemoryRepository extends CrudRepository<MemoryEntity, Integer>{
    public MemoryEntity findByMemory(String Memory);
    
    // bảo làm
    public MemoryEntity findByProductDetail(ProductDetailEntity productDetail);
    
    @Query(value = "select * from memory m inner join product_detail pd on m.id = pd.memory_id\n" +
        "where pd.product_id = ?1 \n" +
        "group by m.memory",nativeQuery = true)
    public List<MemoryEntity> findByProductId(int productId);
}
