/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;


import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Integer>{
    public ProductEntity findByName(String Name);
    
    
    
    @Query(value = "Select product_id from product_promotion pp where pp.promotion_id = ?1 ", nativeQuery = true)
    public List<Integer> FindListProductId(int promotionId);
    
    @Query("Select p From ProductEntity p where " 
            + "p.name like %?1% or p.screen like %?2% or p.operatingSystem like %?3% or p.rearCamera like %?4% "
            + "or p.frontCamera like %?5%" 
            + "or p.batteryCapacity like %?6%"
            + "or p.ram like %?7%")
    public List<ProductEntity> findProductNameOrScreenOrOperatingSystemOrRearCameraOrFrontCameraOrBatteryCapacityOrRam(
            String name, String screen, String operatingSystem, String rearCamera, String frontCamera, String batteryCapacity, String ram);
    public List<ProductEntity> findByNameOrScreenOrOperatingSystemOrRearCameraOrFrontCameraOrBatteryCapacityOrRam(
    String name, String screen, String operatingSystem, String rearCamera, String frontCamera, String batteryCapacity, String ram);
    //bảo làm
    @Query(value = "select * from product \n" +
        "limit 0,10",nativeQuery = true)
    public List<ProductEntity> findByProductLimit();
    
    public ProductEntity findByProductDetail(ProductDetailEntity productDetail);
    
    List<ProductEntity> findByNameContainingAndStatus(String name,int status);
    
    List<ProductEntity> findByProducerAndStatus(ProducerEntity producer,int status);
    
        @Query(value = "SELECT DISTINCT *\n" +
"	FROM product pd JOIN product_promotion ppr ON pd.id = ppr.product_id \n" +
"	JOIN promotion pr ON ppr.promotion_id = pr.id \n" +
"	WHERE (NOW() BETWEEN pr.start_date AND pr.end_date) ORDER BY pr.start_date DESC", nativeQuery = true)
    public List<ProductEntity> findListProductPromotion();    
    
}
