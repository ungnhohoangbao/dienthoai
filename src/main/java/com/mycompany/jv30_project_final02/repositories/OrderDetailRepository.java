/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.OrderDetailEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.enums.StatusOrder;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface OrderDetailRepository extends CrudRepository<OrderDetailEntity, Integer>{
    public List<OrderDetailEntity> findByOrderId(int orderId);
    
    @Query(value = "Select product_id from orderdetail od where od.id = ?1 ", nativeQuery = true)
    public int findProductId(int orderdetailId);
    
    //bảo làm
     List<OrderDetailEntity> findByOrder(OrdersEntity orders);
    
    @Query("select odt from OrderDetailEntity odt where odt.product = ?1 and odt.order.customer.account = ?2 and odt.order.status like ?3")
    List<OrderDetailEntity> findByProductAndAccountAndStatusOrder(ProductEntity product, AccountEntity account,StatusOrder statusOrder);
    
    @Query(value="select odt.product_id from orderdetail odt \n" +
        " group by odt.product_id limit 0,10",nativeQuery = true)
    List<Integer> findIdProduct();
}
