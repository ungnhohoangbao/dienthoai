/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author AnhLe
 */
@Repository
public interface AccountRepository extends
        CrudRepository<AccountEntity, Integer> {

    AccountEntity findByEmailLikeAndPasswordLike(
            String email, String password);

    @Query("Select acc From AccountEntity acc "
            + "Join fetch acc.accountRoles "
            + "Where acc.email Like ?1 and "
            + "acc.password Like ?2")
    AccountEntity findAccountByEmailAndPassword(
            String email, String password);
    
    @Query(value = "Select acc_role_id from acc_role_relationship pp where pp.acc_id = ?1 ", nativeQuery = true)
    List<Integer> ListAccRoleId(int accountId);
    
    AccountEntity findByEmail(String email);
     @Query("select acc from AccountEntity acc where acc.name  Like %?1% or acc.email Like %?1% or acc.phoneNumber Like %?1%")
    public List<AccountEntity> findByName(String searchText);
}
