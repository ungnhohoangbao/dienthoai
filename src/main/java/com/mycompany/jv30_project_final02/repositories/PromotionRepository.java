/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.PromotionEntity;
import java.util.Set;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface PromotionRepository extends CrudRepository<PromotionEntity, Integer>{
    @Query(value = "Delete from product_promotion pp where pp.promotion_id = ?1 and pp.product_id = ?2 ", nativeQuery = true)
    public void DeleteProductPromotion(int promotionId, int productId);
    
    @Query(value = "Select promotion_id from product_promotion pp where pp.product_id = ?1 ", nativeQuery = true)
    public int findPromotionId(int productId);
    
    @Query(value = "select count(*) from promotion where discount > 0", nativeQuery = true)
    public Integer findSumPromotion();
    //bảo làm
    @Query(value = "SELECT * from product_promotion ppr inner join promotion pr on ppr.promotion_id = pr.id\n" +
"		INNER JOIN product p on p.id = ppr.product_id\n" +
"		WHERE p.id =?1 \n" +
"		AND (NOW() BETWEEN pr.start_date AND pr.end_date) ",nativeQuery = true)
    Set<PromotionEntity> findByProductIdAndDateNow(int productId);
}
