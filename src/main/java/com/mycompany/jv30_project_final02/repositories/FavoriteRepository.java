/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.FavoriteEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface FavoriteRepository extends CrudRepository<FavoriteEntity, Integer>{
    FavoriteEntity findByAccountAndProduct(AccountEntity account, ProductEntity product);
    
    List<FavoriteEntity> findByAccount(AccountEntity account);
    
}
