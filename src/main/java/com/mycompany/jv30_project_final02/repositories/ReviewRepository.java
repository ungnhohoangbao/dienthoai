/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.ReviewEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface ReviewRepository extends CrudRepository<ReviewEntity, Integer>{
    public int findByProductId(int ReviewId);
    
    @Query(value = "Select product_id from review rr where rr.id = ?1 ", nativeQuery = true)
    public int findProductId(int ReviewId);
    
    @Query(value = "Select account_Id from review rr where rr.id = ?1 ", nativeQuery = true)
    public int findAccountId(int ReviewId);
    
    @Query(value = "select a.content, a.id , a.review_date, b.name as name_account, c.name as name_product " +
                  "from review a " +
                  "inner join account b on a.account_Id = b.id " +
                  "inner join product c on a.product_id = c.id " +
                  "where c.name like ?1 " +
                  "or b.name like ?1 ", nativeQuery = true)
    public List<ReviewEntity> searchReview(String searchTxt);
    
    @Query(value = "Select r From ReviewEntity r where r.account.name like %?1% "
            + "or r.product.name like %?1% ")
    public List<ReviewEntity> searchReviewByAccNameOrProductName(String searchTxt);
    
    @Query(value = "select count(*) from review where content != ''", nativeQuery = true)
    public Integer findSumReview();
    //bảo làm
    List<ReviewEntity> findByProductOrderByIdDesc(ProductEntity product);
    
    List<ReviewEntity> findByProduct(ProductEntity product);
}
