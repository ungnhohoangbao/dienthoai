/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.ColorEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface ColorRepository extends CrudRepository<ColorEntity, Integer>{
    public ColorEntity findByColor(String Color);
    
    public ColorEntity findByProductDetail(ProductDetailEntity productDetail);
    
}
