/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.ImageEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface ImagesRepository extends CrudRepository<ImageEntity, Integer>{
    public List<ImageEntity> findByProductId(int productId);
    // bảo làm
    public List<ImageEntity> findByProduct(ProductEntity product);
}
