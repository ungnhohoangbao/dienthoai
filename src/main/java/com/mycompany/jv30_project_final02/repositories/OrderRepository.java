/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.enums.StatusOrder;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface OrderRepository extends CrudRepository<OrdersEntity, Integer>{
    @Query(value = "Select customer_id from orders od where od.id = ?1 ", nativeQuery = true)
    public int findCustomerId(int orderId);
    
   
    @Query(value = "Select a.orderDate, a.totalPrice, a.status, b.name from orders a "
            + "inner join customer b on a.customer_id = b.id"
            + "where a.orderDate like ?1 "
            + "or b.name like ?1 ", nativeQuery = true)
    public List<OrdersEntity> findSearchOrder(String searchText);
    
    public List<OrdersEntity> findByOrderDateBetween(Date date1, Date date2);
    
    @Query(value = "select count(*) from orders where status = 'Done'", nativeQuery = true)
    public Integer findSumOrderDone();
    
    @Query(value = "select  sum(totalPrice) " 
            + "from orders where status = 'Done' GROUP BY status" ,nativeQuery =  true)
    public double findSumPriceOrder();
    @Query(value = "select * from orders o inner join customer c on o.customer_id = c.id\n" +
        "where c.account_id =?1", nativeQuery = true)
    List<OrdersEntity> findByAccountId(int accountId);
    
    @Query("select o from OrdersEntity o where o.orderDate between ?1 and ?2 and o.customer.account =?3 ")
    List<OrdersEntity> findByOrderDateBetweenAndAccount(Date startDate, Date endDate,AccountEntity account);
    
    @Query("select o from OrdersEntity o where o.status like ?1 and o.customer.account =?2 ")
    List<OrdersEntity> findByStatusAndAccount(StatusOrder status,AccountEntity account);
    
    @Query("select o from OrdersEntity o where o.orderDate between ?1 and ?2 and o.customer.account =?3 and o.status like ?4")
    List<OrdersEntity> findByOrderDateBetweenAndAccountAndStatus(Date startDate, Date endDate,AccountEntity account,StatusOrder status);
}
