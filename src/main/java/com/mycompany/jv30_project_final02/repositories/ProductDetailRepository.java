/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.repositories;

import com.mycompany.jv30_project_final02.entities.MemoryEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Hi_XV
 */
@Repository
public interface ProductDetailRepository extends CrudRepository<ProductDetailEntity, Integer>{
    public List<ProductDetailEntity> findByProductId(int productId);
    
    @Query("Select s From ProductDetailEntity s where" 
            + " s.product.id Like ?1")
    public List<ProductDetailEntity> findProductId(int productId);
    
    @Query(value = "select * from product_detail pd inner join product p on pd.product_id = p.id\n" +
    "   where p.status = 1 and pd.quantity_in_store>0     " +
             "group by product_id\n" +
    "        order by date_create ASC \n" +
    "        limit 0,10",nativeQuery = true)
    List<ProductDetailEntity> findProductDetailLimit();
    
    List<ProductDetailEntity> findByProduct(ProductEntity product);
    
    List<ProductDetailEntity> findByMemoryAndProduct(MemoryEntity memory,ProductEntity product);
    
    @Query(value = "select * from product_detail pd inner join product p on pd.product_id = p.id\n" +
        "where p.status = 1 and pd.quantity_in_store > 0  and p.producer_id =?1  \n" +
        "group by product_id\n" +
        "order by date_create desc\n" +
        "limit 0,6",nativeQuery = true)
    List<ProductDetailEntity> findByProducerIdLimit(int producerId);
    
    @Query(value = "select * from product_detail pd inner join memory m on pd.memory_id = m.id\n" +
        "where pd.product_id =?1\n" +
        "group by m.memory",nativeQuery = true)
    List<ProductDetailEntity> findByProductIdGroupMemory(int productId);
    

}
