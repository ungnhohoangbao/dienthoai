/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.entities;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Hi_XV
 */
@Entity
@Table(name = "orderDetail")
public class OrderDetailEntity implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    private int quantity;
    
    private double price;
    
    private String memory;
    
    @Column(nullable = false)
    private String color;
    
    @ManyToOne
    @JoinColumn(name = "order_id")
    private OrdersEntity order;
    
    @ManyToOne
    @JoinColumn(name = "product_id")
    private ProductEntity product;

    public OrderDetailEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public OrdersEntity getOrder() {
        return order;
    }

    public void setOrder(OrdersEntity order) {
        this.order = order;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }
    
//    public String getPriceFormatted(){
//        NumberFormat currency = NumberFormat.getCurrencyInstance();
//        String priceFormatted = currency.format(price);
//        return priceFormatted;
//    }
    public String getPriceFormatted(){
        Locale localeVN = new Locale("vi", "VN");
        NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
        String priceFormatted = currency.format(price);
        return priceFormatted;
    }
    public String getTotalPriceFormatted(){
        double totalPrice = price * quantity;
        Locale localeVN = new Locale("vi", "VN");
        NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
        String priceFormatted = currency.format(totalPrice);
        return priceFormatted;
    }
    public double totalPriceOrderDetail(){
        double totalPrice = price * quantity;
        return totalPrice;
    }
}
