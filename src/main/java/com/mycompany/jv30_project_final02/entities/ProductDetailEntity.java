/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.entities;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author HOANGBAO
 */
@Entity
@Table(name ="product_detail")
public class ProductDetailEntity implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @ManyToOne      
    @JoinColumn(name ="product_id")
    private ProductEntity product;
    
    @ManyToOne      
    @JoinColumn(name ="color_id")
    private ColorEntity color;
    
    @ManyToOne      
    @JoinColumn(name ="memory_id")
    private MemoryEntity memory;

    private Double price;
    
    
    private String image;
    
    @Column(name = "quantity_in_store")
    private int quantityInStore;
    
    public ProductDetailEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    public ColorEntity getColor() {
        return color;
    }

    public void setColor(ColorEntity color) {
        this.color = color;
    }

    public MemoryEntity getMemory() {
        return memory;
    }

    public void setMemory(MemoryEntity memory) {
        this.memory = memory;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantityInStore() {
        return quantityInStore;
    }

    public void setQuantityInStore(int quantityInStore) {
        this.quantityInStore = quantityInStore;
    }
    
    public String getPriceFormatted(){
        Locale localeVN = new Locale("vi", "VN");
        NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
        String priceFormatted = currency.format(price);
        return priceFormatted;
    }
//    public String getPriceFormatted(){
////        Locale localeVN = new Locale("vi", "VN");
////        NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
//        DecimalFormat formatter = new DecimalFormat("###,###,###");
//        String priceFormatted = (formatter.format(price) + " đ");
//        return priceFormatted;
//    }
     public String getDiscount(){
         double priceDiscount = price - (price * ((product.totalPromotion() * 1.0)/100));
         Locale localeVN = new Locale("vi", "VN");
          NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
//        DecimalFormat formatter = new DecimalFormat("###,###,###");
//        String priceFormatted = (formatter.format(priceDiscount) + " đ");
          String priceFormatted = currency.format(priceDiscount);
        return priceFormatted;
    }
}
