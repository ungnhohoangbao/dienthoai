/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.entities;

import com.mycompany.jv30_project_final02.enums.StatusOrder;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Hi_XV
 */
@Entity
@Table(name = "Orders")
public class OrdersEntity implements Serializable, Comparable<OrdersEntity>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "orderDate")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date orderDate = new Date();
    
    private double totalPrice;
    
    @Enumerated(EnumType.STRING)
    private StatusOrder status = StatusOrder.Ordered; 
    
    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<OrderDetailEntity> orderDetails;
    
    @ManyToOne 
    @JoinColumn(name ="customer_id")
    private CustomerEntity customer;
    
    public OrdersEntity() {
    }

    public List<OrderDetailEntity> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetailEntity> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }
   
    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public StatusOrder getStatus() {
        return status;
    }

    public void setStatus(StatusOrder status) {
        this.status = status;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }
    
    public String getTotalPriceFormatted(){
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        String priceFormatted = currency.format(totalPrice);
        return priceFormatted;
    }
    
    public String getOrderDateFormatted(){
            SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
        return dt1.format(orderDate);
    }
    public String totalPriceOrder(){
        double totalPriceOrder = 0;
        for (OrderDetailEntity orderDetail : orderDetails) {
        totalPriceOrder = totalPriceOrder + orderDetail.totalPriceOrderDetail();
        }
        Locale localeVN = new Locale("vi", "VN");
        NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
        String priceFormatted = currency.format(totalPriceOrder);
        return priceFormatted;
        
    }
    public String getTotalPriceFormatted02(){
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        String priceFormatted = currency.format(totalPrice);
        return priceFormatted;
    }
    public int compareTo(OrdersEntity order) {
            if(getOrderDate() == null || order.getOrderDate() == null)
                return 0;
            return getOrderDate().compareTo(order.getOrderDate());
	}
}
