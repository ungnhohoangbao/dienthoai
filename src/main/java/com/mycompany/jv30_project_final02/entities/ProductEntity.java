/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author HOANGBAO
 */
@Entity
@Table(name ="product")
public class ProductEntity implements Serializable, Comparable<ProductEntity>{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(length = 100,nullable = false)
    @NotBlank(message = "Không được bỏ trống")
    @Size(max=100,message = "Tên không được quá 100")
    private String name;
    
    @Column(nullable = false,columnDefinition = "text CHARACTER SET 'utf8'")
    @NotNull(message = "Không được bỏ trống")
    private String content;
    
    private int status =1;
    
    @Column(length = 100)
    private String screen;
    
    @Column(length = 30,name = "operating_system")
    private String operatingSystem;
    
    @Column(name = "rear_camera") 
    private String rearCamera;
    
    @Column(length = 100, name = "front_camera")
    private String frontCamera;
    
    @Column(length = 50, name = "battery_capacity")
    private String batteryCapacity;
    
    @Column(length = 10)
    private String ram;
    
    @Column(name = "date_create")
    @Temporal(TemporalType.DATE)
    private Date dateCreate = new Date();
    
    @ManyToOne      
    @JoinColumn(name ="producer_id")
    private ProducerEntity producer;
    
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                CascadeType.PERSIST,
                CascadeType.MERGE
            }, mappedBy = "product")
    Set<PromotionEntity> promotion = new HashSet<>();
    
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ImageEntity> image;
    
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ProductDetailEntity> productDetail;
    
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<OrderDetailEntity> orderDetail;
    
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<FavoriteEntity> favorite;
    
    @OneToMany(mappedBy = "product",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ReviewEntity> review;

    public ProductEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public String getRearCamera() {
        return rearCamera;
    }

    public void setRearCamera(String rearCamera) {
        this.rearCamera = rearCamera;
    }

    public String getFrontCamera() {
        return frontCamera;
    }

    public void setFrontCamera(String frontCamera) {
        this.frontCamera = frontCamera;
    }

    public String getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(String batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public ProducerEntity getProducer() {
        return producer;
    }

    public void setProducer(ProducerEntity producer) {
        this.producer = producer;
    }

    public Set<PromotionEntity> getPromotion() {
        return promotion;
    }

    public void setPromotion(Set<PromotionEntity> promotion) {
        this.promotion = promotion;
    }

    public List<ImageEntity> getImage() {
        return image;
    }

    public void setImage(List<ImageEntity> image) {
        this.image = image;
    }

    public List<ProductDetailEntity> getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(List<ProductDetailEntity> productDetail) {
        this.productDetail = productDetail;
    }

    public List<OrderDetailEntity> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(List<OrderDetailEntity> orderDetail) {
        this.orderDetail = orderDetail;
    }
 
    public List<FavoriteEntity> getFavorite() {
        return favorite;
    }

    public void setFavorite(List<FavoriteEntity> favorite) {
        this.favorite = favorite;
    }

    public List<ReviewEntity> getReview() {
        return review;
    }

    public void setReview(List<ReviewEntity> review) {
        this.review = review;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    public int totalPromotion(){
        int sum = 0;
        for (PromotionEntity promotionEntity : promotion) {
            
            sum = sum + promotionEntity.getDiscount();
        }
        return sum;
    }
    public int compareTo(ProductEntity product) {
            if(getDateCreate() == null || product.getDateCreate() == null)
                return 0;
            return getDateCreate().compareTo(product.getDateCreate());
	}

    
    
}
