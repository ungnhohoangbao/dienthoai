/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.AccountRoleEntity;
import com.mycompany.jv30_project_final02.enums.AccountRole;
import com.mycompany.jv30_project_final02.enums.StatusAccount;
import com.mycompany.jv30_project_final02.repositories.AccountRepository;
import com.mycompany.jv30_project_final02.repositories.AccountRoleRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author AnhLe
 */
@Service
public class AccountService {
    @Autowired
    private AccountRoleService accountRoleService;
    
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private AccountRoleRepository accountRoleRepository;

    public AccountEntity
            findAccountByEmailAndPassword(String email, String password) {
        return accountRepository.findAccountByEmailAndPassword(email, password);
    }
    public AccountEntity findAccount(int accountId){
        return accountRepository.findOne(accountId);
    }
    public List<AccountEntity> getAccounts(){
        return (List<AccountEntity>)accountRepository.findAll();
    }
    public AccountEntity saveAccountStatus(AccountEntity account,StatusAccount status ){
        account.setStatus(status);
        return accountRepository.save(account);  
    }
    public AccountEntity saveAccount(AccountEntity account){
        return accountRepository.save(account);
    }
    public AccountEntity saveListAccountRole(int accountId, List<AccountRole> listAccountRole){
        AccountEntity account = findAccount(accountId);
        List<AccountEntity> accounts = new ArrayList<>();
        accounts.add(account);
        List<AccountRoleEntity> accountRoles = new ArrayList<>();
        for (AccountRole accountRoleEmun : listAccountRole) {
            AccountRoleEntity accountRole = new AccountRoleEntity();
            accountRole.setRole(accountRoleEmun);
            accountRole.setAccounts(accounts);
            accountRoleRepository.save(accountRole);
            accountRoles.add(accountRole);
        }
        account.setAccountRoles(accountRoles);
        return accountRepository.save(account);
    }
    public List<AccountRoleEntity> getAccountRole(int accountId){
        List<Integer> ListAccountRoleId = accountRepository.ListAccRoleId(accountId);
        List<AccountRoleEntity> accountRoles = new ArrayList<>();
        for (Integer integer : ListAccountRoleId) {
            AccountRoleEntity accountRole = accountRoleRepository.findOne(integer);
            accountRoles.add(accountRole);
        }
        return accountRoles;
    }
     public AccountEntity findByEmail(String email){
        return accountRepository.findByEmail(email);
    }
     public AccountEntity registerAccount(AccountEntity account) {
        account = save(account);
        if(account !=null && account.getId()>0){
            List<AccountRoleEntity> role = accountRoleService.getAll();
            List<AccountRoleEntity> accRole = new ArrayList<>();
            for (AccountRoleEntity acr : role) {
                if (acr.getRole() == AccountRole.ROLE_USER) {
                    accRole.add(acr);
                }
            }
            List<AccountEntity> accounts = new ArrayList<>();
            accounts.add(account);
            accRole.get(0).setAccounts(accounts);
            accounts.get(0).setAccountRoles(accRole);
            account = save(accounts.get(0));
            if(account != null && account.getId()>0){
                return account;
            }
            return new AccountEntity();
        }
        return new AccountEntity();
    } 
      public AccountEntity save(AccountEntity account) {
        return accountRepository.save(account);
    }
      public AccountEntity findById(int id) {
        AccountEntity account = accountRepository.findOne(id);
        if (account != null && account.getId() > 0) {
            return account;
        } else {
            return new AccountEntity();
        }
    }
       public List<AccountEntity> searchAccount(String searchText){
            return (List<AccountEntity>)
                   accountRepository.findByName(searchText);
      }
}
