/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.AccountRoleEntity;
import com.mycompany.jv30_project_final02.repositories.AccountRoleRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author HOANGBAO
 */
@Service
public class AccountRoleService {
    
    @Autowired
    private AccountRoleRepository accountRoleRepository;
    
    public List<AccountRoleEntity> getAll(){
        List<AccountRoleEntity> role = (List<AccountRoleEntity>) accountRoleRepository.findAll();
        if(role !=null && role.size() >0){
            return role;
        }
        else{
            return new ArrayList<>();
        }
    }
}
