/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.PromotionEntity;
import com.mycompany.jv30_project_final02.repositories.ProductRepository;
import com.mycompany.jv30_project_final02.repositories.PromotionRepository;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hi_XV
 */
@Service
public class PromotionService {
    @Autowired
    PromotionRepository promotionRepository;
    
    @Autowired
    ProductRepository productRepository;
    
    public int findSumPromotion(){
       return promotionRepository.findSumPromotion();
    }
    public List<PromotionEntity> getPromotion(){
        return (List<PromotionEntity>)promotionRepository.findAll();
    }
    public PromotionEntity findPromotion(int promotionId){
        PromotionEntity promotion = promotionRepository.findOne(promotionId);
        return promotion;
    }
    public PromotionEntity savePromotion(PromotionEntity promotion, List<String> files){
        
        for (String file : files) {
            promotion.setImage(file);
        }
        promotionRepository.save(promotion);
        return promotion;
    }
    public PromotionEntity savePromotionProduct(PromotionEntity promotion, Set<ProductEntity> products){
        
        Set<PromotionEntity> promotions = new HashSet<>();
        promotions.add(promotion);
        promotion.setProduct(products);
        for (ProductEntity product : products) {
            product.setPromotion(promotions);
            productRepository.save(product);
        }
        return promotionRepository.save(promotion);
    }
    public void deleteProductPromotion(int promotionId, int productId){
        promotionRepository.DeleteProductPromotion(promotionId, productId);
    }
    public int findPromotionId(int productId){
        return promotionRepository.findPromotionId(productId);
    }
    
     public Set<PromotionEntity> findByProductIdAndDateNow(int productId){
        Set<PromotionEntity> listPromotion  = promotionRepository.findByProductIdAndDateNow(productId);
        if(listPromotion != null && listPromotion.size()>0){
            return listPromotion;
        }
        else{
            return new HashSet<>();
        }
    }
}
