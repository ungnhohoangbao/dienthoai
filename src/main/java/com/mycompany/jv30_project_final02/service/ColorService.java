/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.ColorEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.repositories.ColorRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class ColorService {
    @Autowired
    ColorRepository colorRepository;
    
    public List<ColorEntity> getColor(){
        return (List<ColorEntity>)colorRepository.findAll();
    }
    public void deleteColor(int colorId){
        colorRepository.delete(colorId);
    }
    public void saveColor(ColorEntity color){
        colorRepository.save(color);
    }
    public List<ColorEntity> findColor(List<String> colors){
        List<ColorEntity> colors01 = new ArrayList<>();
        for (String color : colors) {
            ColorEntity color01 = colorRepository.findByColor(color);
            colors01.add(color01);
        }
        return colors01;
    }
    public ColorEntity findByProductDetail(ProductDetailEntity productDetail){
        return colorRepository.findByProductDetail(productDetail);
    }
}
