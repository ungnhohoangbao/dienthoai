/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.ImageEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.repositories.ImagesRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class ImagesService {
    @Autowired
    ImagesRepository imagesRepository;
    
     public List<ImageEntity> getProductId(int productId){
        return imagesRepository.findByProductId(productId);
    }
     public ImageEntity findImage(int imageId){
         return imagesRepository.findOne(imageId);
     }
     public void deleteImage(int imageId){
         imagesRepository.delete(imageId);
     }
      public List<ImageEntity> findByProduct(ProductEntity product){
        return (List<ImageEntity>)imagesRepository.findByProduct(product);
    }
}
