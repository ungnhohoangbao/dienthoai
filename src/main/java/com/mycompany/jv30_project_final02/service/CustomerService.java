/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.CustomerEntity;
import com.mycompany.jv30_project_final02.entities.OrderDetailEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.repositories.CustomerRepository;
import com.mycompany.jv30_project_final02.repositories.OrderRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hi_XV
 */
@Service
public class CustomerService {
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderDetailService orderDetailService;
    
//    @Transactional(rollbackFor = Exception.class)
//     public OrdersEntity saveOrder(CustomerEntity customer, OrdersEntity order){
//        OrdersEntity orderSave = new OrdersEntity();
//        List<OrderDetailEntity> orderDetails = new ArrayList<>();
//        List<OrdersEntity> orders = new ArrayList<>(); 
//         try {
//            customer = customerRepository.save(customer);
//            if(customer != null && customer.getId()>0){
//                order.setCustomer(customer);
//                order = orderRepository.save(order);
//                if(order!=null && order.getId()>0){
//                    List<OrderDetailEntity> odt = order.getOrderDetails();
//                    for(OrderDetailEntity o:odt){
//                        o.setOrder(order);
//                        orderDetailService.save(o);
//                        orderDetails.add(o); 
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            orderSave.setOrderDetails(orderDetails);
//            orderSave.setCustomer(customer);
//            orders.add(orderSave);
//        }
//        return order;
//    }
     
    @Transactional(rollbackFor = Exception.class)
     public OrdersEntity saveOrder(CustomerEntity customer, OrdersEntity order){
        List<OrderDetailEntity> orderDetails = new ArrayList<>();
        List<OrdersEntity> orders = new ArrayList<>(); 
         try {
            customer = customerRepository.save(customer);
            if(customer != null && customer.getId()>0){
                order.setCustomer(customer);
                order = orderRepository.save(order);
                if(order!=null && order.getId()>0){
                    List<OrderDetailEntity> odt = order.getOrderDetails();
                    for(OrderDetailEntity o:odt){
                        o.setOrder(order);
                        orderDetailService.save(o);
                        orderDetails.add(o); 
                    }
                }
            }
        } catch (Exception ex) {
            order.setOrderDetails(orderDetails);
            order.setCustomer(customer);
            orders.add(order);
        }
        return order;
    }
    
    public void deleteCustomer(CustomerEntity custemer) {
        customerRepository.delete(custemer);
    }
}
