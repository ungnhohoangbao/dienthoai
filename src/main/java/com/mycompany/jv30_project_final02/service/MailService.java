package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailService {

     @Autowired
    JavaMailSender mailSender;

    @Autowired
    TemplateEngine templateEngine;

    public void sendEmail(String subject, OrdersEntity order, String recipientEmail) throws MessagingException {
        Locale locale = LocaleContextHolder.getLocale();

        // Prepare the evaluation context
        Context ctx = new Context(locale);
        ctx.setVariable("order", order);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setTo(recipientEmail);

        // Create the HTML body using Thymeleaf
        String htmlContent = "";
        htmlContent = templateEngine.process("invoice.html", ctx);
        mimeMessageHelper.setText(htmlContent, true);
        // Send email
        mailSender.send(mimeMessage);
    }
    
    public void sendEmailConfirm(String subject, AccountEntity account, String recipientEmail) throws MessagingException {
        Locale locale = LocaleContextHolder.getLocale();

        // Prepare the evaluation context
        Context ctx = new Context(locale);
        ctx.setVariable("account", account);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setTo(recipientEmail);

        // Create the HTML body using Thymeleaf
        String htmlContent = "";
        htmlContent = templateEngine.process("confirm_register.html", ctx);
        mimeMessageHelper.setText(htmlContent, true);
        // Send email
        mailSender.send(mimeMessage);
    }
    
    public void cancleOrder(String subject, OrdersEntity order, String recipientEmail) throws MessagingException {
        Locale locale = LocaleContextHolder.getLocale();

        // Prepare the evaluation context
        Context ctx = new Context(locale);
        ctx.setVariable("order", order);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setTo(recipientEmail);

        // Create the HTML body using Thymeleaf
        String htmlContent = "";
        htmlContent = templateEngine.process("cancle_order.html", ctx);
        mimeMessageHelper.setText(htmlContent, true);
        // Send email
        mailSender.send(mimeMessage);
    }
}
