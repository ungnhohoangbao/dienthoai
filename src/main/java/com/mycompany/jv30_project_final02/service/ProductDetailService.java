/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.MemoryEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.repositories.ProductDetailRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class ProductDetailService {
    
    @Autowired
    ProductDetailRepository productDetailRepository;
    
    public List<ProductDetailEntity> getProductId(int productId){
        return productDetailRepository.findByProductId(productId);
    }
    public void deleteProductDetail(int productDetailId){
        productDetailRepository.delete(productDetailId);
    }
    public ProductDetailEntity getProductDetail(int productDetailId){
        ProductDetailEntity productDetail = productDetailRepository.findOne(productDetailId);
        return productDetail;
    }
    public ProductDetailEntity addImage(ProductDetailEntity productDetail, List<String> urls){
        for (String url : urls) {
            productDetail.setImage(url);
        }
        return  productDetailRepository.save(productDetail);
        
    }
     public List<ProductDetailEntity> getAll(){
        return (List<ProductDetailEntity>) productDetailRepository.findAll();
    }
    
    public ProductDetailEntity findById(int id){
        return productDetailRepository.findOne(id);
    }
    
    public List<ProductDetailEntity> findProductDetailLimit(){
        return (List<ProductDetailEntity>) productDetailRepository.findProductDetailLimit();
    }
    
    public List<ProductDetailEntity> finByProduct(ProductEntity product){
        return (List<ProductDetailEntity>) productDetailRepository.findByProduct(product);
    }
    
    public List<ProductDetailEntity> finByMemoryAndProduct(MemoryEntity memory,ProductEntity product){
        return (List<ProductDetailEntity>) productDetailRepository.findByMemoryAndProduct(memory,product);
    }
    
    public List<ProductDetailEntity> findByProducerIdLimit(int producerId){
        return (List<ProductDetailEntity>) productDetailRepository.findByProducerIdLimit(producerId);
    }
    
    public List<ProductDetailEntity> findByProductIdGroupMemory(int productId){
        return (List<ProductDetailEntity>) productDetailRepository.findByProductIdGroupMemory(productId);
    }
}
