/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.FavoriteEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.repositories.FavoriteRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class FavoriteService {
    //bảo làm
    @Autowired
    private FavoriteRepository favoriteRepository;
    
    public FavoriteEntity findByAccountAndProduct(AccountEntity account, ProductEntity product){
        FavoriteEntity favorite = favoriteRepository.findByAccountAndProduct(account, product);
        if(favorite!=null && favorite.getId()>0){
            return favorite;
        }
        else{
            return new FavoriteEntity();
        }
    }
    
    public FavoriteEntity save(FavoriteEntity favorite){
        FavoriteEntity f =  favoriteRepository.save(favorite);
        if(f !=null && f.getId()>0){
            return f;
        }
        else{
            return new FavoriteEntity();
        }
    }
    public FavoriteEntity findById(int id){
        FavoriteEntity favorite = favoriteRepository.findOne(id);
        if(favorite!=null && favorite.getId()>0){
            return favorite;
        }
        else{
            return new FavoriteEntity();
        }
    }
    
    public boolean deleteById(int id){
        favoriteRepository.delete(id);
        if(!favoriteRepository.exists(id)){
            return true;
        }
        else{
            return false;
        }
    }
    
    public List<FavoriteEntity> findByAccount(AccountEntity account){
        List<FavoriteEntity> favorites = favoriteRepository.findByAccount(account);
        if(favorites !=null && favorites.size()>0){
            return favorites;
        }
        else{
            return new ArrayList<>();
        }
    }
}
