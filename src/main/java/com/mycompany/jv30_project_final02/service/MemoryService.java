/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.MemoryEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.repositories.MemoryRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class MemoryService {
    @Autowired
    MemoryRepository memoryRepository;
    
    public List<MemoryEntity> getMemory(){
        return (List<MemoryEntity>)memoryRepository.findAll();
    }
    public MemoryEntity findMemory(int memoryId){
        return memoryRepository.findOne(memoryId);
    }
    public MemoryEntity saveMemory(MemoryEntity memory){
        return memoryRepository.save(memory);
    }
    public MemoryEntity findMemory(String Memory){
        return memoryRepository.findByMemory(Memory);
    }
     public MemoryEntity findByProductDetail(ProductDetailEntity productDetailEntity){
        return memoryRepository.findByProductDetail(productDetailEntity);
    }
    
    public List<MemoryEntity> findByProductId(int productId){
        return (List<MemoryEntity>) memoryRepository.findByProductId(productId);
    }
}
