/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.repositories.ProducerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class ProducerService {
    @Autowired
    ProducerRepository producerRepository;
    public void saveProducer(ProducerEntity producer){
        producerRepository.save(producer);  
    }
    public List<ProducerEntity> getProducer(){
        return (List<ProducerEntity>)producerRepository.findAll();
    }
    public ProducerEntity getProducerId(int producerId){
        ProducerEntity producer = producerRepository.findOne(producerId);
        return producer;
    }
    public ProducerEntity findByProduct(ProductEntity product){
        return producerRepository.findByProduct(product);
    }
    
    public List<ProducerEntity> getAll(){
        return (List<ProducerEntity>) producerRepository.findAll();
    }
    
    public ProducerEntity findById(int id){
        return producerRepository.findOne(id);
    }
}
