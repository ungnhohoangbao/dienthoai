/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.CustomerEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.enums.StatusOrder;
import com.mycompany.jv30_project_final02.repositories.CustomerRepository;
import com.mycompany.jv30_project_final02.repositories.OrderRepository;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    
    @Autowired
    CustomerRepository customerRepository;
    
    public List<OrdersEntity> getOrders(){
        return (List<OrdersEntity>)orderRepository.findAll();
    }
    public OrdersEntity findOrder(int orderId){
        return orderRepository.findOne(orderId);
    }
    public OrdersEntity saveOrderStatus(OrdersEntity order, StatusOrder statusOrder){
        order.setStatus(statusOrder);
       return orderRepository.save(order);  
    }
    public CustomerEntity findCustomer(int orderId){
       return customerRepository.findOne(orderRepository.findCustomerId(orderId));
    }
    public List<OrdersEntity> ListOrdersDate(Date date1, Date date2){
        return orderRepository.findByOrderDateBetween(date1, date2);
    }
    public int findSumOrderDone(){
       return orderRepository.findSumOrderDone();
    }
    public double findPriceOrder(){
        return  orderRepository.findSumPriceOrder();
    }
    public OrdersEntity save(OrdersEntity order){
        return orderRepository.save(order);
    }
    
    public List<OrdersEntity> findByAccountId(int accountId){
        List<OrdersEntity> orders= orderRepository.findByAccountId(accountId);
        if(orders!=null && orders.size()>0){
            return orders;
        }
        else{
            return new ArrayList<>();
        }
    }
    
    public OrdersEntity findById(int id){
        OrdersEntity order = orderRepository.findOne(id);
        if(order != null && order.getId() >0){
            return order;
        }
        else{
            return new OrdersEntity();
        }
    }
    
    public List<OrdersEntity> findByOrderDateBetweenAndAccount(Date startDate, Date endDate,AccountEntity account){
        List<OrdersEntity> orders = orderRepository.findByOrderDateBetweenAndAccount(startDate, endDate, account);
        if(orders!=null && orders.size()>0){
            return orders;
        }
        else{
            return new ArrayList<>();
        }
    }
    
    public List<OrdersEntity> findByStatusAndAccount(StatusOrder status,AccountEntity account){
        List<OrdersEntity> orders= orderRepository.findByStatusAndAccount(status,account);
        if(orders!=null && orders.size()>0){
            return orders;
        }
        else{
            return new ArrayList<>();
        }
    }
    
    public List<OrdersEntity> findByOrderDateBetweenAndAccountAndStatus(Date startDate, Date endDate,AccountEntity account,StatusOrder status){
        List<OrdersEntity> orders = orderRepository.findByOrderDateBetweenAndAccountAndStatus(startDate, endDate, account, status);
        if(orders!=null && orders.size()>0){
            return orders;
        }
        else{
            return new ArrayList<>();
        }
    }
}
