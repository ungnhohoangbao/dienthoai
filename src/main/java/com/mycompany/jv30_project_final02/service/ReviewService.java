/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.ReviewEntity;
import com.mycompany.jv30_project_final02.repositories.ReviewRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class ReviewService {
    @Autowired
    ReviewRepository reviewRepository;
    
    public int findSumReview(){
        return reviewRepository.findSumReview();
    }
    
    public List<ReviewEntity> getReviews(){
        return (List<ReviewEntity>) reviewRepository.findAll();
    }
    public int findProductId(int reviewId){
        return reviewRepository.findProductId(reviewId);
    }
    public int findAccountId(int reviewId){
        return reviewRepository.findAccountId(reviewId);
    }
    public void deleteReview(int reviewId){
       reviewRepository.delete(reviewId);
    }
    public List<ReviewEntity> searchReview(String searchTxt){
        return reviewRepository.searchReviewByAccNameOrProductName("%"+searchTxt+"%");
    }
//     public List<ReviewEntity> searchReview(String searchTxt){
//        return reviewRepository.searchReviewByAccNameOrProductName(searchTxt);
//    }
    
    public ReviewEntity save(ReviewEntity review){
        review =  reviewRepository.save(review);
        if(review!=null && review.getId()>0){
            return review;
        }
        return new ReviewEntity();
    }
    
    public List<ReviewEntity> findByProductOrderByIdDesc(ProductEntity product){
        List<ReviewEntity> riviews = reviewRepository.findByProductOrderByIdDesc(product);
        if(riviews!=null && riviews.size()>0){
            return riviews;
        }
        else{
            return new ArrayList<>();
        }
    }
    
    public List<ReviewEntity> findByProduct(ProductEntity product){
        List<ReviewEntity> riviews = reviewRepository.findByProduct(product);
        if(riviews!=null && riviews.size()>0){
            return riviews;
        }
        else{
            return new ArrayList<>();
        }
    }
}
