/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.OrderDetailEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.enums.StatusOrder;
import com.mycompany.jv30_project_final02.repositories.OrderDetailRepository;
import com.mycompany.jv30_project_final02.repositories.ProductRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class OrderDetailService {
    @Autowired
    OrderDetailRepository orderDetailRepository;
    
    @Autowired
    ProductRepository productRepository;
    
    public List<OrderDetailEntity> findListOrderDetail(int orderId){
        return orderDetailRepository.findByOrderId(orderId);
    }
    public ProductEntity findProduct(int orderdetailId){
        return productRepository.findOne(orderDetailRepository.findProductId(orderdetailId));
    }
     public List<OrderDetailEntity> findByOrder(OrdersEntity orders){
        List<OrderDetailEntity> orderDetails = orderDetailRepository.findByOrder(orders);
        if(orderDetails !=null && orderDetails.size() >0){
            return orderDetails;
        }
        else{
            return new ArrayList<>();
        }
    }
    
    public List<OrderDetailEntity> findByProductAndAccountAndStatusOrder(ProductEntity product, AccountEntity account,StatusOrder statusOrder){
        List<OrderDetailEntity> orderDetails = orderDetailRepository.findByProductAndAccountAndStatusOrder(product, account, statusOrder);
        if(orderDetails!=null && orderDetails.size()>0){
            return orderDetails;
        }
        else{
            return new ArrayList<>();
        }
    }
    
    public OrderDetailEntity save(OrderDetailEntity odt){
        return orderDetailRepository.save(odt);
    }
    
    public List<Integer> findIdProduct(){
        List<Integer> listIdProduct = orderDetailRepository.findIdProduct();
        if(listIdProduct !=null){
            return listIdProduct;
        }
        else{
            return new ArrayList<>();
        }
    }
}
