/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.service;

import com.mycompany.jv30_project_final02.entities.ColorEntity;
import com.mycompany.jv30_project_final02.entities.ImageEntity;
import com.mycompany.jv30_project_final02.entities.MemoryEntity;
import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;

import com.mycompany.jv30_project_final02.repositories.ImagesRepository;
import com.mycompany.jv30_project_final02.repositories.ProductDetailRepository;
import com.mycompany.jv30_project_final02.repositories.ProductRepository;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hi_XV
 */
@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    
    @Autowired
    ImagesRepository imagesRepository;
    
    @Autowired
    ProductDetailRepository productDetailRepository;
            
    public List<ProductEntity> getProduct(){
       return (List<ProductEntity>) productRepository.findAll();
    }
    public ProductEntity getProductId(int productId){
        return productRepository.findOne(productId);
    }
     public ProductEntity saveProductId(ProductEntity product){
        return productRepository.save(product);
    }
    public ProductEntity saveProduct(ProductEntity product, 
             List<String> urls){
        productRepository.save(product);
       
        List<ImageEntity> images = new ArrayList<ImageEntity>();
        
        for (String url : urls) {
            ImageEntity image = new ImageEntity();
            image.setProduct(product);
            image.setName(url);
            imagesRepository.save(image);
            images.add(image);
        }
        product.setImage(images);
        
        return product;
    }
    public ProductEntity findProductId(int productId){
        ProductEntity product = productRepository.findOne(productId);
        return product;
    }
    public ProductEntity saveProductDetail(ProductEntity product,MemoryEntity memory,
            double price, List<ColorEntity> colors, int quantityInStore){
        List<ProductDetailEntity> productDetails = new ArrayList<>();
        for (ColorEntity color : colors) { 
            ProductDetailEntity productDetail = new ProductDetailEntity();
            productDetail.setProduct(product);
            productDetail.setMemory(memory);
            productDetail.setColor(color);
            productDetail.setPrice(price);
            productDetail.setQuantityInStore(quantityInStore);
            productDetailRepository.save(productDetail);
            productDetails.add(productDetail);
        }
        product.setProductDetail(productDetails);
        return product;
    }
   public ProductEntity saveImage(ProductEntity product, 
             List<String> urls){
        List<ImageEntity> images = new ArrayList<ImageEntity>();
        for (String url : urls) {
            ImageEntity image = new ImageEntity();
            image.setProduct(product);
            image.setName(url);
            imagesRepository.save(image);
            images.add(image);
        }
        product.setImage(images);
        return product;
    }
   public ProductEntity updateStatus(ProductEntity product, int Status){
       product.setStatus(Status);
      return productRepository.save(product);
   }
   public Set<ProductEntity> findNameProduct(List<String> nameProducts){
       Set<ProductEntity> products = new HashSet<>();
       for (String nameProduct : nameProducts) {
           ProductEntity product =  productRepository.findByName(nameProduct);
           products.add(product);
       }
       return products;
   }
   public Set<ProductEntity> findListProduct(List<Integer> ListProductId){
       Set<ProductEntity> products = new HashSet<>();
       for (Integer productId : ListProductId) {
           ProductEntity product =  productRepository.findOne(productId);
           products.add(product);
       }
       return products;
   }
  public Set<ProductEntity> findListProduct02(int promotionId){
       Set<ProductEntity> products = new HashSet<>();
       List<Integer> ListProductId = productRepository.FindListProductId(promotionId);
       for (Integer productId : ListProductId) {
           ProductEntity product =  productRepository.findOne(productId);
           products.add(product);
       }
       return products;
   }
  public List<ProductEntity> searchProduct(String search){
      return productRepository.findProductNameOrScreenOrOperatingSystemOrRearCameraOrFrontCameraOrBatteryCapacityOrRam(search, search, search, search, search, search, search);
  }
  public List<ProductEntity> findByProductLimit(){
        return (List<ProductEntity>) productRepository.findByProductLimit();
    }
    
    public ProductEntity findByProductDetail(ProductDetailEntity productDetail){
        return productRepository.findByProductDetail(productDetail);
    }
    
    public List<ProductEntity> searchByName(String search){
        return (List<ProductEntity>) productRepository.findByNameContainingAndStatus(search,1);
    }
    
    public List<ProductEntity> findByProducer(ProducerEntity producer){
        return (List<ProductEntity>) productRepository.findByProducerAndStatus(producer,1);
    }
    
    public List<ProductEntity> findListProductPromotion(){
        return (List<ProductEntity>) productRepository.findListProductPromotion();
    }
    public ProductEntity findById(int id){
        ProductEntity product = productRepository.findOne(id);
        if(product !=null && product.getId()>0){
            return product;
        }
        else{
            return new ProductEntity();
        }
    }
}
