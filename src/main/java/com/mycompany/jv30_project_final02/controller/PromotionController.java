/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.controller;

import com.mycompany.jv30_project_final02.entities.ImageEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.PromotionEntity;
import com.mycompany.jv30_project_final02.repositories.PromotionRepository;
import com.mycompany.jv30_project_final02.service.ImagesService;
import com.mycompany.jv30_project_final02.service.ProductDetailService;
import com.mycompany.jv30_project_final02.service.ProductService;
import com.mycompany.jv30_project_final02.service.PromotionService;
import com.mycompany.jv30_project_final02.utils.UpLoadFile;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hi_XV
 */
@Controller
@RequestMapping("/manager")
public class PromotionController {

    @Autowired
    PromotionService promotionService;
    @Autowired
    ServletContext servletContext;
    @Autowired
    ImagesService imagesService;
    @Autowired
    ProductService productService;
    @Autowired
    ProductDetailService productDetailService;
    @Autowired
    UpLoadFile upLoadFile;

    @RequestMapping("/homeListPromotion")
    public String homePromotion(Model model) {
        List<PromotionEntity> promotions = promotionService.getPromotion();
        model.addAttribute("promotions", promotions);
        return "manager/listPromotion";
    }

    @RequestMapping("/add-promotion")
    public String addPromotion(Model model) {
        model.addAttribute("promotion", new PromotionEntity());
        model.addAttribute("action", "add-promotion");
        return "manager/form-Promotion";
    }

    @RequestMapping(value = "/add-promotion", method = RequestMethod.POST)
    public String savePromotion(Model model, @Valid @ModelAttribute("promotion") PromotionEntity promotion, BindingResult result,
            @RequestParam("files") List<MultipartFile> files) {
        if (result.hasErrors()) {
            model.addAttribute("promotion", promotion);
            model.addAttribute("action", "add-promotion");
            return "manager/form-Promotion";
        } else {
            List<String> urls = new ArrayList<>();
            String pathName ="D:\\TrungTam\\DoAn\\DienThoai\\Java\\Mau7\\DienThoai\\src\\main\\webapp\\resources\\images\\promotion";
//                    = servletContext.getRealPath("/resources/upload/");
            urls = upLoadFile.upLoadImage(files, pathName);
            promotionService.savePromotion(promotion, urls);
            return "redirect:/manager/homeListPromotion";
        }

    }

    @RequestMapping("edit-promotion/{promotionId}")
    public String editPromotion(Model model, @PathVariable("promotionId") int promotionId) {
        model.addAttribute("promotion", promotionService.findPromotion(promotionId));
        model.addAttribute("action", "edit-promotion");
        return "manager/form-Promotion";
    }

    @RequestMapping(value = "/edit-promotion", method = RequestMethod.POST)
    public String saveEditPromotion(Model model, @ModelAttribute("promotion") PromotionEntity promotion,
            @RequestParam("files") List<MultipartFile> files) {
        List<String> urls = new ArrayList<>();
        String pathName = "D:\\TrungTam\\DoAn\\DienThoai\\Java\\Mau7\\DienThoai\\src\\main\\webapp\\resources\\images\\promotion";
//                = servletContext.getRealPath("/resources/upload/");
        urls = upLoadFile.upLoadImage(files, pathName);
        promotionService.savePromotion(promotion, urls);
        return "redirect:/manager/homeListPromotion";
    }

    @RequestMapping("promotion-product/{promotionId}")
    public String promotionProduct(Model model, @PathVariable("promotionId") int promotionId) {
        Set<ProductEntity> productPromotions = productService.findListProduct02(promotionId);
        for (ProductEntity product : productPromotions) {
            List<ProductDetailEntity> productDetails = productDetailService.getProductId(product.getId());
            List<ImageEntity> images = imagesService.getProductId(product.getId());
            product.setProductDetail(productDetails);
            product.setImage(images);
        }
        model.addAttribute("promotion", promotionService.findPromotion(promotionId));
        model.addAttribute("products", productService.getProduct());
        model.addAttribute("action", "promotion-product");
        model.addAttribute("productPromotion", productPromotions);
        return "manager/addPromotionProduct";
    }

    @RequestMapping(value = {"promotion-product"}, method = RequestMethod.POST)
    public String savePromotionProduct(Model model, @RequestParam("id") int promotionId,
            @RequestParam("ListProductId") List<Integer> ListProductId) {
        Set<ProductEntity> products = productService.findListProduct(ListProductId);
        PromotionEntity promotion = promotionService.findPromotion(promotionId);
        promotionService.savePromotionProduct(promotion, products);
        return "redirect:/manager/homeListPromotion";
    }

    @RequestMapping("/listpromotion-product/{promotionId}")
    public String ListPromotionProduct(Model model, @PathVariable("promotionId") int promotionId) {
        PromotionEntity promotion = promotionService.findPromotion(promotionId);
        Set<ProductEntity> productPromotions = productService.findListProduct02(promotionId);
        Set<PromotionEntity> promotions = new HashSet<>();
        promotions.add(promotion);
        for (ProductEntity product : productPromotions) {
            product.setPromotion(promotions);
            List<ProductDetailEntity> productDetails = productDetailService.getProductId(product.getId());
            for (ProductDetailEntity productDetail : productDetails) {
                productDetail.setProduct(product);
            }
            List<ImageEntity> images = imagesService.getProductId(product.getId());
            product.setProductDetail(productDetails);
            product.setImage(images);

        }
        model.addAttribute("productPromotion", productPromotions);
        model.addAttribute("promotionId", promotionId);
        return "manager/listPromotionProduct";
    }

    @RequestMapping("delete-productPromotion/{productId}/{promotionId}")
    public String deleteProductPromotion(Model model, @PathVariable("productId") int productId,
            @PathVariable("promotionId") int promotionId) {
        PromotionEntity promotion = promotionService.findPromotion(promotionId);
        Set<ProductEntity> productPromotions = productService.findListProduct02(promotionId);
//        ProductEntity productEntity = productService.findProductId(productId);
//        if(productPromotions.contains(productEntity)){
//            productPromotions.remove(productEntity);
//        }
        for (ProductEntity pr : productPromotions) {
            if (pr.getId() == productId) {
                productPromotions.remove(pr);
                break;
            }
        }
        promotion.setProduct(productPromotions);
        promotionService.savePromotionProduct(promotion, productPromotions);
        return "redirect:/manager/listpromotion-product/" + promotionId;
    }
}
