/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.controller;

import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.service.ProducerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Hi_XV
 */
@Controller
@RequestMapping("/manager")
public class ProducerController {
    @Autowired
    ProducerService producerService;
    
      @RequestMapping("/homeListPrducer")
      public String viewlistPoducer(Model model){
          List<ProducerEntity> producers = producerService.getProducer();
          model.addAttribute("producers", producers);
          return "manager/listProducer";
      }
      @RequestMapping("/addProducer")
      public String addProducer(Model model){
          model.addAttribute("producer", new ProducerEntity());
          model.addAttribute("action", "addProducer");
          return "manager/add-Form-Producer";
      }
      @RequestMapping(value = {"/addProducer"}, method = RequestMethod.POST)
      public String saveProducer(Model model, @ModelAttribute("producer") ProducerEntity producer){
          producerService.saveProducer(producer);
          return "redirect:/manager/homeListPrducer";
      }
      @RequestMapping(value = {"/edit-Producer/{producerId}"})
      public String saveProducer(Model model, @PathVariable("producerId") int producerId){
          model.addAttribute("producer", producerService.getProducerId(producerId));
          model.addAttribute("action", "edit-Producer");
          return "manager/add-Form-Producer";
      }
      @RequestMapping(value = {"/edit-Producer"}, method = RequestMethod.POST)
      public String saveEditProducer(Model model, @ModelAttribute("producer") ProducerEntity producer){
          producerService.saveProducer(producer);
          return "redirect:/manager/homeListPrducer";
      }
}
