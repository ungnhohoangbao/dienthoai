/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.controller;

import com.google.gson.Gson;
import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.ColorEntity;
import com.mycompany.jv30_project_final02.entities.CustomerEntity;
import com.mycompany.jv30_project_final02.entities.ImageEntity;
import com.mycompany.jv30_project_final02.entities.MemoryEntity;
import com.mycompany.jv30_project_final02.entities.OrderDetailEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.ReviewEntity;
import com.mycompany.jv30_project_final02.enums.Gender;
import com.mycompany.jv30_project_final02.service.AccountService;
import com.mycompany.jv30_project_final02.service.ColorService;
import com.mycompany.jv30_project_final02.service.ImagesService;
import com.mycompany.jv30_project_final02.service.MemoryService;
import com.mycompany.jv30_project_final02.service.OrderDetailService;
import com.mycompany.jv30_project_final02.service.OrderService;
import com.mycompany.jv30_project_final02.service.ProducerService;
import com.mycompany.jv30_project_final02.service.ProductDetailService;
import com.mycompany.jv30_project_final02.service.ProductService;
import com.mycompany.jv30_project_final02.service.PromotionService;
import com.mycompany.jv30_project_final02.service.ReviewService;
import com.mycompany.jv30_project_final02.utils.ExportExcel;
import com.mycompany.jv30_project_final02.utils.HasCode;
import com.mycompany.jv30_project_final02.utils.UpLoadFile;
import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hi_XV
 */
@Controller
@RequestMapping("/manager")
public class ManagerController{
    @Autowired
    AccountService accountService;
    
    @Autowired
    ProductService productService;
    
    @Autowired
    ProducerService producerService;
    
    @Autowired
    ProductDetailService productDetailService;
    
    @Autowired
    ColorService colorService;
    
    @Autowired
    MemoryService memoryService;
    
    @Autowired
    ServletContext servletContext;
    
    @Autowired
    ImagesService imagesService;
    
    @Autowired
    UpLoadFile upLoadFile;
    
    @Autowired
    OrderService orderService;
    
    @Autowired
    PromotionService promotionService;
    
    @Autowired
    ReviewService reviewService;
    
    @Autowired
    OrderDetailService orderDetailService;
    
    @RequestMapping(value = {"/homeManager", "/"}, method = RequestMethod.GET)
    public String getManages(Model model){
        int sumOrder = orderService.findSumOrderDone();
        int sumReview = reviewService.findSumReview();
        int sumPromotion = promotionService.findSumPromotion();
        double sumOrderPriceDone = orderService.findPriceOrder();
        Locale localeVN = new Locale("vi", "VN");
        NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
        String priceFormatted = currency.format(sumOrderPriceDone);
        model.addAttribute("sumOrder", sumOrder);
        model.addAttribute("sumReview", sumReview);
        model.addAttribute("sumPromotion", sumPromotion);
        model.addAttribute("sumOrderPriceDone", priceFormatted);
        
        return "manager/home-manages";
    }
     
    @RequestMapping(value = {"/viewlistProduct"}, method = RequestMethod.GET)
    public String getProduct(Model model){
        List<ProductEntity> products = productService.getProduct();
        for (ProductEntity product : products) {
            List<ProductDetailEntity> productDetails = productDetailService.getProductId(product.getId());
            List<ImageEntity> images = imagesService.getProductId(product.getId());
            product.setProductDetail(productDetails);
            product.setImage(images);
        }
        Collections.sort(products, new Comparator<ProductEntity>() {
        @Override
        public int compare(ProductEntity o1, ProductEntity o2) {
        return o2.getDateCreate().compareTo(o1.getDateCreate());
        }
        });
    model.addAttribute("products", products);
    return "manager/listProduct";
    }
    @RequestMapping("/add-product")
    public String addProduct(Model model){
        
        model.addAttribute("product", new ProductEntity());
        model.addAttribute("producers", producerService.getProducer());
        model.addAttribute("action", "add-product");
        return "manager/form-Product";
    }
    @RequestMapping(value = {"/add-product"}, method = RequestMethod.POST)
    public String saveProduct(Model model,@Valid @ModelAttribute("product") ProductEntity product, BindingResult result,
            @RequestParam("files") List<MultipartFile> files){
        if(result.hasErrors()){
            model.addAttribute("product", product);
        model.addAttribute("producers", producerService.getProducer());
        model.addAttribute("action", "add-product");
        return "manager/form-Product";
        }else{
            List<String> urls = new ArrayList<>();
        String pathName = "D:\\TrungTam\\DoAn\\DienThoai\\Java\\Mau7\\DienThoai\\src\\main\\webapp\\resources\\images\\phone";
//                   servletContext.getRealPath("/resources/upload/");
        urls = upLoadFile.upLoadImage(files, pathName);

        productService.saveProduct(product, urls);
        return "redirect:/manager/viewlistProduct";
        }
        
    }
    @RequestMapping("/edit-product/{productId}")
    public String editProduct(Model model, @PathVariable("productId") int productId){
        List<ImageEntity> images = imagesService.getProductId(productId);
        model.addAttribute("producers", producerService.getProducer());
        model.addAttribute("product", productService.findProductId(productId));
        model.addAttribute("images", images);
        model.addAttribute("action", "edit-product");
        return "manager/form-Product";
    }
    @RequestMapping(value = {"/edit-product"}, method = RequestMethod.POST)
    public String saveEditProduct(Model model, @ModelAttribute("product") ProductEntity product)
            {
        
//        List<String> urls = new ArrayList<>();
//        String pathName = 
//                   servletContext.getRealPath("/resources/upload/");
//        urls = UpLoadFile.upLoadImage(files, pathName);
        productService.saveProductId(product);
        return "redirect:/manager/viewlistProduct";
    }
    @RequestMapping(value = {"edit-images"}, method = RequestMethod.POST)
    public String editImages(Model model, @ModelAttribute("product") ProductEntity product,
              @RequestParam("files") List<MultipartFile> files){
         List<String> urls = new ArrayList<>();
        String pathName = "D:\\TrungTam\\DoAn\\DienThoai\\Java\\Mau7\\DienThoai\\src\\main\\webapp\\resources\\images\\phone";
//                   servletContext.getRealPath("/resources/upload/");
        urls = upLoadFile.upLoadImage(files, pathName);
        productService.saveImage(product, urls);
        model.addAttribute("action","edit-images");
        return "redirect:/manager/edit-product/"+product.getId();
    }
   @RequestMapping("/deleteImages/{imageId}")
      public String DeleteImages(Model model,@PathVariable("imageId") int imageId){
          ImageEntity image = imagesService.findImage(imageId);
          ProductEntity product = productService.findProductId(image.getProduct().getId());
          imagesService.deleteImage(imageId);
          return "redirect:/manager/edit-product/"+product.getId();
      }
     @RequestMapping("/update-Status/{productId}")
      public String updateStatus(Model model,
              @PathVariable("productId") int productId, @RequestParam("status") int status){
          ProductEntity product = productService.findProductId(productId);
          productService.updateStatus(product, status);
          return "redirect:/manager/viewlistProduct";
      }
    
      @RequestMapping("/addProducrDetail/{productId}") 
      public String uploadProductDetail(Model model, @PathVariable("productId") int productId){
          
          List<ProductDetailEntity> productDetails = productDetailService.getProductId(productId);
          model.addAttribute("product", productService.findProductId(productId));
          model.addAttribute("productDetails", productDetails);
          model.addAttribute("memorys", memoryService.getMemory());
          model.addAttribute("colors", colorService.getColor());
          model.addAttribute("action", "addProductDetail");
          return "manager/addProductDetail";
      }
      
      @RequestMapping(value = {"/addProductDetail"}, method = RequestMethod.POST)
      public String saveAddProductDetail(Model model, @ModelAttribute("product") ProductEntity product, 
             @RequestParam("memory") String memory,
             @RequestParam("price") double price,
             @RequestParam("colors") List<String> colors,
             @RequestParam("quantityInStore") int quantityInStore ){
          MemoryEntity memoryEntity = memoryService.findMemory(memory);
          List<ColorEntity> colorEntitys = colorService.findColor(colors);
          productService.saveProductDetail(product, memoryEntity, price,colorEntitys,quantityInStore);
         
          return "redirect:/manager/addProducrDetail/"+product.getId();
      }
      @RequestMapping("/update-ImagePd/{productDetailId}")
      public String updateImagePd(Model model,@PathVariable("productDetailId") int productDetailId,
              @RequestParam("files") List<MultipartFile> files){
          ProductDetailEntity productDetail = productDetailService.getProductDetail(productDetailId);
          ProductEntity product = productService.findProductId(productDetail.getProduct().getId());
           List<String> urls = new ArrayList<>();
        String pathName = "D:\\TrungTam\\DoAn\\DienThoai\\Java\\Mau7\\DienThoai\\src\\main\\webapp\\resources\\images\\product_detail";
//                   servletContext.getRealPath("/resources/upload/");
        urls = upLoadFile.upLoadImage(files, pathName);
        productDetailService.addImage(productDetail, urls);
        return "redirect:/manager/addProducrDetail/"+product.getId(); 
        
      }
      @RequestMapping("/deleteProductDetail/{productDetailId}")
      public String DeleteProductDetail(Model model,@PathVariable("productDetailId") int productDetailId){
          ProductDetailEntity productDetail = productDetailService.getProductDetail(productDetailId);
          ProductEntity product = productService.findProductId(productDetail.getProduct().getId());
          productDetailService.deleteProductDetail(productDetailId);
          return "redirect:/manager/addProducrDetail/"+product.getId();
      }
      @RequestMapping("/add-Color")
      public String addColor(Model model){
          model.addAttribute("colors", colorService.getColor());
          model.addAttribute("color",new ColorEntity());
          return "manager/form-color";
      }
      @RequestMapping(value = {"/save-Color"}, method = RequestMethod.POST)
      public String saveColor(Model model, @ModelAttribute("color") ColorEntity color){
          colorService.saveColor(color);
          return "redirect:/manager/add-Color";
      }
      @RequestMapping("/delete-Color/{colorId}")
      public String deleteColor(Model model, @PathVariable("colorId") int colorId){
          colorService.deleteColor(colorId);
          return "redirect:/manager/add-Color";
      }
    @RequestMapping("/add-Memory")
      public String addMemory(Model model){
          model.addAttribute("memorys", memoryService.getMemory());
          model.addAttribute("memory",new MemoryEntity());
          return "manager/form-Memory";
      }
      @RequestMapping(value = {"/save-Memory"}, method = RequestMethod.POST)
      public String saveMemory(Model model, @ModelAttribute("memory") MemoryEntity memory){
          memoryService.saveMemory(memory);
          return "redirect:/manager/add-Memory";
      }
      @RequestMapping("/search-Product")
      public String searchProduct(Model model, @RequestParam("searchText") String searchText){
          List<ProductEntity> products  = productService.searchProduct(searchText);
           for (ProductEntity product : products) {
            List<ProductDetailEntity> productDetails = productDetailService.getProductId(product.getId());
            List<ImageEntity> images = imagesService.getProductId(product.getId());
            product.setProductDetail(productDetails);
            product.setImage(images);
        }
          model.addAttribute("products", products);
          return "manager/listProduct";
      }
      //bảo làm
      @RequestMapping(value ="/infor",method = RequestMethod.GET)
    public String register(Model model){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity account = new AccountEntity();
        if (principal instanceof AccountEntity) {
            account = (AccountEntity)principal;
        } 
        model.addAttribute("gender", Gender.values());
        model.addAttribute("account", account);
        return "manager/infor";
    }
    @RequestMapping(value ="/update-infor",method = RequestMethod.POST)
    public String updateInfor(Model model,@Valid @ModelAttribute("account") AccountEntity account,
           BindingResult result,HttpSession session){
       if(result.hasErrors()){
            model.addAttribute("account", account);
            model.addAttribute("gender",Gender.values());
            return "manager/infor";
       }
       else{
           Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
           AccountEntity accounts = (AccountEntity)principal;
           AccountEntity acc = accountService.findByEmail(account.getEmail());
           if((acc == null) || (acc.getEmail().equals(accounts.getEmail()))){
                if(acc == null){
                    accounts.setEmail(account.getEmail());
                }
                accounts.setName(account.getName());
                accounts.setPhoneNumber(account.getPhoneNumber());
                accounts.setBirthDate(account.getBirthDate());
                accounts.setGender(account.getGender());
                accounts.setAddress(account.getAddress());
                // sửa code accountService.saveAccount(accounts);
                accountService.saveAccount(accounts);
                model.addAttribute("mess", "success");
                model.addAttribute("account", accounts);
                model.addAttribute("gender",Gender.values());
           }
           else if(!acc.getEmail().equals(accounts.getEmail())){
                model.addAttribute("gender", Gender.values());
                model.addAttribute("account", account);
                model.addAttribute("mess", "already_exist");
           }
           return "manager/infor";
       }
    }
    
    @RequestMapping(value = "/check-email",method = RequestMethod.POST)
    @ResponseBody
    public String checkEmail(@RequestBody String email){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity accounts = (AccountEntity)principal;
        AccountEntity acc = accountService.findByEmail(email);
        String checkEmail = "success";
        if(acc != null &&!acc.getEmail().equals(accounts.getEmail())){
            checkEmail = "already_exist";
        }
        Gson gson = new Gson();
        String returnString = gson.toJson(checkEmail);
        return returnString;
    }
    
    @RequestMapping(value ="/change-password",method = RequestMethod.GET)
    public String viewChangePassword(){
        return "manager/change-password";
    }
    
    @RequestMapping(value ="/change-password",method = RequestMethod.POST)
    public String changePassword(Model model,
            @RequestParam("oldPass") String oldPass,
            @RequestParam("newPass") String newPass,
            @RequestParam("againPass") String againPass){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity accounts = (AccountEntity)principal;
        String stReturn = "";
        if(!HasCode.MD5Encode(oldPass).equals(accounts.getPassword())){
            stReturn = "incorrect";
            model.addAttribute("mess", stReturn);
            return "manager/change-password";
        }
        else if(!newPass.equals(againPass)){
            stReturn = "not_match";
            model.addAttribute("mess", stReturn);
            return "manager/change-password";
        }
        else{
            accounts.setPassword(HasCode.MD5Encode(newPass));
            accountService.saveAccount(accounts);
        }
        return "manager/message-change-pass";
    }
    
    @RequestMapping("/UpdateMemory")
    public String Memory(Model model){
        List<MemoryEntity> memorys = memoryService.getMemory();
        model.addAttribute("memory", new MemoryEntity());
        model.addAttribute("memorys", memorys);
        return "manager/updateMemory";
    }
    
    @RequestMapping("/list-Review")
    public String listReview(Model model){
        List<ReviewEntity> reviews = reviewService.getReviews();
        for (ReviewEntity review : reviews) {
            int productId = reviewService.findProductId(review.getId());
            ProductEntity product = productService.findProductId(productId);
            int accountId = reviewService.findAccountId(review.getId());
            AccountEntity account = accountService.findAccount(accountId);
            review.setProduct(product);
            review.setAccount(account);
        }
        model.addAttribute("reviews", reviews);
        return "manager/listReviews";
    }
    @RequestMapping("/delete-Review/{reviewId}")
    public String deleteReview(Model model, @PathVariable("reviewId") int reviewId){
        reviewService.deleteReview(reviewId);
        return "redirect:/manager/list-Review";
    }
    @RequestMapping(value = {"/search-Review"}, method = RequestMethod.POST)
    public String searchReview(Model model, @RequestParam("searchText")String seachText){
        List<ReviewEntity> reviews = reviewService.searchReview(seachText);
        model.addAttribute("reviews", reviews);
        return "manager/listReviews";
    }
    
    @RequestMapping(value="/dowload-order")
    protected void processRequest(HttpServletResponse response, HttpSession session) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        List<OrdersEntity> orderList = (List<OrdersEntity>)session.getAttribute("listSearchOrders");
        if (orderList == null) {
            orderList = orderService.getOrders();
            for (OrdersEntity order : orderList) {
                CustomerEntity customer = orderService.findCustomer(order.getId());
                List<OrderDetailEntity> orderDetails = orderDetailService.findListOrderDetail(order.getId());
                for (OrderDetailEntity orderDetail : orderDetails) {
                    ProductEntity product = orderDetailService.findProduct(orderDetail.getId());
                    orderDetail.setProduct(product);
                }
                order.setCustomer(customer);
                order.setOrderDetails(orderDetails);
            }
        }
        
        ExportExcel exportExcel = new ExportExcel();
        Workbook workbook = exportExcel.exportFileManager(orderList);
        response.setHeader("content-disposition", "attachment; filename=Report.xls");
        response.setHeader("cache-control", "no-cache");
        
        OutputStream out = response.getOutputStream();
        workbook.write(out);
        out.close();   
    }
}
