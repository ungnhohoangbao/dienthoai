/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.controller;

import com.google.gson.Gson;
import com.mycompany.jv30_project_final02.common.AjaxProduct;
import com.mycompany.jv30_project_final02.service.AccountRoleService;
import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.CustomerEntity;
import com.mycompany.jv30_project_final02.entities.FavoriteEntity;
import com.mycompany.jv30_project_final02.entities.MemoryEntity;
import com.mycompany.jv30_project_final02.entities.OrderDetailEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.PromotionEntity;
import com.mycompany.jv30_project_final02.entities.ReviewEntity;
import com.mycompany.jv30_project_final02.enums.Gender;
import com.mycompany.jv30_project_final02.enums.StatusAccount;
import com.mycompany.jv30_project_final02.enums.StatusOrder;
import com.mycompany.jv30_project_final02.service.AccountService;
import com.mycompany.jv30_project_final02.service.ColorService;
import com.mycompany.jv30_project_final02.service.CustomerService;
import com.mycompany.jv30_project_final02.service.FavoriteService;
import com.mycompany.jv30_project_final02.service.ImagesService;
import com.mycompany.jv30_project_final02.service.MailService;
import com.mycompany.jv30_project_final02.service.MemoryService;
import com.mycompany.jv30_project_final02.service.OrderDetailService;
import com.mycompany.jv30_project_final02.service.ProducerService;
import com.mycompany.jv30_project_final02.service.ProductDetailService;
import com.mycompany.jv30_project_final02.service.ProductService;
import com.mycompany.jv30_project_final02.service.PromotionService;
import com.mycompany.jv30_project_final02.service.ReviewService;
import com.mycompany.jv30_project_final02.utils.HasCode;
import java.text.NumberFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author AnhLe
 */
@Controller
public class HomeController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductDetailService productDetailService;

    @Autowired
    private ColorService colorService;

    @Autowired
    private MemoryService memoryService;

    @Autowired
    private MemoryService memoryService1;

    @Autowired 
    private ImagesService imageService;

    @Autowired
    private ProducerService producerService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private MailService mailService;

    @Autowired
    private PromotionService promotionService;
    
    @Autowired
    private AccountService accountService;
    
    @Autowired
    private FavoriteService favoriteService;
    
    @Autowired
    private OrderDetailService orderDetailService;
    
    @Autowired
    private AccountRoleService accountRoleService;
    
    @Autowired
    private ReviewService reviewService;
    
    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String viewHome(Model model) {
        List<ProductDetailEntity> productDetail = productDetailService.findProductDetailLimit();
        List<ProductEntity> product = new ArrayList<>();
        for (int i = 0; i < productDetail.size(); i++) {
            ProductEntity p = productService.findByProductDetail(productDetail.get(i));
            List<ProductDetailEntity> pd = new ArrayList<>();
            pd.add(productDetail.get(i));
            p.setProductDetail(pd);
            product.add(p);
        }
        
        for(int i =0; i<product.size(); i++){
            product.get(i).setPromotion(promotionService.findByProductIdAndDateNow(product.get(i).getId()));
            product.get(i).setReview(reviewService.findByProduct(product.get(i)));
        }
        
        // Sản phẩm hot
        List<ProductEntity> product2 = new ArrayList<>();
        List<Integer> listIdProduct = orderDetailService.findIdProduct();
        for(Integer idp: listIdProduct){
            product2.add(productService.findById(idp));
        }
        for (int i = 0; i < product2.size(); i++) {
            List<ProductDetailEntity> pd = new ArrayList<>();
            pd = productDetailService.finByProduct(product2.get(i));
            product2.get(i).setProductDetail(pd);
        }
        for(int i =0; i<product2.size(); i++){
            product2.get(i).setPromotion(promotionService.findByProductIdAndDateNow(product2.get(i).getId()));
            product2.get(i).setReview(reviewService.findByProduct(product2.get(i)));
        }
        model.addAttribute("listProduct", product);
        model.addAttribute("listProduct2", product2);
        return "home";
    }

    @RequestMapping("/login")
    public String viewLogin(Model model,
            @RequestParam(value = "isError", required = false) boolean isError) {
        if (isError) {
            model.addAttribute("message", "Đăng nhập không thành công!!.");
        }
        return "login";
    }

    @RequestMapping(value = "/product-detail/{id}", method = RequestMethod.GET)
    public String viewProductDetail(@PathVariable(value = "id", required = false) int id, Model model) {
        ProductDetailEntity productDetail = productDetailService.findById(id);
        if(productDetail !=null && productDetail.getId()>0){
            productDetail.setColor(colorService.findByProductDetail(productDetail));
            productDetail.setMemory(memoryService.findByProductDetail(productDetail));
            productDetail.getProduct().setImage(imageService.findByProduct(productDetail.getProduct()));
            Set<PromotionEntity> promotion = promotionService.findByProductIdAndDateNow(productDetail.getProduct().getId());
            model.addAttribute("promotion", promotion);
            model.addAttribute("productDetail", productDetail);
            List<MemoryEntity> listMemory = memoryService1.findByProductId(productDetail.getProduct().getId());
            for (int i = 0; i < listMemory.size(); i++) {
                listMemory.get(i).setProductDetail(productDetailService.finByMemoryAndProduct(listMemory.get(i), productDetail.getProduct()));
            }
            model.addAttribute("listMemory", listMemory);
            // sản phẩm tương tự
            List<ProductDetailEntity> listProductDetail = productDetailService.findByProducerIdLimit(producerService.findByProduct(productDetail.getProduct()).getId());
            for(int i=0; i<listProductDetail.size(); i++){
                listProductDetail.get(i).getProduct().setPromotion(promotionService.findByProductIdAndDateNow(listProductDetail.get(i).getProduct().getId()));
            }
            model.addAttribute("listProductDetail", listProductDetail);
            
            // check riview and favorite
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof AccountEntity) {
                AccountEntity account = (AccountEntity)principal;
                List<OrderDetailEntity> orderDetails = orderDetailService.findByProductAndAccountAndStatusOrder(productDetail.getProduct(), account, StatusOrder.Done);
                if(orderDetails.size()>0){
                    model.addAttribute("checkComment", "checkComment");
                }
                FavoriteEntity favorite = favoriteService.findByAccountAndProduct(account, productDetail.getProduct());
                model.addAttribute("favorite", favorite);
            } 
            
            // list riview
            List<ReviewEntity> riviews = reviewService.findByProductOrderByIdDesc(productDetail.getProduct());
            model.addAttribute("riviews", riviews);
        }
        return "product-detail";
    }

    @RequestMapping("order/{productDetailId}")
    @ResponseBody
    public String order(@RequestBody @PathVariable("productDetailId") int productDetailId,
            Model model,
            HttpSession session) {
        OrdersEntity order = (OrdersEntity) session.getAttribute("order");
        if (order == null) {
            order = new OrdersEntity();
        }

        List<OrderDetailEntity> orderDetail = order.getOrderDetails();
        if (orderDetail == null) {
            orderDetail = new ArrayList<>();
        }
        ProductDetailEntity produceDetail = productDetailService.findById(productDetailId);
        produceDetail.setProduct(productService.findByProductDetail(produceDetail));
        List<ProductDetailEntity> listProductDetail1 = new ArrayList<>();
        listProductDetail1.add(produceDetail);
        produceDetail.getProduct().setProductDetail(listProductDetail1);

        int check = 0;
        double totalPrice = 0;
//        Cập nhật lại số lượng trong giỏ hàng
        if (orderDetail.size() > 0) {
            for (int i = 0; i < orderDetail.size(); i++) {
                List<ProductDetailEntity> listProductDetail = orderDetail.get(i).getProduct().getProductDetail();
                for (ProductDetailEntity list : listProductDetail) {
                    if (list.getId() == produceDetail.getId()) {
                        orderDetail.get(i).setQuantity(orderDetail.get(i).getQuantity() + 1);
                        totalPrice = orderDetail.get(i).getPrice();
                        check = 1;
                        break;
                    }
                }
            }
        }
        // Nếu orderDetail null thì khởi tạo mảng
        if (orderDetail == null) {
            orderDetail = new ArrayList<>();
        }
//        chưa có để cập nhật thì thêm vào orderDetail mới
        if (check == 0) {
            OrderDetailEntity odt = new OrderDetailEntity();
            odt.setOrder(order);            //Chú ý
            odt.setQuantity(1);
            Set<PromotionEntity> promotion = promotionService.findByProductIdAndDateNow(produceDetail.getProduct().getId());
            Iterator value = promotion.iterator(); 
  
            // Displaying the values after iterating through the iterator 
            PromotionEntity pr = new PromotionEntity();
            while (value.hasNext()) { 
                pr = (PromotionEntity) value.next(); 
            } 
            if(pr!=null && pr.getId()>0){
                odt.setPrice(produceDetail.getPrice() - (produceDetail.getPrice() * pr.getDiscount())/100);
            }
            else{
                odt.setPrice(produceDetail.getPrice());
            }
            odt.setMemory(produceDetail.getMemory().getMemory());
            odt.setColor(produceDetail.getColor().getColor());
            odt.setProduct(produceDetail.getProduct());
            orderDetail.add(odt);
            totalPrice = odt.getQuantity() * odt.getPrice();
        }

        order.setOrderDetails(orderDetail);
        order.setOrderDate(new Date());
        order.setTotalPrice(order.getTotalPrice() + totalPrice);
        session.setAttribute("order", order);

        Gson gson = new Gson();
        String ok = gson.toJson("ok");
        return ok;
    }

    @RequestMapping("/cart")
    public String viewCart() {
        return "cart";
    }

    @RequestMapping(value = "/update-quantity", method = RequestMethod.POST)
    @ResponseBody
    public String updateQuantity(@RequestBody String body,
            Model model,
            HttpSession session) {
        OrdersEntity order = (OrdersEntity) session.getAttribute("order");
        String[] updateQ = body.split(",");
        int productDetailId = Integer.parseInt(updateQ[0]);
        int quantity = Integer.parseInt(updateQ[1]);
        double totalPrice = 0;
        List<OrderDetailEntity> orderDetail = order.getOrderDetails();
        ProductDetailEntity productDetail = productDetailService.findById(productDetailId);
        String checkQuantity = "ok";
        int diffeQuantity = 0;
        for (int i = 0; i < orderDetail.size(); i++) {
            if (productDetailId == orderDetail.get(i).getProduct().getProductDetail().get(0).getId()) {
                if (quantity > productDetail.getQuantityInStore()) {
                    quantity = productDetail.getQuantityInStore();
                    checkQuantity = "no-ok";
                }
                diffeQuantity = quantity - orderDetail.get(i).getQuantity();
                totalPrice = diffeQuantity * orderDetail.get(i).getPrice();
                orderDetail.get(i).setQuantity(quantity);
            }
        }
        order.setTotalPrice(order.getTotalPrice() + totalPrice);
        session.setAttribute("order", order);
        Gson gson = new Gson();
        String returnString = gson.toJson("{\"ok\":\"" + checkQuantity + "\",\"totalPrice\":\""
                + order.getTotalPriceFormatted() + "\",\"quantity\":\"" + quantity + "\","
                + "\"diffeQuantity\":\"" + diffeQuantity + "\"}");
        return returnString;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(Model model,
            @RequestParam(value = "search") String search,
            HttpSession session) {
        List<ProductEntity> listProduct = productService.searchByName(search);
        for (int i = 0; i < listProduct.size(); i++) {
            List<ProductDetailEntity> listProductDetail = productDetailService.findByProductIdGroupMemory(listProduct.get(i).getId());
            listProduct.get(i).setProductDetail(listProductDetail);
            listProduct.get(i).setPromotion(promotionService.findByProductIdAndDateNow(listProduct.get(i).getId()));
            listProduct.get(i).setReview(reviewService.findByProduct(listProduct.get(i)));
        }
        ArrayList<String> listSearch = (ArrayList<String>) session.getAttribute("listSearch");
        if (listSearch == null) {
            listSearch = new ArrayList<>();
        }
        if (listSearch.size() > 6) {
            listSearch.remove(0);
        }
        boolean check = false;
        for (int i = 0; i < listSearch.size(); i++) {
            if (listSearch.get(i).equals(search)) {
                check = true;
            }
        }
        if (listSearch.size() == 0 || check == false) {
            listSearch.add(search);
        }
        model.addAttribute("search", search);
        session.setAttribute("listSearch", listSearch);
        model.addAttribute("listProduct", listProduct);
        return "search";
    }
    
    @RequestMapping(value = "/producer/{id}", method = RequestMethod.GET)
    public String viewProductByProducer(@PathVariable(value = "id", required = false) int id, Model model){
        ProducerEntity producer = producerService.findById(id);
        List<ProductEntity> listProduct = new ArrayList<>();
        if(producer!=null && producer.getId()>0){
            listProduct = productService.findByProducer(producer);
            for (int i = 0; i < listProduct.size(); i++) {
                List<ProductDetailEntity> listProductDetail = productDetailService.findByProductIdGroupMemory(listProduct.get(i).getId());
                listProduct.get(i).setProductDetail(listProductDetail);
                listProduct.get(i).setPromotion(promotionService.findByProductIdAndDateNow(listProduct.get(i).getId()));
                listProduct.get(i).setReview(reviewService.findByProduct(listProduct.get(i)));
            }
        }
        else{
            producer = new ProducerEntity();
        }
        model.addAttribute("listProduct", listProduct);
        model.addAttribute("producer", producer);
        return "search";
    }

    @RequestMapping("deleteProductDetail/{productDetailId}")
    @ResponseBody
    public String deleteProductDetail(@RequestBody @PathVariable("productDetailId") int productDetailId,
            HttpSession session) {
        OrdersEntity order = (OrdersEntity) session.getAttribute("order");
        List<OrderDetailEntity> orderDetail = order.getOrderDetails();
        double totalPrice = 0;
        int quantity = 0;
        for (int i = 0; i < orderDetail.size(); i++) {
            if (orderDetail.get(i).getProduct().getProductDetail().get(0).getId() == productDetailId) {
                quantity = order.getOrderDetails().get(i).getQuantity();
                totalPrice = quantity * order.getOrderDetails().get(i).getPrice();
                order.getOrderDetails().remove(i);
            }
        }
        order.setTotalPrice(order.getTotalPrice() - totalPrice);
        session.setAttribute("order", order);
        Gson gson = new Gson();
        String returnString = gson.toJson("{\"totalPrice\":\""
                + order.getTotalPriceFormatted() + "\",\"quantity\":\"" + quantity + "\"}");
        return returnString;
    }

    @RequestMapping(value = "/pay", method = RequestMethod.GET)
    public String viewPay(Model model, HttpSession session) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CustomerEntity customer = new CustomerEntity();
        if (principal instanceof AccountEntity) {
            customer.setEmail(((AccountEntity) principal).getEmail());
            customer.setName(((AccountEntity) principal).getName());
            customer.setPhoneNumber(((AccountEntity) principal).getPhoneNumber());
            customer.setAddress(((AccountEntity) principal).getAddress());
        }
        model.addAttribute("customer", customer);
        model.addAttribute("gender", Gender.values());
        return "pay";
    }

    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    public String pay(Model model, @Valid @ModelAttribute("customer") CustomerEntity customer,
            BindingResult result, HttpSession session) {
        if (result.hasErrors()) {
            model.addAttribute("customer", customer);
            model.addAttribute("gender", Gender.values());
            return "pay";
        } else {
            OrdersEntity order = (OrdersEntity) session.getAttribute("order");
            ArrayList<OrdersEntity> orders = new ArrayList<>();
            orders.add(order);
            customer.setOrders(orders);
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof AccountEntity) {
                customer.setAccount((AccountEntity) principal);
            }
            try {
                order =customerService.saveOrder(customer, order);
            } catch (Exception ex) {
                customerService.deleteCustomer(order.getCustomer());
            }
            if(order!=null && order.getId()>0){
                String subject = "Chi tiết hóa đơn";
                try {
                    mailService.sendEmail(subject,order,customer.getEmail());
                } catch (MessagingException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
//                model.addAttribute("order1", order);
                session.removeAttribute("order");
                return "invoice-details";
            }
            else{
                return "mess-fail-pay";
            }
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String viewRegister(Model model) {
        model.addAttribute("account", new AccountEntity());
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(Model model, @Valid @ModelAttribute("account") AccountEntity account,
            BindingResult result,@RequestParam(value = "againPassword") String againPassword) {
        if (result.hasErrors()) {
            model.addAttribute("customer", account);
            return "register";
        } else {
            if(!againPassword.equals(account.getPassword())){
                model.addAttribute("mess", "Mật khẩu không trùng nhau");
            }
            else{
                AccountEntity acc = accountService.findByEmail(account.getEmail());
                if(acc != null && acc.getId()>0){
                    model.addAttribute("mess", "Email đã tồn tại");
                }
                else{
                    account.setStatus(StatusAccount.Khóa);
                    account.setPassword(HasCode.MD5Encode(account.getPassword()));
                    account = accountService.registerAccount(account);
                    if(account !=null && account.getId()>0){
                        String subject = "Xác nhận đăng ký";
                        try {
                            mailService.sendEmailConfirm(subject,account,account.getEmail());
                        } catch (MessagingException ex) {
                            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        model.addAttribute("success", "Đăng ký thành công, thông tin xác nhận được được gởi đến email của bạn,"
                                + " bạn vui lòng kiểm tra lại email");
                        return "mess-confirm-regisger";
                    }
                }
            }
        }
        model.addAttribute("customer", account);
        return "register";
    }
    
    @ModelAttribute
    public void getCategory(Model model) {
        List<ProducerEntity> producer = producerService.getAll();
        if (producer != null && producer.size() > 0) {
            model.addAttribute("listProducer", producer);
        }else{
            model.addAttribute("listProducer", new ArrayList<>());
        }
    }
    // user and guest
    
    @RequestMapping(value = "/confirm-register/{id}", method = RequestMethod.GET)
    public String confirmRegister(@PathVariable(value = "id", required = false) int id, Model model){
        AccountEntity account = accountService.findById(id);
        if(account.getStatus() == StatusAccount.Khóa){
            account.setStatus(StatusAccount.Mở);
            account = accountService.save(account);
            if(account != null && account.getId()>0){
                model.addAttribute("success", "Xác nhận đăng ký tài khoản thành công");
            }
            else{
                model.addAttribute("fail", "Xác nhận không thành công");
            }
        }
        else{
            if(account.getId()>0){
                model.addAttribute("fail", "Tài khoản của bạn đã được xác nhận");
            }
            else{
                model.addAttribute("fail", "Không tìm thấy tài khoản để xác nhận");
            }
        }
        return "mess-confirm-regisger";
    }
    
    
    @RequestMapping(value = "/search-ajax",method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String search(@RequestBody  String value) {
        List<ProductEntity> listProduct = productService.searchByName(value);
        for (int i = 0; i < listProduct.size(); i++) {
            List<ProductDetailEntity> listProductDetail = productDetailService.findByProductIdGroupMemory(listProduct.get(i).getId());
            listProduct.get(i).setProductDetail(listProductDetail);
            listProduct.get(i).setPromotion(promotionService.findByProductIdAndDateNow(listProduct.get(i).getId()));
            listProduct.get(i).setReview(reviewService.findByProduct(listProduct.get(i)));
        }
        int length = 5;
        if(listProduct.size() < 5){
            length = listProduct.size();
        }
        List<AjaxProduct> arrayAP = new ArrayList<>();
        for(int i=0; i< length; i++){
            double price = listProduct.get(i).getProductDetail().get(0).getPrice();
            int dc = 0;
            for(PromotionEntity ppr : listProduct.get(i).getPromotion()){
                dc = ppr.getDiscount();
            }
            double pricePromotion = price - (price * dc)/100;
            Locale localeVN = new Locale("vi", "VN");
            NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
            String priceFormatted = currency.format(pricePromotion);
            AjaxProduct ap = new AjaxProduct(listProduct.get(i).getProductDetail().get(0).getId(),
                listProduct.get(i).getProductDetail().get(0).getImage(),
                listProduct.get(i).getName() + " "+ listProduct.get(i).getRam() + "/"+listProduct.get(i).getProductDetail().get(0).getMemory().getMemory(),
                listProduct.get(i).getProductDetail().get(0).getPriceFormatted(),priceFormatted);
            arrayAP.add(ap);
        }
        Gson gson = new Gson();
        String str = gson.toJson(arrayAP);
        return str;
    }
    
    @RequestMapping(value = "/buy-product/{productDetailId}", method = RequestMethod.GET)
    public String viewBuyProduct(@PathVariable(value = "productDetailId", required = false) int productDetailId,
            Model model, HttpSession session) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CustomerEntity customer = new CustomerEntity();
        if (principal instanceof AccountEntity) {
            customer.setEmail(((AccountEntity) principal).getEmail());
            customer.setName(((AccountEntity) principal).getName());
            customer.setPhoneNumber(((AccountEntity) principal).getPhoneNumber());
            customer.setAddress(((AccountEntity) principal).getAddress());
        }
        model.addAttribute("customer", customer);
        model.addAttribute("gender", Gender.values());
        
        ProductDetailEntity productDetail = productDetailService.findById(productDetailId);
        OrderDetailEntity orderDetail = new OrderDetailEntity();
        orderDetail.setColor(productDetail.getColor().getColor());
        orderDetail.setMemory(productDetail.getMemory().getMemory());
        
        Set<PromotionEntity> promotion = promotionService.findByProductIdAndDateNow(productDetail.getProduct().getId());
        Iterator value = promotion.iterator(); 
  
            // Displaying the values after iterating through the iterator 
        PromotionEntity pr = new PromotionEntity();
        while (value.hasNext()) { 
            pr = (PromotionEntity) value.next(); 
        } 
        if(pr!=null && pr.getId()>0){
            orderDetail.setPrice(productDetail.getPrice() - (productDetail.getPrice() * pr.getDiscount())/100);
        }
        else{
            orderDetail.setPrice(productDetail.getPrice());
        }
        orderDetail.setQuantity(1);
        orderDetail.setProduct(productService.findByProductDetail(productDetail));
        orderDetail.setPrice(productDetail.getPrice());
        List<OrderDetailEntity> listOrderDetail = new ArrayList<>();
        listOrderDetail.add(orderDetail);
        OrdersEntity order = new OrdersEntity();
        
        order.setOrderDetails(listOrderDetail);
        order.setOrderDate(new Date());
        order.setTotalPrice(listOrderDetail.get(0).getPrice());
        session.setAttribute("orderBought", order);
        return "buy-product";
    }
    
    @RequestMapping(value = "/buy-product", method = RequestMethod.POST)
    public String buy(Model model, @Valid @ModelAttribute("customer") CustomerEntity customer,
            BindingResult result, HttpSession session) {
        if (result.hasErrors()) {
            model.addAttribute("customer", customer);
            model.addAttribute("gender", Gender.values());
            return "pay";
        } else {
            OrdersEntity order = (OrdersEntity) session.getAttribute("orderBought");
            ArrayList<OrdersEntity> orders = new ArrayList<>();
            orders.add(order);
            customer.setOrders(orders);
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof AccountEntity) {
                customer.setAccount((AccountEntity) principal);
            }
            try {
                order =customerService.saveOrder(customer, order);
            } catch (Exception ex) {
                customerService.deleteCustomer(order.getCustomer());
            } 
            if(order!=null && order.getId()>0){
                String subject = "Chi tiết hóa đơn";
                try {
                    mailService.sendEmail(subject,order,customer.getEmail());
                } catch (MessagingException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
                model.addAttribute("order1", order);
                session.removeAttribute("order");
                return "invoice-details";
            }
            else{
                return "mess-fail-pay";
            }
        }
    }
}
