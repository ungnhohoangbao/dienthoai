/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.controller;

import com.mycompany.jv30_project_final02.entities.CustomerEntity;
import com.mycompany.jv30_project_final02.entities.ImageEntity;
import com.mycompany.jv30_project_final02.entities.OrderDetailEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.enums.StatusOrder;
import com.mycompany.jv30_project_final02.repositories.OrderRepository;
import com.mycompany.jv30_project_final02.service.ImagesService;
import com.mycompany.jv30_project_final02.service.MailService;
import com.mycompany.jv30_project_final02.service.OrderDetailService;
import com.mycompany.jv30_project_final02.service.OrderService;
import com.mycompany.jv30_project_final02.service.ProductDetailService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Hi_XV
 */
@Controller
@RequestMapping("/manager")
public class    OrderManageController {
    @Autowired 
    OrderService orderService;
    
    @Autowired
    private MailService mailService;
    
    @Autowired
    OrderDetailService orderDetailService;
    
    @Autowired
    ImagesService imagesService;
    @Autowired
    ProductDetailService productDetailService;
    @RequestMapping("/list-orders")
    public String listOrders(Model model){
        List<OrdersEntity> orders = orderService.getOrders();
        for (OrdersEntity order : orders) {
            CustomerEntity customer = orderService.findCustomer(order.getId());
            List<OrderDetailEntity> orderDetails = orderDetailService.findListOrderDetail(order.getId());
            for (OrderDetailEntity orderDetail : orderDetails) {
                ProductEntity product = orderDetailService.findProduct(orderDetail.getId());
                orderDetail.setProduct(product);
            }
            order.setCustomer(customer);
            order.setOrderDetails(orderDetails);
        }
        Collections.sort(orders, new Comparator<OrdersEntity>() {
        @Override
        public int compare(OrdersEntity o1, OrdersEntity o2) {
        return o2.getOrderDate().compareTo(o1.getOrderDate());
        }
        });
        model.addAttribute("orders", orders);
        model.addAttribute("statusOrder", StatusOrder.values());
        return "manager/listOrders";
    }
    @RequestMapping(value = {"/update-statusOrder/{orderId}"},method = RequestMethod.POST)
    public String updateStatusOrder(Model model, @PathVariable("orderId") int orderId,
            @RequestParam("status") StatusOrder statusOrder){
        OrdersEntity order = orderService.findOrder(orderId);
        CustomerEntity customer = orderService.findCustomer(order.getId());
            List<OrderDetailEntity> orderDetails = orderDetailService.findListOrderDetail(order.getId());
            for (OrderDetailEntity orderDetail : orderDetails) {
                ProductEntity product = orderDetailService.findProduct(orderDetail.getId());
                orderDetail.setProduct(product);
            }
            order.setCustomer(customer);
            order.setOrderDetails(orderDetails);
        if(statusOrder == StatusOrder.Cancel){
            String subject = "Hủy đơn hàng";
                try {
                    mailService.cancleOrder(subject,order,order.getCustomer().getEmail());
                } catch (MessagingException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        if(statusOrder == StatusOrder.Done){
           String subject = "Chi tiết hóa đơn";
                try {
                    mailService.sendEmail(subject,order,order.getCustomer().getEmail());
                } catch (MessagingException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        
        orderService.saveOrderStatus(order, statusOrder);
        return "redirect:/manager/list-orders";
    }
    @RequestMapping("/order-Detail/{orderId}")
    public String orderDetail(Model model, @PathVariable("orderId") int orderId){
        List<OrderDetailEntity> orderDetails = orderDetailService.findListOrderDetail(orderId);
            for (OrderDetailEntity orderDetail : orderDetails) {
                ProductEntity product = orderDetailService.findProduct(orderDetail.getId());
                List<ProductDetailEntity> productDetails = productDetailService.getProductId(product.getId());
                List<ImageEntity> images = imagesService.getProductId(product.getId());
                product.setImage(images);
                product.setProductDetail(productDetails);
                orderDetail.setProduct(product);
            }
            model.addAttribute("orderDetails", orderDetails);
    return "manager/orderDetail";
    }
    @RequestMapping("/order-Search")
    public String orderSearch(Model model, @RequestParam(value = "startDate", required = false) String startDate,
            @RequestParam(value = "endDate", required = false) String endDate, HttpSession session){
        if(startDate == null || endDate == null){
            return "redirect:/manager/list-orders";
        }
        
        List<OrdersEntity> ListSearchOrders = (List<OrdersEntity>)session.getAttribute("listSearchOrders");
        if(ListSearchOrders == null){
            ListSearchOrders = new ArrayList<>();
        }
        
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
        Date startDateD;
        Date endDateD;
        try {
            startDateD = dt1.parse(startDate);
            endDateD = dt1.parse(endDate);
        } catch (ParseException ex) {
            startDateD = new Date();
            endDateD = new Date();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        List<OrdersEntity> ListOrders = orderService.ListOrdersDate(startDateD, endDateD);
         for (OrdersEntity order : ListOrders) {
            CustomerEntity customer = orderService.findCustomer(order.getId());
            List<OrderDetailEntity> orderDetails = orderDetailService.findListOrderDetail(order.getId());
            for (OrderDetailEntity orderDetail : orderDetails) {
                ProductEntity product = orderDetailService.findProduct(orderDetail.getId());
                orderDetail.setProduct(product);
            }
            order.setCustomer(customer);
            order.setOrderDetails(orderDetails);
        }
        session.setAttribute("listSearchOrders", ListOrders);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("statusOrder", StatusOrder.values());
        model.addAttribute("orders", ListOrders);
            return "manager/listOrders"; 
    }
    @RequestMapping("/listOrder-Done")
    public String listOrderDone(Model model, @RequestParam("SearchStatusOrder") StatusOrder SearchStatusOrder, HttpSession session){
        List<OrdersEntity> listOrders = orderService.getOrders();
        List<OrdersEntity> listOrderDone = new ArrayList<>();
        for (OrdersEntity listOrder : listOrders) {
            if(listOrder.getStatus() == SearchStatusOrder){
                listOrderDone.add(listOrder);
            }
        }
        for (OrdersEntity order : listOrderDone) {
            CustomerEntity customer = orderService.findCustomer(order.getId());
            List<OrderDetailEntity> orderDetails = orderDetailService.findListOrderDetail(order.getId());
            for (OrderDetailEntity orderDetail : orderDetails) {
                ProductEntity product = orderDetailService.findProduct(orderDetail.getId());
                orderDetail.setProduct(product);
            }
             order.setCustomer(customer);
            order.setOrderDetails(orderDetails);
        }
        session.setAttribute("listSearchOrders", listOrderDone);
        model.addAttribute("orders", listOrderDone);
        model.addAttribute("statusOrder", StatusOrder.values());
        return "manager/listOrders"; 
    }
}
