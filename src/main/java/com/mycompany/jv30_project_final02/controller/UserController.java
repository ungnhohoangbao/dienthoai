/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.controller;

import com.google.gson.Gson;
import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.CustomerEntity;
import com.mycompany.jv30_project_final02.entities.FavoriteEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.entities.ProducerEntity;
import com.mycompany.jv30_project_final02.entities.ProductDetailEntity;
import com.mycompany.jv30_project_final02.entities.ProductEntity;
import com.mycompany.jv30_project_final02.entities.ReviewEntity;
import com.mycompany.jv30_project_final02.enums.Gender;
import com.mycompany.jv30_project_final02.enums.StatusOrder;
import com.mycompany.jv30_project_final02.service.AccountService;
import com.mycompany.jv30_project_final02.service.FavoriteService;
import com.mycompany.jv30_project_final02.service.MailService;
import com.mycompany.jv30_project_final02.service.OrderDetailService;
import com.mycompany.jv30_project_final02.service.OrderService;
import com.mycompany.jv30_project_final02.service.ProducerService;
import com.mycompany.jv30_project_final02.service.ProductDetailService;
import com.mycompany.jv30_project_final02.service.ProductService;
import com.mycompany.jv30_project_final02.service.PromotionService;
import com.mycompany.jv30_project_final02.service.ReviewService;
import com.mycompany.jv30_project_final02.utils.ExportExcel;
import com.mycompany.jv30_project_final02.utils.HasCode;
import java.io.IOException;
import java.io.OutputStream;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author AnhLe
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private AccountService accountService;
            
    @Autowired
    private ProducerService producerService;
    
    @Autowired
    private OrderService orderService;
    
    @Autowired
    private OrderDetailService orderDetailService;
    
    @Autowired
    private MailService mailService;
    
    @Autowired
    private FavoriteService favoriteService;
    
    @Autowired
    private ProductService productService;
    
    @Autowired
    private ProductDetailService productDetailService;
    
    @Autowired
    private PromotionService promotionService;
    
    @Autowired
    private ReviewService reviewService;
    
    @RequestMapping("/home")
    public String viewHome(Model model) {
        model.addAttribute("message", "User Home Page");
        return "user/home";
    }
    
    @RequestMapping(value ="/infor",method = RequestMethod.GET)
    public String register(Model model){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity account = new AccountEntity();
        if (principal instanceof AccountEntity) {
            account = (AccountEntity)principal;
        } 
        model.addAttribute("gender", Gender.values());
        model.addAttribute("account", account);
        return "user/infor";
    }
    @RequestMapping(value ="/update-infor",method = RequestMethod.POST)
    public String updateInfor(Model model,@Valid @ModelAttribute("account") AccountEntity account,
           BindingResult result,HttpSession session){
       if(result.hasErrors()){
            model.addAttribute("account", account);
            model.addAttribute("gender",Gender.values());
            return "user/infor";
       }
       else{
           Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
           AccountEntity accounts = (AccountEntity)principal;
           AccountEntity acc = accountService.findByEmail(account.getEmail());
           if((acc == null) || (acc.getEmail().equals(accounts.getEmail()))){
                if(acc == null){
                    accounts.setEmail(account.getEmail());
                }
                accounts.setName(account.getName());
                accounts.setPhoneNumber(account.getPhoneNumber());
                accounts.setBirthDate(account.getBirthDate());
                accounts.setGender(account.getGender());
                accounts.setAddress(account.getAddress());
                accountService.save(accounts);
                model.addAttribute("mess", "success");
                model.addAttribute("account", accounts);
                model.addAttribute("gender",Gender.values());
           }
           else if(!acc.getEmail().equals(accounts.getEmail())){
                model.addAttribute("gender", Gender.values());
                model.addAttribute("account", account);
                model.addAttribute("mess", "already_exist");
           }
           return "user/infor";
       }
    }
    
    @RequestMapping(value = "/check-email",method = RequestMethod.POST)
    @ResponseBody
    public String checkEmail(@RequestBody String email){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity accounts = (AccountEntity)principal;
        AccountEntity acc = accountService.findByEmail(email);
        String checkEmail = "success";
        if(acc != null &&!acc.getEmail().equals(accounts.getEmail())){
            checkEmail = "already_exist";
        }
        Gson gson = new Gson();
        String returnString = gson.toJson(checkEmail);
        return returnString;
    }
    
    @RequestMapping(value ="/change-password",method = RequestMethod.GET)
    public String viewChangePassword(){
        return "user/change-password";
    }
    
    @RequestMapping(value ="/change-password",method = RequestMethod.POST)
    public String changePassword(Model model,
            @RequestParam("oldPass") String oldPass,
            @RequestParam("newPass") String newPass,
            @RequestParam("againPass") String againPass){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity accounts = (AccountEntity)principal;
        String stReturn = "";
        if(!HasCode.MD5Encode(oldPass).equals(accounts.getPassword())){
            stReturn = "incorrect";
            model.addAttribute("mess", stReturn);
            return "user/change-password";
        }
        else if(!newPass.equals(againPass)){
            stReturn = "not_match";
            model.addAttribute("mess", stReturn);
            return "user/change-password";
        }
        else{
            accounts.setPassword(HasCode.MD5Encode(newPass));
            accountService.save(accounts);
        }
        return "user/message-change-pass"; 
    }
    
    @RequestMapping(value ="/list-order",method = RequestMethod.GET)
    public String viewListOrder(Model model,HttpSession session){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<OrdersEntity> orders = orderService.findByAccountId(((AccountEntity)principal).getId());
        for(int i=0; i<orders.size(); i++){
            orders.get(i).setOrderDetails(orderDetailService.findByOrder(orders.get(i)));
        }
        model.addAttribute("orders", orders);
        model.addAttribute("listStatus", StatusOrder.values());
        model.addAttribute("all", "all");
        session.setAttribute("searchOrder", orders);
        return "user/list-order";
    }
    
    @ModelAttribute
    public void getCategory(Model model) {
        List<ProducerEntity> producer = producerService.getAll();
        if (producer != null && producer.size() > 0) {
            model.addAttribute("listProducer", producer);
        }else{
            model.addAttribute("listProducer", new ArrayList<>());
        }  
    }
    // continue favorite
    
    @RequestMapping(value ="/favorite",method = RequestMethod.GET)
    public String viewFavorite(Model model){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<FavoriteEntity> favorites = favoriteService.findByAccount((AccountEntity)principal);
        
        List<ProductEntity> listProduct = new ArrayList<>();
        for(FavoriteEntity f: favorites){
            listProduct.add(f.getProduct());
        }
        for (int i = 0; i < listProduct.size(); i++) {
            List<ProductDetailEntity> listProductDetail = productDetailService.findByProductIdGroupMemory(listProduct.get(i).getId());
            listProduct.get(i).setProductDetail(listProductDetail);
            listProduct.get(i).setPromotion(promotionService.findByProductIdAndDateNow(listProduct.get(i).getId()));
            listProduct.get(i).setReview(reviewService.findByProduct(listProduct.get(i)));
        }
        model.addAttribute("listProduct", listProduct);
        return "user/favorite";
    }
    
    @RequestMapping(value = "/order-detail/{id}", method = RequestMethod.GET)
    public String viewOrderDetail(@PathVariable(value = "id", required = false) int id, Model model){
        OrdersEntity order = orderService.findById(id);
        order.setOrderDetails(orderDetailService.findByOrder(order));
        model.addAttribute("order1", order);
        return "user/order-detail";
    }
    @RequestMapping(value ="/list-order/search-order",method = RequestMethod.POST)
    public String searchOrder(Model model,HttpSession session,
            @RequestParam("startDate") String startDate,
            @RequestParam("endDate") String endDate){
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
        Date startDateD;
        Date endDateD;
        try {
            startDateD = dt1.parse(startDate);
            endDateD = dt1.parse(endDate);
        } catch (ParseException ex) {
            startDateD = new Date();
            endDateD = new Date();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<OrdersEntity> orders = orderService.findByOrderDateBetweenAndAccount(startDateD, endDateD,(AccountEntity)principal);
        for(int i=0; i<orders.size(); i++){
            orders.get(i).setOrderDetails(orderDetailService.findByOrder(orders.get(i)));
        }
        model.addAttribute("orders", orders);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("listStatus", StatusOrder.values());
        model.addAttribute("all", "all");
        session.setAttribute("searchOrder", orders);
        return "user/list-order";
    }
    
    @RequestMapping(value ="/list-order/{tab}",method = RequestMethod.GET)
    public String viewOrderByStatus(@PathVariable(value = "tab", required = false) String tab,
            Model model,HttpSession session){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        StatusOrder status = StatusOrder.valueOf(tab);
        List<OrdersEntity> orders = orderService.findByStatusAndAccount(status,((AccountEntity)principal));
        for(int i=0; i<orders.size(); i++){
            orders.get(i).setOrderDetails(orderDetailService.findByOrder(orders.get(i)));
        }
        model.addAttribute("orders", orders);
        model.addAttribute("listStatus", StatusOrder.values());
        model.addAttribute("tab", tab);
        session.setAttribute("searchOrder", orders);
        return "user/list-order";
    }
    
    @RequestMapping(value ="/list-order/{tab}/search-order",method = RequestMethod.POST)
    public String searchOrderByStatus(@PathVariable(value = "tab", required = false) String tab,Model model,
            @RequestParam("startDate") String startDate,
            @RequestParam("endDate") String endDate,
            HttpSession session){
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
        Date startDateD;
        Date endDateD;
        try {
            startDateD = dt1.parse(startDate);
            endDateD = dt1.parse(endDate);
        } catch (ParseException ex) {
            startDateD = new Date();
            endDateD = new Date();
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        StatusOrder status = StatusOrder.valueOf(tab);
        List<OrdersEntity> orders = orderService.findByOrderDateBetweenAndAccountAndStatus(startDateD, endDateD,(AccountEntity)principal,status);
        for(int i=0; i<orders.size(); i++){
            orders.get(i).setOrderDetails(orderDetailService.findByOrder(orders.get(i)));
        }
        model.addAttribute("orders", orders);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("listStatus", StatusOrder.values());
        model.addAttribute("tab", tab);
        session.setAttribute("searchOrder", orders);
        return "user/list-order";
    }
    
    @RequestMapping("/cancle-order/{id}")
    @ResponseBody
    public String deleteProductDetail(@RequestBody @PathVariable("id") int id) {
        OrdersEntity order = orderService.findById(id);
        
        String status = "";
        if(order.getId()>0){
            order.setStatus(StatusOrder.Cancel);
            order = orderService.save(order);
            if(order !=null && order.getId()>0){
                String subject = "Hủy đơn hàng";
                try {
                    mailService.cancleOrder(subject,order,order.getCustomer().getEmail());
                } catch (MessagingException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
                status = "ok";
            }
            else{
                status = "fail";
            }
        }
        else{
            status = "noId";
        }
        Gson gson = new Gson();
        String returnString = gson.toJson("{\"status\":\""
            + status + "\"}");
        return returnString;
    }
    
    @RequestMapping("/add-favorite/{productId}")
    @ResponseBody
    public String addFavorite(@RequestBody @PathVariable("productId") int id) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        FavoriteEntity favorite = new FavoriteEntity();
        int product_id = 0;
        if (principal instanceof AccountEntity) {
             favorite.setAccount((AccountEntity)principal);
            ProductEntity product = productService.findById(id);
            if(product.getId()>0){
                favorite.setProduct(product);
                favorite = favoriteService.save(favorite);
                if(favorite.getId()>0){
                    product_id = favorite.getId();
                }
           }
        }
        Gson gson = new Gson();
        return gson.toJson(product_id);
    }
    
    @RequestMapping("/remove-favorite/{id}")
    @ResponseBody
    public String removeFavorite(@RequestBody @PathVariable("id") int id) {
        String check = "fail";
        if(favoriteService.deleteById(id)){
            check = "ok";
        }
        Gson gson = new Gson();
        return gson.toJson(check);
    }
    
    @RequestMapping(value = "/add-review/{id}",method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String addReview(@RequestBody @PathVariable("id") int id,@RequestBody String body) {
        String [] reviewArr = body.split(",_,");
        ReviewEntity review = new ReviewEntity();
        review.setVote(Integer.parseInt(reviewArr[0]));
        review.setContent(reviewArr[1]);
        review.setProduct(productService.findById(id));
        review.setReviewDate(LocalDate.now());
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        review.setAccount((AccountEntity)principal);
        review = reviewService.save(review);
        
        Gson gson = new Gson();
        return gson.toJson("{\"vote\":\""
                + review.getVote() + "\",\"content\":\"" + review.getContent() + 
                "\",\"name\":\"" + review.getAccount().getName() +
                "\",\"date\":\"" + review.getReviewDate() + "\"}");
    }
    
    @RequestMapping(value="/dowload-order")
    protected void processRequest(HttpServletResponse response, HttpSession session) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        List<OrdersEntity> orderList = (List<OrdersEntity>)session.getAttribute("searchOrder");
        if(orderList == null){
            orderList = new ArrayList<>();
            CustomerEntity customer = new CustomerEntity();
            for(int i = 0; i<orderList.size(); i++){
                orderList.get(i).setCustomer(customer);
            }
        }
        ExportExcel exportExcel = new ExportExcel();
        Workbook workbook = exportExcel.exportOrder(orderList);
        response.setHeader("content-disposition", "attachment; filename=Report.xls");
        response.setHeader("cache-control", "no-cache");
        
        OutputStream out = response.getOutputStream();
        workbook.write(out);
        out.close();   
    }
    
}
