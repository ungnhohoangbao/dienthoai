/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.controller;

import com.google.gson.Gson;
import com.mycompany.jv30_project_final02.entities.AccountEntity;
import com.mycompany.jv30_project_final02.entities.AccountRoleEntity;

import com.mycompany.jv30_project_final02.enums.AccountRole;
import com.mycompany.jv30_project_final02.enums.Gender;
import com.mycompany.jv30_project_final02.enums.StatusAccount;
import com.mycompany.jv30_project_final02.service.AccountService;
import com.mycompany.jv30_project_final02.utils.HasCode;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author AnhLe
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    AccountService accountService;

    @RequestMapping(value={"/home-admin","/"})
    public String viewHome(Model model) {
        List<AccountEntity> accounts = accountService.getAccounts();
        for (AccountEntity account : accounts) {
            List<AccountRoleEntity> accountRoles = accountService.getAccountRole(account.getId());
            account.setAccountRoles(accountRoles);
        }
        Collections.sort(accounts, new Comparator<AccountEntity>(){
            @Override
            public int compare(AccountEntity o1, AccountEntity o2) {
                return o2.getDateCreated().compareTo(o1.getDateCreated());
            }
        });
        model.addAttribute("accounts", accounts);
        model.addAttribute("statusAccount", StatusAccount.values());
        model.addAttribute("message", "Admin Home Page");
        return "admin/list-account";
    }
    @RequestMapping(value = {"/update-StatusAccount/{accountId}"}, method = RequestMethod.POST)
      public String updateStatus(Model model,
              @PathVariable("accountId") int accountId, @RequestParam("status") StatusAccount status){
          AccountEntity account = accountService.findAccount(accountId);
          accountService.saveAccountStatus(account, status);
          return "redirect:/admin/home-admin";
      }
    @RequestMapping("/update-role/{accountId}")
    public String updatePower(Model model, @PathVariable("accountId") int accountId){
        AccountEntity account = accountService.findAccount(accountId);
        AccountRole[] cc= AccountRole.values();
        model.addAttribute("accountRole", AccountRole.values());
        List<AccountRoleEntity> listAccountRole = accountService.getAccountRole(accountId);
        model.addAttribute("listAccountRole", listAccountRole);
        model.addAttribute("account", account);
        return "admin/update-role";
    }
    @RequestMapping(value = {"/update-role"}, method = RequestMethod.POST)
    public String saveUpdatePower(Model model, @RequestParam("id") int accountId,
            @RequestParam("ListAccountRole") List<AccountRole> listAccountRole){
        accountService.saveListAccountRole(accountId, listAccountRole);
        return "redirect:/admin/home-admin";
    }
    @RequestMapping("/add-account")
    public String addAccountAdmin(Model model){
        AccountEntity account = new AccountEntity();
        model.addAttribute("account", account);
        model.addAttribute("gender", Gender.values());
        model.addAttribute("action", "add-AccountAdmin");
        return "admin/form-account";
    }
    @RequestMapping(value = {"/add-account"}, method = RequestMethod.POST)
    public String saveAddAccountAdmin(Model model,@Valid @ModelAttribute("Account") AccountEntity account, BindingResult result){
        if(result.hasErrors()){
            model.addAttribute("account", account);
            model.addAttribute("gender", Gender.values());
            model.addAttribute("action", "add-AccountAdmin");
            return  "admin/form-account";
        } else {
            AccountEntity acc = accountService.findByEmail(account.getEmail());
            if (acc != null && acc.getId() > 0) {
                model.addAttribute("mess", "already_exist");
            } else {
                account.setPassword(HasCode.MD5Encode(account.getPassword()));
                account.setStatus(StatusAccount.Mở);
                accountService.saveAccount(account);
                return  "redirect:/admin/home-admin";
            }
        }
            model.addAttribute("account", account);
        model.addAttribute("gender", Gender.values());
        return  "admin/form-account";
    }
    @RequestMapping("/edit-AccountAdmin/{accountId}")
    public String editAccountAdmin(Model model, @PathVariable("accountId") int accountId){
        model.addAttribute("account", accountService.findAccount(accountId));
        model.addAttribute("gender", Gender.values());
        model.addAttribute("action", "edit-AccountAdmin");
        return "admin/form-account";
    }
    @RequestMapping(value = {"/edit-AccountAdmin"}, method = RequestMethod.POST)
    public String saveEditAccountAdmin(Model model, @ModelAttribute("Account") AccountEntity account){
        accountService.saveAccount(account);
        return "redirect:/admin/home-admin";
    }
    // bảo làm
    @RequestMapping(value ="/infor",method = RequestMethod.GET)
    public String register(Model model){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity account = new AccountEntity();
        if (principal instanceof AccountEntity) {
            account = (AccountEntity)principal;
        } 
        model.addAttribute("gender", Gender.values());
        model.addAttribute("account", account);
        return "admin/infor";
    }
    @RequestMapping(value ="/update-infor",method = RequestMethod.POST)
    public String updateInfor(Model model,@Valid @ModelAttribute("account") AccountEntity account,
           BindingResult result,HttpSession session){
       if(result.hasErrors()){
            model.addAttribute("account", account);
            model.addAttribute("gender",Gender.values());
            return "admin/infor";
       }
       else{
           Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
           AccountEntity accounts = (AccountEntity)principal;
           AccountEntity acc = accountService.findByEmail(account.getEmail());
           if((acc == null) || (acc.getEmail().equals(accounts.getEmail()))){
                if(acc == null){
                    accounts.setEmail(account.getEmail());
                }
                accounts.setName(account.getName());
                accounts.setPhoneNumber(account.getPhoneNumber());
                accounts.setBirthDate(account.getBirthDate());
                accounts.setGender(account.getGender());
                accounts.setAddress(account.getAddress());
                // sửa code accountService.saveAccount(accounts);
                accountService.saveAccount(accounts);
                model.addAttribute("mess", "success");
                model.addAttribute("account", accounts);
                model.addAttribute("gender",Gender.values());
           }
           else if(!acc.getEmail().equals(accounts.getEmail())){
                model.addAttribute("gender", Gender.values());
                model.addAttribute("account", account);
                model.addAttribute("mess", "already_exist");
           }
           return "admin/infor";
       }
    }
    
    @RequestMapping(value = "/check-email",method = RequestMethod.POST)
    @ResponseBody
    public String checkEmail(@RequestBody String email){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity accounts = (AccountEntity)principal;
        AccountEntity acc = accountService.findByEmail(email);
        String checkEmail = "success";
        if(acc != null &&!acc.getEmail().equals(accounts.getEmail())){
            checkEmail = "already_exist";
        }
        Gson gson = new Gson();
        String returnString = gson.toJson(checkEmail);
        return returnString;
    }
    
    @RequestMapping(value ="/change-password",method = RequestMethod.GET)
    public String viewChangePassword(){
        return "admin/change-password";
    }
    
    @RequestMapping(value ="/change-password",method = RequestMethod.POST)
    public String changePassword(Model model,
            @RequestParam("oldPass") String oldPass,
            @RequestParam("newPass") String newPass,
            @RequestParam("againPass") String againPass){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity accounts = (AccountEntity)principal;
        String stReturn = "";
        if(!HasCode.MD5Encode(oldPass).equals(accounts.getPassword())){
            stReturn = "incorrect";
            model.addAttribute("mess", stReturn);
            return "admin/change-password";
        }
        else if(!newPass.equals(againPass)){
            stReturn = "not_match";
            model.addAttribute("mess", stReturn);
            return "admin/change-password";
        }
        else{
            accounts.setPassword(HasCode.MD5Encode(newPass));
            accountService.saveAccount(accounts);
        }
        return "admin/message-change-pass";
    }
     @RequestMapping("/search-Account")
    public String searchAccount(Model model, @RequestParam("searchText") String searchText){
        List<AccountEntity> accounts = accountService.searchAccount(searchText);
        for (AccountEntity account : accounts) {
            List<AccountRoleEntity> accountRoles = accountService.getAccountRole(account.getId());
            account.setAccountRoles(accountRoles);
        }
        model.addAttribute("accounts", accounts);
        model.addAttribute("statusAccount", StatusAccount.values());
        return "admin/list-account";
    }
}
