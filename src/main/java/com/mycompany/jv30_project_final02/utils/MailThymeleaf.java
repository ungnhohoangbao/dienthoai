/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.utils;

import java.util.Locale;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;


/**
 *
 * @author HOANGBAO
 */
public class MailThymeleaf {
    @Autowired
    JavaMailSender mailSender;
     
    @Autowired
    TemplateEngine templateEngine;
    
    public void sendEmail(String subject, String message, String recipientEmail) throws MessagingException {
        Locale locale = LocaleContextHolder.getLocale();
        // Prepare the evaluation context
        Context ctx = new Context(locale);
        ctx.setVariable("message", message);
        // Prepare message using a Spring helper
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
        mimeMessageHelper.setSubject(subject);
        mimeMessageHelper.setTo(recipientEmail);
        // Create the HTML body using Thymeleaf
        String htmlContent = "";
        htmlContent = templateEngine.process("mail/email_en.html", ctx);
        mimeMessageHelper.setText(htmlContent, true);
        // Send email
        mailSender.send(mimeMessage);
    }
    
    public  void sendMail(String subject, String message1, String recipientEmail)
            throws MessagingException{
        Locale locale = LocaleContextHolder.getLocale();
        // Prepare the evaluation context
        Context ctx = new Context(locale);
        ctx.setVariable("message", message1);
        JavaMailSenderImpl mailSender1 = new JavaMailSenderImpl();
        mailSender1.setHost("smtp.gmail.com");
        mailSender1.setPort(587);
 
        mailSender1.setUsername("ungnhohoangbao113@gmail.com");
        mailSender1.setPassword("kumchihata13ctbv");
        mailSender1.setDefaultEncoding("UTF-8");
        
        Properties props = mailSender1.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        MimeMessage message = mailSender1.createMimeMessage();
        boolean multipart = true;
        MimeMessageHelper helper = new MimeMessageHelper(message, multipart, "utf-8");
        helper.setSubject(subject);
        helper.setTo(recipientEmail);
        
        String htmlContent = "";
        htmlContent = templateEngine.process("mail/email_en.html", ctx);
        helper.setText(htmlContent, true);
        mailSender1.send(message);
    }
}
