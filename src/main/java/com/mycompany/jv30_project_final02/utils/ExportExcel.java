/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.utils;

import com.mycompany.jv30_project_final02.entities.OrderDetailEntity;
import com.mycompany.jv30_project_final02.entities.OrdersEntity;
import com.mycompany.jv30_project_final02.service.OrderDetailService;
import com.mycompany.jv30_project_final02.service.OrderService;
import java.awt.Color;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author HOANGBAO
 */
public class ExportExcel {
    
    @Autowired
    OrderService orderService;
    
    @Autowired
    OrderDetailService orderDetailService;
    
    // Export of User
    public  Workbook exportOrder(List<OrdersEntity> orders){
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet =workbook.createSheet("Đơn hàng");
        
        CellStyle cellStyle = createStyleForHeader(sheet);
//        CellStyle cellStyle02 = createStyleForHeader02(sheet);
        CellStyle dateCellStyle = workbook.createCellStyle(); 
        
        Row row = sheet.createRow(0);
        Cell cell_1 = row.createCell(0);
        cell_1.setCellStyle(styleHeder(sheet));
        cell_1.setCellValue("Đơn hàng của bạn");
        cell_1.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
        cell_1.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
        sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 8));
        
        CellStyle cellStyle1 = styleTile(sheet);
        int countRow = 4,numberOfColumn;
        for(int i =0; i< orders.size(); i++){
            OrdersEntity order =orders.get(i);
            Row rowHeader = sheet.createRow(countRow);
            Cell cell = rowHeader.createCell(0); cell.setCellStyle(cellStyle); cell.setCellValue("Mã đơn hàng");
            cell = rowHeader.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Họ tên"); 
            cell = rowHeader.createCell(4); cell.setCellStyle(cellStyle); cell.setCellValue("Tên sản phẩm"); 
            cell = rowHeader.createCell(5); cell.setCellStyle(cellStyle); cell.setCellValue("Màu sắc"); 
            cell = rowHeader.createCell(6); cell.setCellStyle(cellStyle); cell.setCellValue("Số lượng"); 
            cell = rowHeader.createCell(7); cell.setCellStyle(cellStyle); cell.setCellValue("Đơn giá"); 
            cell = rowHeader.createCell(8); cell.setCellStyle(cellStyle); cell.setCellValue("Thành tiền"); 
            
            // add value order and customer
            cell = rowHeader.createCell(2); cell.setCellValue(orders.get(i).getCustomer().getName()); 
            // call auto size
            numberOfColumn = sheet.getRow(countRow).getPhysicalNumberOfCells();
            autosizeColumn(sheet, numberOfColumn+1);
            // row 2 of list order
            
            Row row2 = sheet.createRow(countRow + 1);
            Cell cellOrderId = row2.createCell(0);  cellOrderId.setCellValue(order.getId());            
            styleOrderId(row2);
            
            cell = row2.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Email:");
            cell = row2.createCell(2); cell.setCellValue(order.getCustomer().getEmail());
            Row row3 = sheet.createRow(countRow + 2);
            cell = row3.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Số điện thoại");
            cell = row3.createCell(2); cell.setCellValue(order.getCustomer().getPhoneNumber());
            Row row4 = sheet.createRow(countRow + 3);
            cell = row4.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Ngày đặt");
            cell = row4.createCell(2); cell.setCellValue(order.getOrderDateFormatted());
            Row row5 = sheet.createRow(countRow + 4);
            cell = row5.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Trạng thái");
            cell = row5.createCell(2); cell.setCellValue(order.getStatus().getStatusOrder());
            Row row6 = sheet.createRow(countRow + 5);
            cell = row6.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Phương thức thanh toán");
            cell = row6.createCell(2); cell.setCellValue("Trả tiền mặt khi nhận hàng");
            Row row7 = sheet.createRow(countRow + 6);
            cell = row7.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Tổng tiền");
            cell = row7.createCell(2); cell.setCellValue(order.getTotalPriceFormatted());
            // for product
            int j = countRow+1;
            for(int k=0; k< order.getOrderDetails().size(); k++){
                Row rowRight;
                if(k < 6){
                    rowRight = sheet.getRow(j);
                }
                else{
                    rowRight = sheet.createRow(j);
                }
                OrderDetailEntity odt = order.getOrderDetails().get(k);
                String name = odt.getProduct().getName()+ " " + odt.getProduct().getRam() + " / " +odt.getMemory();
                cell = rowRight.createCell(4); cell.setCellValue(name);
                 
                Cell cell_color = rowRight.createCell(5); 
                cell_color.setCellValue(odt.getColor());
//                CellStyle setColor = setColor(sheet,odt.getColor()); 
//                cell_color.setCellStyle(setColor);
//                cell_color.getCellStyle().set
                
                cell= rowRight.createCell(6); cell.setCellValue(odt.getQuantity());
                cell = rowRight.createCell(7); cell.setCellValue(odt.getTotalPriceFormatted());
                Locale localeVN = new Locale("vi", "VN");
                NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
                cell = rowRight.createCell(8); cell.setCellValue(currency.format(odt.getPrice()*odt.getQuantity()));
                j++;
            }
            if(order.getOrderDetails().size() < 6){
                sheet.addMergedRegion(new CellRangeAddress(countRow +1, countRow + 6, 0, 0));
                countRow += 8;
            }
            else{
                sheet.addMergedRegion(new CellRangeAddress(countRow +1, j-1, 0, 0));
                countRow = j+1;
            }
        }
        return workbook;
    }
    
    private  CellStyle createStyleForHeader(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color
        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        return cellStyle;
    }
    
    private  void styleOrderId(Row row) {
        // Create font
        Cell cellOrderId = row.getCell(0);
        cellOrderId.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
        cellOrderId.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
        
    }
    
    private  CellStyle styleTile(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 11); // font size
        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        return cellStyle;
    }
    
    private  CellStyle styleHeder(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 20); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color
        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        
//         XSSFColor myColor = new XSSFColor(new java.awt.Color(0, 73,144));
//        cellStyle.setFillForegroundColor(myColor.getIndex());
        return cellStyle;
    }
    private void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }
    
    // nhóp 
//    private  CellStyle setColor(HSSFSheet sheet,String color) {
//        // Create font
//        HSSFPalette palette = workbook.getCustomPalette();
//        sheet.getColumnStyle(0);
//        Color color = Color.decode("#d82");
//        HSSFColor myColor = palette.findSimilarColor(color.getRed(), color.getGreen(), color.getBlue());
//        // get the palette index of that color 
//        short palIndex = myColor.getIndex();
//        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
//        cellStyle.setFillBackgroundColor(Short.decode(color));
////        cellStyle.setFillForegroundColor(Short.decode(color));
//        return cellStyle;
//    }
    
    public  Workbook exportFile(List<OrdersEntity> orders){
        HSSFWorkbook  workbook = new HSSFWorkbook ();
        Sheet sheet =workbook.createSheet("Đơn hàng");
        
        CellStyle cellStyle = createStyleForHeader(sheet);
//        CellStyle cellStyle02 = createStyleForHeader02(sheet);
        CellStyle dateCellStyle = workbook.createCellStyle(); 
        
        Row row = sheet.createRow(0);
        Cell cell_1 = row.createCell(0);
        cell_1.setCellStyle(styleHeder(sheet));
        cell_1.setCellValue("Đơn hàng của bạn");
        cell_1.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
        cell_1.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
        sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 8));
        
        CellStyle cellStyle1 = styleTile(sheet);
        int countRow = 4,numberOfColumn;
        for(int i =0; i< orders.size(); i++){
            OrdersEntity order =orders.get(i);
            Row rowHeader = sheet.createRow(countRow);
            Cell cell = rowHeader.createCell(0); cell.setCellStyle(cellStyle); cell.setCellValue("Mã đơn hàng");
            cell = rowHeader.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Họ tên"); 
            cell = rowHeader.createCell(4); cell.setCellStyle(cellStyle); cell.setCellValue("Tên sản phẩm"); 
            cell = rowHeader.createCell(5); cell.setCellStyle(cellStyle); cell.setCellValue("Màu sắc"); 
            cell = rowHeader.createCell(6); cell.setCellStyle(cellStyle); cell.setCellValue("Số lượng"); 
            cell = rowHeader.createCell(7); cell.setCellStyle(cellStyle); cell.setCellValue("Đơn giá"); 
            cell = rowHeader.createCell(8); cell.setCellStyle(cellStyle); cell.setCellValue("Thành tiền"); 
            
            // add value order and customer
            cell = rowHeader.createCell(2); cell.setCellValue(orders.get(i).getCustomer().getName()); 
            // call auto size
            numberOfColumn = sheet.getRow(countRow).getPhysicalNumberOfCells();
            autosizeColumn(sheet, numberOfColumn+1);
            // row 2 of list order
            
            Row row2 = sheet.createRow(countRow + 1);
            Cell cellOrderId = row2.createCell(0);  cellOrderId.setCellValue(order.getId());            
            styleOrderId(row2);
            
            cell = row2.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Email:");
            cell = row2.createCell(2); cell.setCellValue(order.getCustomer().getEmail());
            Row row3 = sheet.createRow(countRow + 2);
            cell = row3.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Số điện thoại");
            cell = row3.createCell(2); cell.setCellValue(order.getCustomer().getPhoneNumber());
            Row row4 = sheet.createRow(countRow + 3);
            cell = row4.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Ngày đặt");
            cell = row4.createCell(2); cell.setCellValue(order.getOrderDateFormatted());
            Row row5 = sheet.createRow(countRow + 4);
            cell = row5.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Trạng thái");
            cell = row5.createCell(2); cell.setCellValue(order.getStatus().getStatusOrder());
            Row row6 = sheet.createRow(countRow + 5);
            cell = row6.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Phương thức thanh toán");
            cell = row6.createCell(2); cell.setCellValue("Trả tiền mặt khi nhận hàng");
            Row row7 = sheet.createRow(countRow + 6);
            cell = row7.createCell(1); cell.setCellStyle(cellStyle1); cell.setCellValue("Tổng tiền");
            cell = row7.createCell(2); cell.setCellValue(order.getTotalPriceFormatted());
            // for product
            int j = countRow+1;
            for(int k=0; k< order.getOrderDetails().size(); k++){
                Row rowRight;
                if(k < 6){
                    rowRight = sheet.getRow(j);
                }
                else{
                    rowRight = sheet.createRow(j);
                }
                OrderDetailEntity odt = order.getOrderDetails().get(k);
                String name = odt.getProduct().getName()+ " " + odt.getProduct().getRam() + " / " +odt.getMemory();
                cell = rowRight.createCell(4); cell.setCellValue(name);
                
                 
                Cell cell_color = rowRight.createCell(5); 
               
                cell_color.getCellStyle().setFillForegroundColor(IndexedColors.BLUE.getIndex());
//                cell_color.setCellValue(odt.getColor());
//                XSSFCellStyle setColor = setColor(sheet,odt.getColor()); 
                
                
                cell= rowRight.createCell(6); cell.setCellValue(odt.getQuantity());
                cell = rowRight.createCell(7); cell.setCellValue(odt.getTotalPriceFormatted());
                Locale localeVN = new Locale("vi", "VN");
                NumberFormat currency = NumberFormat.getCurrencyInstance(localeVN);
                cell = rowRight.createCell(8); cell.setCellValue(currency.format(odt.getPrice()*odt.getQuantity()));
                j++;
            }
            if(order.getOrderDetails().size() < 6){
                sheet.addMergedRegion(new CellRangeAddress(countRow +1, countRow + 6, 0, 0));
                countRow += 8;
            }
            else{
                sheet.addMergedRegion(new CellRangeAddress(countRow +1, j-1, 0, 0));
                countRow = j+1;
            }
            
        }
        return workbook;
    }
    
    // Export of Manager
    public Workbook exportFileManager(List<OrdersEntity> orderList) {
        Workbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet("Order Table");
        //cell style
        CellStyle cellStyle = createStyleForHeader(sheet);
        CellStyle cellStyle02 = createStyleForHeader02(sheet);
        CellStyle dateCellStyle = workbook.createCellStyle();
//        dateCellStyle.setDataFormat(
//createHelper.createDataFormat().getFormat("MMMM dd, yyyy"));

        Row row = sheet.createRow(0);

        Cell cell1 = row.createCell(0);
        cell1.setCellStyle(cellStyle);
        cell1.setCellValue("Bảng báo cáo đơn hàng");
        cell1.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
        cell1.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
        sheet.addMergedRegion(new CellRangeAddress(0, 2, 0, 7));
        
        
        Row rowHeader = sheet.createRow(4);
        Cell cell = rowHeader.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Mã đơn hàng");
        cell = rowHeader.createCell(1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Tên Khách hàng");
        cell = rowHeader.createCell(2);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Ngày đặt hàng");
        cell = rowHeader.createCell(3);
        cell.setCellStyle(cellStyle);
        cell.setCellValue("Tổng tiền");

        int i = 5, numberOfColumn;
        Row rowi;
        for (int k = 0; k < orderList.size(); k++) {
            OrdersEntity order = orderList.get(k);
            rowi = sheet.createRow(i);

            cell = rowi.createCell(0);
            cell.setCellStyle(cellStyle02);
            cell.setCellValue(order.getId());
            cell = rowi.createCell(1);
            cell.setCellStyle(cellStyle02);
            cell.setCellValue(order.getCustomer().getName());
            cell = rowi.createCell(2);
            cell.setCellStyle(cellStyle02);
            cell.setCellValue(order.getOrderDateFormatted());
            cell = rowi.createCell(3);
            cell.setCellStyle(cellStyle02);
            cell.setCellValue(order.totalPriceOrder());

            numberOfColumn = sheet.getRow(i).getPhysicalNumberOfCells();
            autosizeColumn(sheet, numberOfColumn);
            Row rowHeader02 = sheet.createRow(i + 1);

            cell = rowHeader02.createCell(2);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Tên sản phẩm");
            cell = rowHeader02.createCell(3);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Số lượng");
            cell = rowHeader02.createCell(4);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Đơn giá");
            cell = rowHeader02.createCell(5);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Màu sắc");
            cell = rowHeader02.createCell(6);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Bộ nhớ");
            cell = rowHeader02.createCell(7);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Tổng tiền");
            int p = i + 2;

            for (int j = 0; j < order.getOrderDetails().size(); j++) {
                OrderDetailEntity orderDetail = order.getOrderDetails().get(j);
                Row rowi02 = sheet.createRow(p);

                rowi02.createCell(2).setCellValue(orderDetail.getProduct().getName());
                rowi02.createCell(3).setCellValue(orderDetail.getQuantity());
                rowi02.createCell(4).setCellValue(orderDetail.getPriceFormatted());
                rowi02.createCell(5).setCellValue(orderDetail.getColor());
                rowi02.createCell(6).setCellValue(orderDetail.getMemory());
                rowi02.createCell(7).setCellValue(orderDetail.getTotalPriceFormatted());
                numberOfColumn = sheet.getRow(p).getPhysicalNumberOfCells();
                autosizeColumn(sheet, numberOfColumn);
                p++;
            }
            i = p + 1;
        }
        return workbook;
    }

    public  CellStyle createStyleForHeader02(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color
        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        return cellStyle;
    }

}
