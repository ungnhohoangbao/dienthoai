/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jv30_project_final02.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hi_XV
 */
@Component
public class UpLoadFile {

    public static List<String> upLoadImage(List<MultipartFile> files, String url) {
        List<String> urls = new ArrayList<>();
        if (files != null && files.size() > 0) {
            for (MultipartFile file : files) {

                try {
                    byte[] bytes = file.getBytes();
//           byte[] bytes1 = file1.getBytes();

                    String pathName
                            = url;
                    File dir = new File(pathName);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    String fileSource = pathName
                            + File.separator + file.getOriginalFilename();

                    File serverFile = new File(fileSource);

                    BufferedOutputStream stream
                            = new BufferedOutputStream(new FileOutputStream(serverFile));

                    stream.write(bytes);

                    stream.close();
                    urls.add(file.getOriginalFilename());

                } catch (Exception e) {
                    System.out.println(e);
                   
                }
            }

        }
        return urls;
    }
}
