$(document).ready(function () {
    // Show hide popover
    $("#txtSearch").click(function () {
        if($(this).val().trim().length ==0){
            $(this).parents(".dropdown-search").find(".dropdown-content").css("display","block");
        }
        else{
            $(".d-of-d").css("display","block");
        }
    });
    $(".p-search").click(function(){
        let p_search = $(this).find(".click-search").text().trim();
        $("#txtSearch").val(p_search);
        $(".tk").click();
    });
    let count_search_prd = 0;
    $("#txtSearch").keypress(function(e){
        $(".dropdown-content").css("display","none");
       
    }).on('keyup', function(e) {
        if (e.keyCode != 16){
            if($(this).val().trim().length == 0){
                count_search_prd = $(this).val().trim().length ;
                $(".dropdown-content").css("display","block");
                $(".d-of-d").remove();
            }
            else{
                let value = $(this).val().trim();
                if(value.length != count_search_prd){
                    count_search_prd = value.length ;
                    $(".dropdown-content").css("display","none");
                    let nameProject = window.location.pathname.split( '/' )[1];
                    var pathName = window.location.origin +"/"+ nameProject;
                    $.ajax({
                        type : "POST",
                        contentType: 'application/json; charset=utf-8',
                        url: pathName +"/search-ajax",
                        data : value,
                        dataType : 'json',
                        timeout : 100000,
                        success :function (data){ 
                            $(".d-of-d").remove();
                            for(let i =0; i< data.length;i++){
                                let a;
                                if(data[i].price == data[i].pricePromotion){
                                    a = "<span class='price-s'>"+data[i].price+"</span>";
                                }
                                else{
                                    a = "<span class='price-s'>"+data[i].pricePromotion +"</span><span class='price'> "+data[i].price+"</span>";
                                }
                                $(".dropdown-search-aj").append(
                                    "<div class='d-of-d col-sm-12' onclick =window.location.href='"+pathName+"/product-detail/"+data[i].id+"'>\n\
                                        <div class='div-image'>\n\
                                            <img src='/"+nameProject+"/resources/images/product_detail/"+ data[i].image+"' />\n\
                                        </div>\n\
                                        <div class='div-name-c'>\n\
                                            <p><b>"+data[i].name+"</b></p>\n\
                                            <p >"+a+"</p>\n\
                                        </div>\n\
                                    </div>"
                                );
                            }
                        },
                        error:function(e){
                            console.log(e);
                        }
                    });
                }
            }
        }
    });
});
$(document).mouseup(e => {
    var $trigger = $("#txtSearch");
    if (!$trigger.is(e.target)) 
    {
        $(".dropdown-content").css("display","none");
        $(".d-of-d").css("display","none");
    }
 });
