<%-- 
    Document   : updateMemory
    Created on : Dec 18, 2019, 3:53:18 PM
    Author     : Hi_XV
--%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="include/manages/css-header-manages.jsp"/>
    </head>
    <body>
    <div id="wrapper">

            <jsp:include page="include/manages/header-area-manages.jsp"/>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <h2>Khuyến mãi</h2>
                    <div class="row">
                        <mvc:form action="${pageContext.request.contextPath}/${action}" method="post" 
                                  enctype="multipart/form-data"
                                  modelAttribute="memory" class="form-horizontal">

                            <c:if test="${action == 'edit-memory'}">
                                <input hidden name="id" value="${promotion.id}" />

                            </c:if>

                            <div class="form-group">
                                <label for="nameId" class="control-label col-sm-2">
                                    Color 
                                </label>
                                <div class="col-sm-6">
                                    <input id="nameId" name="Color" class="form-control" value="${promotion.name}"/>

                                </div>
                            </div>

                            <div class="form-group" style="text-align: center">
                                <button type="submit" class="btn btn-primary">Register</button>
                            </div>
                        </mvc:form>
                    </div>
                    
                        
                    </div>
                </div>
            </div>
        
        <jsp:include page="include/manages/footer-manages.jsp"/>
    </body>
</html>
