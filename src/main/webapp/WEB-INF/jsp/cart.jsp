
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Giỏ hàng</title>
  	<meta charset="utf-8">
  	<jsp:include page="include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="include/user/header-area.jsp" />
    
    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Giỏ hàng</p>
                            <h3 class="text-color">
                                GIỎ HÀNG CỦA TÔI
                            </h3>
                        </div>
                        <div class="col-sm-12">
                            <c:forEach var="ods" items="${order.orderDetails}">
                                <c:set var="count_cart" value="${count_cart +ods.quantity}" />
                            </c:forEach>
                            <c:choose>
                                <c:when test="${count_cart == null }">
                                    <div class="gh">
                                        <img src="<c:url value="resources/images/banner/gio-hang.png" />" />
                                        <h5><b>Giỏ hàng đang trống</b></h5>
                                        <button type="button" class="btn btn-danger"
                                            onclick="window.location.href='<c:url value="/home" />'">
                                            Tiếp tục mua sắm
                                        </button>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div class="gh" style="display:none;">
                                        <img src="<c:url value="resources/images/banner/gio-hang.png" />" />
                                        <h5><b>Giỏ hàng đang trống</b></h5>
                                        <button type="button" class="btn btn-danger"
                                            onclick="window.location.href='<c:url value="/home" />'">
                                            Tiếp tục mua sắm
                                        </button>
                                    </div>
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>STT</th>
                                            <th>Ảnh</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Màu</th>
                                            <th>Số lượng</th>
                                            <th>Giá</th>
                                            <th>Đơn giá</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <c:set var="count" value="1" />
                                            <c:forEach var="odt" items="${order.orderDetails}">
                                                <tr>
                                                    <td>${count}</td>
                                                    <td>
                                                        <img src="<c:url value="/resources/images/product_detail/${odt.product.productDetail[0].image}" />" 
                                                             style="width: 50px;"/>
                                                    </td>
                                                    <td>
                                                        <a href="<c:url value="/product-detail/${odt.product.productDetail[0].id}" />">${odt.product.name} Ram ${odt.product.ram} / ${odt.memory}</a>
                                                    </td>
                                                    <td><p class="color-table" style="background: ${odt.color}"></p></td>
                                                    <td style="width: 150px;">
                                                        <input type="hidden" value="${odt.product.productDetail[0].id}" />
                                                        <input class="quantity" min="1" type="number" value="${odt.quantity}" />
                                                    </td>
                                                    <td>
                                                        <input class="price-odt" type="hidden" value="${odt.price}"/>
                                                        ${odt.priceFormatted}
                                                    </td>
                                                    <td>
                                                        <p class="total-prices"><fmt:formatNumber value="${odt.price * odt.quantity}" type="currency"/></p>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-default delete_product">
                                                            <input type="hidden" value="${odt.product.productDetail[0].id}" />
                                                            <span class="glyphicon glyphicon-trash"></span>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <c:set var="count" value="${count + 1}" />
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <div class="col-sm-12" id="thanh-toan">
                                        <button type="button" class="btn btn-danger" style="float: right;margin-left: 10px;"
                                            onclick="window.location.href='<c:url value="/pay" />'">Thanh toán
                                        </button>
                                        <p style="float: right;"><b>Tổng tiền (<span id="tt_count">${count_cart}</span> sản phẩm): </b> 
                                            <span id="tt-price">${order.totalPriceFormatted}</span>
                                        </p>
                                    </div>
                                        <div class="col-sm-12" style="margin-bottom: 10px;">
                                        <button type="button" class="btn btn-warning"
                                                onclick="window.history.go(-1)">Quay lại
                                        </button>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>
</body>
<script>
    
    $(document).ready(function(){
        $(".quantity").change(function(){
            let $this = $(this);
            var updateQ = $(this).prev().val() + "," + $(this).val();
            let quantity_this = $(this).val();
            $.ajax({
                 beforeSend: function(xhrObj){
                    xhrObj.setRequestHeader("Accept","application/json;charset=utf-8");
                },
                type : "POST",
                contentType: 'application/json;charset=utf-8',
                url: "<c:url value="/update-quantity" />",
                data:updateQ,
                dataType : 'json',
                timeout : 100000,
                success :function (data){ 
                    let check = JSON.parse(data);
                    if(check.ok != "ok"){
                        $this.siblings("p").remove();
                        $this.after("<p style='color:red'>Số lượng tối đa <br>"+check.quantity+"\
                            sản phẩm</p>");
                    }
                    else{
                        $this.siblings("p").remove();
                    }
                    $this.val(check.quantity);
                    var tt_price = $this.parents("tr").find(".price-odt").val() * $this.val();
                    $this.parents("tr").find(".total-prices").text(tt_price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +" đ");
                    $("#tt-price").text(check.totalPrice);
                    let tt_count = Number($("#of-cart").text().trim()) + Number(check.diffeQuantity);
                    $("#of-cart").text(tt_count);
                    $("#tt_count").text(tt_count);
                },
                error:function(e){
                    console.log(e);
                }
            });
        });
        
        $(".delete_product").click(function(){
            let $this = $(this);
            let productDetailId = $this.children("input").val();
            swal({
                title: "Bạn muốn xóa sản phẩm này không?",
                text: "Sau khi xóa sản phẩm sẽ k còn trên giỏ hàng của bạn nữa!!!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Có',
                cancelButtonText: 'Không',
//                closeOnConfirm: false,
                //closeOnCancel: false
                 },
                 function(isConfirm) {
                    if (isConfirm) {
                        call_delete();
                    }
            });
            function call_delete(){
                $.ajax({
                    beforeSend: function(xhrObj){
                        xhrObj.setRequestHeader("Accept","application/json;charset=utf-8");
                    },
                    type : "GET",
                    url: "<c:url value="/deleteProductDetail/" />"+productDetailId,
                    dataType : 'json',
                    timeout : 100000,
                    success :function (data){   
                        let check = JSON.parse(data);
                        $this.parents("tr").remove();
                        let tt_count = Number($("#of-cart").text().trim()) - Number(check.quantity);
                        $("#of-cart").text(tt_count);
                        $("#tt_count").text(tt_count);
                        $("#tt-price").text(check.totalPrice);
                        if(tt_count ==0){
                            $(".table").remove();
                            $("#thanh-toan").remove();
                            $(".gh").css("display","block");
                        }
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
            }
            
        });
    });

</script>
