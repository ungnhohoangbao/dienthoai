<%-- 
    Document   : listProducer
    Created on : Dec 16, 2019, 4:55:32 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/admin/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/admin/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
                    <div class="row" style="margin-bottom: 10px;">
                        <h1>Danh sách tài khoản!</h1> 
                        <div class="col-sm-8">

                        </div>
                        <div class="col-sm-4">
                            <mvc:form action="${pageContext.request.contextPath}/admin/search-Account" class="form-inline">

                                <div class="form-group">

                                    <input type="text" class="form-control" name="searchText" placeholder="Tìm kiếm khách hàng, email, số điện thoại....">

                                    <button class="btn btn-info" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </mvc:form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr class="bg-success">
                                    <th scope="col">Tên tài khoảng</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Ngày tạo</th>
                                    <th scope="col">Số điện thoại</th>
                                    <th scope="col">Giới tính</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                <c:forEach items="${accounts}" var="a" >
                                    <tr>
                                        <td><strong><a style=" color: #000; font-size: 17px"><c:out value="${a.name}"/></a></strong></td>
                                        <td><c:out value="${a.email}"/></td>
                                        <td><fmt:formatDate pattern = "dd-MM-yyyy" 
                                                        value='${a.dateCreated}' /></td>
                                        <td><c:out value="${a.phoneNumber}"/></td>
                                        <td><c:out value="${a.gender.gender}"/></td>
                                        <td>
                                            <c:if test="${a.status == 'Khóa'}">
                                                <mvc:form action="${pageContext.request.contextPath}/admin/update-StatusAccount/${a.id}" method="POST">
                                                    <c:forEach items="${statusAccount}" var="sa"  end="0">
                                                        <input hidden name="status"  value="${sa}"/>            
                                                    </c:forEach>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-success" style="margin-top: 5px; margin-left: 5px">Mở tài khoản</button>
                                                    </div>
                                                </mvc:form>
                                            </c:if>
                                            <c:set var="check_role" value="0" />
                                            <c:forEach items="${a.accountRoles}" var="r">
                                                <c:if test="${r.role == 'ROLE_ADMIN'}">
                                                    <c:set var="check_role" value="1" />
                                                </c:if>
                                            </c:forEach>
                                            <c:if test="${check_role == 1}">
                                                <p style="font-size: 20px; color: #ff6600; margin-top: 7px; margin-left: 30px">Admin</p>
                                            </c:if>
                                            <c:if test="${check_role == 0}">
                                                <c:if test="${a.status == 'Mở'}">
                                                    <mvc:form action="${pageContext.request.contextPath}/admin/update-StatusAccount/${a.id}" method="POST">
                                                        <c:forEach items="${statusAccount}" var="sa" begin="1">
                                                            <input hidden name="status"  value="${sa}"/>            
                                                        </c:forEach>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-danger" style="margin-top: 5px; margin-left: 5px">Khóa tài khoản</button>
                                                        </div>
                                                    </mvc:form>
                                                </c:if></td>
                                            </c:if>
                                        <td>
                                            <c:forEach items="${a.accountRoles}" var="r">
                                                <b><p id="text-role"><c:out value="${r.role}"/></p></b>
                                                </c:forEach>
                                            <button type="button" class="btn btn-warning"
                                                    onclick="location.href = '${pageContext.request.contextPath}/admin/update-role/${a.id}'">Quyền</button>
                                            <!--                        <button type="button" class="btn btn-success"
                                                                    onclick="location.href = '${pageContext.request.contextPath}/admin/edit-AccountAdmin/${a.id}'">update tài khoản</button>-->
                                        </td>
                                    </tr>
                                </c:forEach>   
                            </table>    
                        </div>
                    </div>
                </div>
            </div>
            <jsp:include page="../include/admin/footer-manages.jsp"/>
    </body>
</html>

