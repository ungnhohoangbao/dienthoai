<%-- 
    Document   : form-Memory
    Created on : Dec 19, 2019, 10:40:07 AM
    Author     : Hi_XV
--%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cập nhật quyền</title>
        <jsp:include page="../include/admin/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/admin/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 40px">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 ">
                            <div class="form-group" style="margin-left: 100px;">
                                <h3>Cập nhật quyền tài khoản</h3>
                                <h4>Tên tài khoản: <span style="color: #dd514c">${account.name}</span> </h4>
                                <h4>Email: <span style="color: #2E2E2E">${account.email}</span> </h4>
                            </div>
                        </div>
                        <mvc:form action="${pageContext.request.contextPath}/admin/update-role" method="post" 
                                   class="form-horizontal">
                                <input hidden name="id" value="${account.id}" />
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">
                                        Quyền
                                    </label>
                                    <div class="col-sm-10 checkbox">
                                        <c:forEach items="${accountRole}" var="ac">
                                             <div class="checkbox">
                                                <p class="color-role"><label class="container">
                                                    <c:set var="checkRole" value="0" />
                                                    <c:forEach var="lac" items="${listAccountRole}">
                                                        <c:if test="${lac.role == ac}">
                                                            <c:set var="checkRole" value="1" />
                                                        </c:if>
                                                    </c:forEach>
                                                    <c:choose>
                                                        <c:when test="${checkRole == 1}">
                                                            <input type="checkbox" name="ListAccountRole" value="${ac}" checked="checked"/><b>${ac}</b>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <input type="checkbox" name="ListAccountRole" value="${ac}"/><b>${ac}</b>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </p>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="margin-left: 180px;">
                                <button type="submit" class="btn btn-primary">Cập nhật</button>
                            </div>
                        </mvc:form> 
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../include/admin/footer-manages.jsp"/>
    </body>
</html>


