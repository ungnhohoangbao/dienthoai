
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Đổi mật khẩu</title>
  	<meta charset="utf-8">
  	<jsp:include page="../include/admin/css-header-manages.jsp" />
</head>
<body>
   <div id="wrapper">
            <jsp:include page="../include/admin/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid">
            <div class="row">
                <div class="col-sm-10">
                    <div class="col-sm-10 pr-title">
                        <div class="col-sm-10 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Đổi mật khẩu</p>
                            
                        </div>
                        <div class="col-sm-12" style="margin-bottom: 20px;">
                            <div class="login">
                                <div class="account-login">
                                    <h3 style="text-align: center;color: red;">
                                        Đổi mật khẩu
                                    </h3>
                                    <form action="${pageContext.request.contextPath}/admin/change-password" 
                                          class="login-form form-horizontal" method="POST"
                                          modelAttribute="account" class="form-update">
                                        <c:if test="${mess == 'incorrect'}">
                                            <p class="already_exist">
                                                <span class="glyphicon glyphicon-remove"></span>
                                                Mật khẩu cũ không đúng
                                            </p>
                                        </c:if>
                                        <c:if test="${mess == 'not_match'}">
                                            <p class="already_exist">
                                                <span class="glyphicon glyphicon-remove"></span>
                                                Mật khẩu không khớp nhau
                                            </p>
                                        </c:if>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="password">Mật khẩu cũ:</label>
                                            <div class="col-sm-8">
                                                <input id="password" type="password" name="oldPass" placeholder="Nhập mật khẩu cũ"  
                                                   class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="new-pass">Mật khẩu mới:</label>
                                            <div class="col-sm-8">
                                                <input id="new-pass" type="password" name="newPass" placeholder="Nhập mật khẩu mới"  
                                                   class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="again-password">Nhập lại mật khẩu:</label>
                                            <div class="col-sm-8">
                                                <input id="again-password" type="password" name="againPass" placeholder="Nhập lại mật khẩu mới"  
                                                   class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-info">Cập nhật</button>
                                            </div>
                                        </div>  
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
    </div>
    <jsp:include page="../include/admin/footer-manages.jsp"/>
    <jsp:include page="../include/admin/script-footer.jsp"/>
    <script>
        
    </script>
</body>

