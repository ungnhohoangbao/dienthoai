
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Chi tiết sản phẩm</title>
    <meta charset="utf-8">
    <jsp:include page="include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="include/user/header-area.jsp" />

    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> ${productDetail.product.name}</p>
                            <h3>
                                ${productDetail.product.name} Ram ${productDetail.product.ram}/ <span class="memory-table">${productDetail.memory.memory}</span>
                                <!--<span class="evaluate">3.4/5 <span class="glyphicon glyphicon-star"></span> 5 đánh giá</span>-->
                            </h3>
                        </div>
                        <div class="image-detail">
                            <div class="easyzoom easyzoom--adjacent">
                                <a href="<c:url value="/resources/images/product_detail/${productDetail.image}" />">
                                    <img src="<c:url value="/resources/images/product_detail/${productDetail.image}" />" width="85%" 
                                         />
                                </a>
                            </div>
                            <div style="width: 100px;">
                                <div id="my-myModal" class="my-modal">
                                    <span class="closes cursor" onclick="closeModal()">&times;</span>
                                    <div class="my-modal-content">
                                        <c:forEach var="image" items="${productDetail.product.image}">
                                            <div class="mySlides">
                                                <div class="numbertext">1 / 4</div>
                                                <img style="width: 100%; max-width: 800px;"  src="<c:url value="/resources/images/phone/${image.name}" />" >
                                            </div>
                                        </c:forEach>

                                        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                        <a class="next" onclick="plusSlides(1)">&#10095;</a>

                                        <div class="caption-container">
                                            <p id="caption"></p>
                                        </div>
                                    </div>
                                    <div class="padding-list-image">
                                        <c:set var="count_image" value="1" />
                                        <c:forEach var="image" items="${productDetail.product.image}">
                                            <div class="columns">
                                                <img class="demo cursor" src="<c:url value="/resources/images/phone/${image.name}" />" 
                                                     style="width:100%" onclick="currentSlide(${count_image})" >
                                            </div>
                                            <c:set var="count_image" value="${count_image +1}" />
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-detail-price">
                            <h3 class="name-phone-price" style="text-align: left;">
                                <c:choose>
                                    <c:when test="${fn:length(promotion) >0}">
                                        <b id="price-title" class="color-price">
                                            <c:forEach var="pr" items="${promotion}" end="0">
                                                <fmt:formatNumber value="${productDetail.price - (pr.discount* productDetail.price)/100}" type="currency" />
                                                <input id="discount" type="hidden" value="${pr.discount}" />
                                            </c:forEach>
                                        </b>
                                        <b><span class="price-detail" id="price-dis">${productDetail.priceFormatted}</span></b>
                                        </c:when>
                                        <c:otherwise>
                                        <b id="price-title" class="color-price">${productDetail.priceFormatted}</b>
                                    </c:otherwise>
                                </c:choose>
                            </h3>
                            <div>
                                <c:forEach var="list" items="${listMemory}">
                                    <div class="price-detail-ram">
                                        <p>${productDetail.product.ram} / <span class="memory-d">${list.memory}</span></p>
                                            <c:forEach var="price" items="${list.productDetail}" end="0">
                                            <p class="name-phone-price-s"><b class="color-price">${price.priceFormatted}</b></p>
                                            </c:forEach>
                                        <input class="c-memory" type="radio" name="memory"  style="display: none;">
                                        <input class="class-memory" type="hidden" value="${list.id}" />
                                        <c:choose>
                                            <c:when test="${list.id == productDetail.memory.id}">
                                                <span class="message-message glyphicon glyphicon-ok"></span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="message-message glyphicon glyphicon-ok" style="display:none;"></span>
                                            </c:otherwise>
                                        </c:choose>    
                                    </div>
                                </c:forEach>
                            </div>
                            <div style="clear: both;">
                                <a aria-label="slide" href="#">
                                    <img class="image-detail-a" src="<c:url value="/resources/images/banner/banner-product-detail.jpg" />"
                                         alt="iphone"  height="95">
                                </a>
                            </div>
                            <div class="div-color"><b>Chọn màu sắc:</b>
                                <c:set var="count" value="0" />
                                <c:set var="checkColor" value="0"/>
                                <c:forEach var="list" items="${listMemory}">
                                    <c:forEach var="price" items="${list.productDetail}">
                                        <c:choose>
                                            <c:when test="${productDetail.memory.id == list.id}">
                                                <div class="select-color loai${list.id}">
                                                    <input class="id-memory1" type="hidden" value="${list.id}" />
                                                    <input class="id-product-detail" type="hidden" value="${price.id}" />
                                                    <input class="url-image" type="hidden" value="<c:url value="/resources/images/product_detail/${price.image}" />" />
                                                    <input class="price" type="hidden"  value="${price.priceFormatted}" />
                                                    <input class="price-nf" type="hidden"  value="${price.price}" />
                                                    <c:choose>
                                                        <c:when test="${price.id == productDetail.id}">
                                                            <input class="c-color" checked type="radio" name="c-color" style="display: none;">
                                                            <span class="color colors" style="background: ${price.color.color};"><i class="glyphicon glyphicon-ok class-ok"></i></span>
                                                                <c:set var="checkColor" value="1"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <input class="c-color" type="radio" name="c-color" style="display: none;">
                                                            <span class="color" style="background: ${price.color.color};"><i style="display:none;" class="glyphicon glyphicon-ok class-ok"></i></span>
                                                            </c:otherwise>
                                                        </c:choose>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="select-color loai${list.id}" style="display:none;">
                                                    <input class="c-color" type="radio" name="c-color" style="display: none;">
                                                    <input class="id-memory1" type="hidden" value="${list.id}" />
                                                    <input class="id-product-detail" type="hidden" value="${price.id}" />
                                                    <input class="url-image" type="hidden" value="<c:url value="/resources/images/product_detail/${price.image}" />" />
                                                    <input class="price" type="hidden"  value="${price.priceFormatted}" />
                                                    <input class="price-nf" type="hidden"  value="${price.price}" />
                                                    <span class="color" style="background: ${price.color.color};"><i style="display:none;" class="glyphicon glyphicon-ok class-ok"></i></span>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </c:forEach>
                                <!--glyphicon glyphicon-heart-empty glyphicon glyphicon-heart favorite-->
                                <sec:authorize access="isAuthenticated()">
                                    <input id="product_id" type="hidden" value="${productDetail.product.id}" />
                                    <c:choose>
                                        <c:when test="${favorite.id>0}">
                                            <input id="favorite_id" type="hidden" value="${favorite.id}" />
                                            <span class="glyphicon glyphicon-heart favorite-color" id="favorite" style="color:red;"></span>
                                            <p class="lyt"><b>Yêu thích:</b></p>
                                        </c:when>
                                        <c:otherwise>
                                            <input id="favorite_id" type="hidden" value="0" />
                                            <span class="glyphicon glyphicon-heart-empty favorite" id="favorite"></span>
                                            <p class="lyt"><b>Yêu thích:</b></p>
                                        </c:otherwise>
                                    </c:choose>
                                </sec:authorize>
                            </div><br>
                            <div style="clear: both;">
                                <div class="bought" id="bought" onclick="window.location.href = '<c:url value="/buy-product/${productDetail.id}" />'">
                                    <h3>MUA NGAY</h3>
                                    <p><b>Miễn phí giao hàng tận nơi</b></p>
                                </div>
                                <div class="bought" style="background: #f7941d;" onclick="order()">
                                    <h3 >Thêm vào giỏ hàng
                                        <span style="top: 5px;" class="glyphicon glyphicon-shopping-cart"></span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="right-pd">
                            <div class="right-detail">
                                <p class="ckbh">
                                    <b>Cam kết bán hàng</b>
                                </p>
                                <div style="padding:0 10px;">
                                    <p><span class="cam-ket glyphicon glyphicon-ok"></span>Trả góp 0% khi mua sản phẩm tại cửa hàng</p>
                                    <p><span class="cam-ket glyphicon glyphicon-ok"></span>Bảo hành 6 tháng. 1 đổi 1 trang vòng 30 ngày</p>
                                    <p><span class="cam-ket glyphicon glyphicon-ok"></span>Mua trả góp chỉ cần CMND + GPLX. Thủ tục đơn giản 15 phút nhận máy.</p>
                                    <p><span class="cam-ket glyphicon glyphicon-ok"></span>Miễn phí giao hàng tận nới khi mua hàng online</p>
                                    <p><span class="cam-ket glyphicon glyphicon-ok"></span>Miễn phí 1 lần nâng cấp trong 07 ngày.</p>
                                    <p><span class="cam-ket glyphicon glyphicon-ok"></span>Bộ sản phẩm gồm các phụ kiện được cửa hàng tặng kèm.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div>
                                    <button type="button" onclick="openModal();currentSlide(1)"
                                            style="padding:5px 20px;">
                                        <i style="font-size: 30px;" class="fa fa-picture-o" aria-hidden="true"></i>
                                    </button>
                                    <p>Bộ ảnh ${fn:length(productDetail.product.image)} </p>
                                </div>
                                <p><b>Thông tin sản phẩm: </b>${productDetail.product.content}</p>
                            </div>
                            <div class="col-sm-6" >
                                <h3>Thông số kỹ thuật</h3>
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td style="width: 150px;">Màn hình</td>
                                            <td>${productDetail.product.screen}</td>
                                        </tr>
                                        <tr>
                                            <td>Hệ điều hành</td>
                                            <td>${productDetail.product.operatingSystem}</td>
                                        </tr>
                                        <tr>
                                            <td>Camera trước</td>
                                            <td>${productDetail.product.rearCamera}</td>
                                        </tr>
                                        <tr>
                                            <td>Camera sau</td>
                                            <td>${productDetail.product.frontCamera}</td>
                                        </tr>
                                        <tr>
                                            <td>Dung lượng pin</td>
                                            <td>${productDetail.product.batteryCapacity}</td>
                                        </tr>
                                        <tr>
                                            <td>Ram</td>
                                            <td>${productDetail.product.ram}</td>
                                        </tr>
                                        <tr>
                                            <td>Bộ nhớ trong</td>
                                            <td class="memory-table">${productDetail.memory.memory}</td>
                                        </tr>
                                        <tr>
                                            <td>Màu</td>
                                            <td><p class="color-table" style="background: ${productDetail.color.color}"></p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-12" style="padding:0;border-top: 1px solid #BDBDBD;">
                            <div class="col-sm-12">
                                <h3>Sản phẩm tương tự</h3>
                                <c:forEach var="pd" items="${listProductDetail}">
                                    <div class="items-phone" onclick="window.location.href = '<c:url value="/product-detail/${pd.id}" />'"
                                         style="width:16.66%;" >
                                        <div class="div-of-items-phone" style="background: #fff;">
                                            <div class="phone-div" >
                                                <img src="<c:url value="/resources/images/product_detail/${pd.image}" />">
                                                <!-- <p class="discount ">Gi?m 3000000 VN?</p> -->
                                            </div>
                                            <p class="name-phone">${pd.product.name} Ram <span style="white-space: nowrap;">${pd.product.ram}</span>/ 32GB</p>
                                            <p class="name-phone-price">
                                                <c:choose>
                                                    <c:when test="${fn:length(pd.product.promotion) >0}">
                                                        <c:forEach var="pr" items="${pd.product.promotion }" end="0">
                                                            <b class="color-price">
                                                                <fmt:formatNumber value="${pd.price - (pr.discount* pd.price)/100}" type="currency" />
                                                            </b>
                                                        </c:forEach>
                                                        <span class="price"> ${pd.priceFormatted}</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <b class="color-price">
                                                            ${pd.priceFormatted}
                                                        </b>
                                                    </c:otherwise>
                                                </c:choose>
                                            </p>
                                            <p class="evaluate">${fn}  3.4/5 <span class="glyphicon glyphicon-star"></span> 5 đánh giá</p>
                                        </div>
                                    </div>
                                </c:forEach>
                                <div  style="border-top: 1px solid #BDBDBD;clear: both;">
                                    <div class="col-sm-8" style="padding:0px;">
                                        <h4 >
                                            Đánh giá và nhận xét ${productDetail.product.name} 
                                        </h4>
                                        <div class="col-sm-12 revew" >
                                            <div class="col-sm-3 left-revew">
                                                <c:set var="rv_vote5" value="0" />
                                                <c:set var="rv_vote4" value="0" />
                                                <c:set var="rv_vote3" value="0" />
                                                <c:set var="rv_vote2" value="0" />
                                                <c:set var="rv_vote1" value="0" />
                                                <c:forEach var="v" items="${riviews}">
                                                    <c:set var="count_rivew" value="${v.vote + count_rivew}" />
                                                    <c:if test="${v.vote == 1}">
                                                        <c:set var="rv_vote1" value="${rv_vote1 + 1}" />
                                                    </c:if>
                                                    <c:if test="${v.vote == 2}">
                                                        <c:set var="rv_vote2" value="${rv_vote2 + 1}" />
                                                    </c:if>
                                                    <c:if test="${v.vote == 3}">
                                                        <c:set var="rv_vote3" value="${rv_vote3 + 1}" />
                                                    </c:if>
                                                    <c:if test="${v.vote == 4}">
                                                        <c:set var="rv_vote4" value="${rv_vote4 + 1}" />
                                                    </c:if>
                                                    <c:if test="${v.vote == 5}">
                                                        <c:set var="rv_vote5" value="${rv_vote5 + 1}" />
                                                    </c:if>
                                                </c:forEach>
                                                <c:if test="${fn:length(riviews) == 0}">
                                                    <c:set var="below" value="1" />
                                                </c:if>
                                                <c:if test="${fn:length(riviews) > 0}">
                                                    <c:set var="below" value="${fn:length(riviews)}" />
                                                </c:if>
                                                <input type="hidden" id="sum-rv" value="${count_rivew}" />
                                                <h3 class="t-aling"><span class="avg-rv"><fmt:formatNumber type="number" pattern="###.#" value="${count_rivew /below}" /></span>/5 <span class="glyphicon glyphicon-star color-revew"></span></h3>
                                                <p class="t-aling"><span class="length-rv">${fn:length(riviews)}</span> đánh giá</p>
                                            </div>
                                            <div class="col-sm-6 center-rivew">
                                                <div class="item-review">
                                                    <p class="five-rv">5 <span class="glyphicon glyphicon-star color-revew"></span></p>
                                                    <div class="myProgress">
                                                        <div class="myBar" style="  width: ${(rv_vote5*100)/below}%;"></div>
                                                    </div>
                                                    <p class="rv-ds"><span class="rvs">${rv_vote5}</span> đánh giá</p>
                                                </div>
                                                <div class="item-review">
                                                    <p class="five-rv">4 <span class="glyphicon glyphicon-star color-revew"></span></p>
                                                    <div class="myProgress">
                                                        <div class="myBar" style="  width: ${(rv_vote4*100)/below}%;"></div>
                                                    </div>
                                                    <p class="rv-ds"><span class="rvs">${rv_vote4}</span>  đánh giá</p>
                                                </div>
                                                <div class="item-review">
                                                    <p class="five-rv">3 <span class="glyphicon glyphicon-star color-revew"></span></p>
                                                    <div class="myProgress">
                                                        <div class="myBar" style="  width: ${(rv_vote3*100)/below}%;"></div>
                                                    </div>
                                                    <p class="rv-ds"><span class="rvs">${rv_vote3}</span>  đánh giá</p>
                                                </div>
                                                <div class="item-review">
                                                    <p class="five-rv">2 <span class="glyphicon glyphicon-star color-revew"></span></p>
                                                    <div class="myProgress">
                                                        <div class="myBar" style="  width: ${(rv_vote2*100)/below}%;"></div>
                                                    </div>
                                                    <p class="rv-ds"><span class="rvs">${rv_vote2}</span>  đánh giá</p>
                                                </div>
                                                <div class="item-review">
                                                    <p class="five-rv">1 <span class="glyphicon glyphicon-star color-revew"></span></p>
                                                    <div class="myProgress">
                                                        <div class="myBar" style="  width: ${(rv_vote1*100)/below}%;"></div>
                                                    </div>
                                                    <p class="rv-ds"><span class="rvs">${rv_vote1}</span>  đánh giá</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-3" style="">
                                                <p class="are-like">Sản phẩm được nhiều người sử dụng!!!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <sec:authorize access="isAuthenticated()">
                                        <c:if test="${checkComment == 'checkComment'}">
                                            <div class="col-sm-8 send-revew">
                                                <div>
                                                    <h5 class="s-select">Chọn đánh giá của bạn</h5>
                                                    <h3 class="list-sao">
                                                        <span class="glyphicon glyphicon-star item-sao"></span>
                                                        <span class="glyphicon glyphicon-star item-sao"></span>
                                                        <span class="glyphicon glyphicon-star item-sao"></span>
                                                        <span class="glyphicon glyphicon-star item-sao"></span>
                                                        <span class="glyphicon glyphicon-star item-sao"></span>
                                                    </h3>
                                                    <p class="chon-dg">Vui lòng chọn đánh giá của bạn</p>
                                                </div>
                                                <div>
                                                    <textarea name="txtComment" placeholder="Nhập nội dung nhận xét tại đây." 
                                                              id="txtComment" class="textarea"></textarea>
                                                    <button type="button" class="btn btn-primary btn-send-rv">Gửi đánh giá</button>
                                                    <p class="content-rv">Vui lòng nhập nội dụng đánh giá sản phẩm</p>
                                                </div>
                                            </div>
                                        </c:if>
                                    </sec:authorize>
                                    <div class="col-sm-8 list-comment">
                                        <div class="item-comment" id="clone-review" style="display:none;">
                                            <p><b class="reivew-name"></b> <span class="boughted glyphicon glyphicon-ok"></span> <span style="color:#088A08;">Đã mua hàng tại của hàng</span></p>
                                            <p>
                                                <span id="before-rv"></span>
                                                <span class="content-riview"></span>
                                            </p>
                                            <p><span class="blue-date"> Ngày đánh giá:</span> <span class="grey-date"></span> </p>
                                        </div>
                                        <c:forEach var="r" items="${riviews}">
                                            <div class="item-comment">
                                                <p><b class="reivew-name">${r.account.name} </b> <span class="boughted glyphicon glyphicon-ok"></span> <span style="color:#088A08;">Đã mua hàng tại của hàng</span></p>
                                                <p>
                                                    <c:forEach var="i" begin="1" end="5">
                                                        <c:choose>
                                                            <c:when test="${i <= r.vote}">
                                                                <span class="glyphicon glyphicon-star color-revew"></span>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <span class="glyphicon glyphicon-star color-revew-s"></span>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                    <span class="content-riview">${r.content}</span>
                                                </p>
                                                <p><span class="blue-date"> Ngày đánh giá:</span><span class="grey-date"> ${r.reviewDate}</span> </p>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>

</body>
<script>
    function openModal() {
        document.getElementById("my-myModal").style.display = "block";
    }

    function closeModal() {
        document.getElementById("my-myModal").style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        captionText.innerHTML = dots[slideIndex - 1].alt;
    }
</script>
<script>
    //    var $easyzoom = $('.easyzoom').easyZoom();
    //    // Setup thumbnails example
    //    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');
    //
    //    $('.thumbnails').on('click', 'a', function(e) {
    //        var $this = $(this);
    //        e.preventDefault();
    //        api1.swap($this.data('standard'), $this.attr('href'));
    //    });

    $(document).ready(function () {
        let discount = $("#discount").val();
        $(".price-detail-ram").click(function () {
            var count = $(".price-detail-ram").length;
            $(this).find(".c-memory").prop('checked', true);
            for (var i = 0; i < count; i++) {
                if ($(".price-detail-ram").find(".c-memory")[i].checked == true) {
                    $(".price-detail-ram").find(".message-message").eq(i).css("display", "");
                    var price = $(this).find(".color-price").text();
                    var id_memory2 = $(this).find(".class-memory").val();
                    var array_id = $(".class-memory");
                    for (var j = 0; j < array_id.length; j++) {
                        if (id_memory2 != array_id.eq(j).val()) {
                            var st_loai = ".loai" + array_id.eq(j).val();
                            $(st_loai).css("display", "none");
                        }
                    }
                    var st_loai = ".loai" + id_memory2;
                    $(st_loai).css("display", "");
                    $(st_loai).eq(0).click();
                    var memory_table = $(this).find(".memory-d").text();
                    $(".memory-table").text(memory_table);
                } else {
                    $(".price-detail-ram").find(".message-message").eq(i).css("display", "none");
                }
            }
        });

        $(".select-color").click(function () {
            var count = $(".select-color").length;
            $(this).find(".c-color").prop('checked', true);
            for (var i = 0; i < count; i++) {
                if ($(".select-color").find(".c-color")[i].checked == true) {
                    $(".select-color").find(".class-ok").eq(i).css("display", "");
                    $(".select-color").find(".color").eq(i).attr("class", "color colors");
                    var url_image = $(this).find(".url-image").val();
                    $(".easyzoom").find("a").attr("href", url_image);
                    $(".easyzoom").find("img").attr("src", url_image);
                    var price = $(this).find(".price").val();
                    var price_nf = $(this).find(".price-nf").val();
                    if (discount > 0) {
                        $("#price-dis").text(price);
                        console.log(price);
                        $("#price-title").text((price_nf - (discount * price_nf) / 100).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') + " đ");
                    } else {
                        $("#price-title").text(price);
                    }
                    var color = $(this).find(".color").css("background");
                    $(".color-table").css("background", color);
                    $("#bought").attr("onclick", "window.location.href='<c:url value="/buy-product/" />" + $(".select-color").eq(i).find(".id-product-detail").val() + "'");
                } else {
                    $(".select-color").find(".class-ok").eq(i).css("display", "none");
                    $(".select-color").find(".color").eq(i).removeClass("colors");
                }
            }
        });

        // Đánh giá
        let index_this = 0;
        let count_its = $(".item-sao").length;
        let check_click = false;
        $(".item-sao").hover(function () {
            $(".item-sao").css("color", "#BDBDBD");
            let number_this = $(".item-sao").index($(this));
            for (let i = 0; i <= number_this; i++) {
                $(".item-sao").eq(i).css("color", "#fd9727");
            }
        }, function () {
            $(".item-sao").css("color", "#fd9727");
            for (let i = count_its; i > index_this; i--) {
                if (check_click) {
                    $(".item-sao").eq(i).css("color", "#BDBDBD");
                } else {
                    $(".item-sao").eq(i - 1).css("color", "#BDBDBD");
                }
            }
        });
        $(".item-sao").click(function () {
            $(".chon-dg").css("display", "none");
            check_click = true;
            index_this = $(".item-sao").index($(this));
            for (let i = 0; i <= index_this; i++) {
                $(".item-sao").eq(i).css("color", "#fd9727");
            }
        });

        // ajax đánh giá
        $(".btn-send-rv").click(function () {
            if (!check_click) {
                $(".chon-dg").css("display", "block");
            } else {
                let content = $("#txtComment").val().trim();
                if (content.length > 0) {
                    let vote = index_this + 1;
                    let favorite = vote + ",_," + content;
                    let product_id = $("#product_id").val();
                    $.ajax({
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        url: "<c:url value="/user/add-review/" />" + product_id,
                        data: favorite,
                        dataType: 'json',
                        timeout: 100000,
                        success: function (data) {
                            let rivew = JSON.parse(data);
                            handlingRiview(rivew);
                        },
                        error: function (e) {
                            console.log(e);
                        }
                    });
                } else {
                    $(".content-rv").css("display", "block");
                }
            }
        });
        function handlingRiview(rivew) {
            $(".content-rv").css("display", "none");
            // remove vote
            check_click = false;
            $("#txtComment").val("");
            index_this = 0;
            for (let i = 0; i <= 4; i++) {
                $(".item-sao").eq(i).css("color", "#BDBDBD");
            }

            let clone_review = $("#clone-review").clone(true, true);
            clone_review.css("display", "block");
            clone_review.removeAttr("id");
            clone_review.find(".reivew-name").text(rivew.name);
            clone_review.find(".content-riview").text(rivew.content);
            clone_review.find(".grey-date").text(rivew.date);
            let vote_nb = Number(rivew.vote);
            for (let i = 1; i <= 5; i++) {
                if (i <= vote_nb) {
                    clone_review.find("#before-rv").before("<span class='glyphicon glyphicon-star color-revew'></span>");
                } else {
                    clone_review.find("#before-rv").before("<span class='glyphicon glyphicon-star color-revew-s'></span>");
                }
            }
            clone_review.find("#before-rv").remove();
            $("#clone-review").after(clone_review);
            let length_rv = Number($(".length-rv").text().trim());
            $(".length-rv").text(length_rv + 1);
            let sum_rv = Number($("#sum-rv").val());
            $(".avg-rv").text(((sum_rv + vote_nb) / (length_rv + 1)).toFixed(1));
            $("#sum-rv").val(sum_rv + vote_nb);

            // change myBar width: ${(rv_vote1*100)/below}%;
            let indext_rv = 0;
            for (let i = 5; i >= 1; i--) {
                if (i == vote_nb) {
                    let width_nb = Number($(".rvs").eq(indext_rv).text().trim()) + 1;
                    let width_st = "" + (width_nb * 100) / (length_rv + 1) + "%";
                    $(".myBar").eq(indext_rv).css("width", width_st);
                    $(".rvs").eq(indext_rv).text(width_nb);
                }
                //                else{
                //                    let width_nb = Number($(".rvs").eq(indext_rv).text().trim());
                //                    let width_st = ""+(width_nb-1 *100)/(length_rv +1) +"%";
                //                    $(".myBar").eq(indext_rv).css("width",width_st);
                //                    $(".rvs").eq(indext_rv).text(width_nb +1);
                //                }
                indext_rv += 1;
            }
        }
        // click yêu thích
        let check_favorite = false;
        let check_id_fa = false;
        let check_rq_id_fa = 0;
        $("#favorite").click(function () {
            let id_favorite = $(this).prev("#favorite_id").val();
            if (id_favorite > 0 && !check_id_fa) {
                check_favorite = true;
                check_id_fa = true;
                check_rq_id_fa = id_favorite;
            }
            let $this = $(this);
            if (check_favorite) {
                $.ajax({
                    type: "GET",
                    url: "<c:url value="/user/remove-favorite/" />" + check_rq_id_fa,
                    dataType: 'json',
                    timeout: 100000,
                    success: function (data) {
                        if (data == 'ok') {
                            $this.removeClass("glyphicon glyphicon-heart favorite-color");
                            $this.addClass("glyphicon glyphicon-heart-empty favorite");
                            $this.css("color", "#000");
                            check_favorite = false;
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            } else {
                let product_id = $("#product_id").val();
                $.ajax({
                    type: "GET",
                    url: "<c:url value="/user/add-favorite/" />" + product_id,
                    dataType: 'json',
                    timeout: 100000,
                    success: function (data) {
                        let request_id_fa = Number(data);
                        if (request_id_fa > 0) {
                            check_rq_id_fa = request_id_fa;
                            $this.removeClass("glyphicon glyphicon-heart-empty favorite");
                            $this.addClass("glyphicon glyphicon-heart favorite-color");
                            $this.css("color", "red");
                            check_favorite = true;
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            }
        });
    });
    function order() {
        var id = 0;
        var count = $(".select-color").length;
        for (var i = 0; i < count; i++) {
            if ($(".select-color").find(".c-color")[i].checked == true) {
                id = $(".select-color").eq(i).find(".id-product-detail").val();
            }
        }
        $.ajax({
            type: "GET",
            url: "<c:url value="/order/" />" + id,
            dataType: 'json',
            timeout: 100000,
            success: function (data) {
                var count_cart = Number($("#of-cart").text().trim());
                $("#of-cart").text(count_cart + 1);
                swal(
                        'Thêm thành công',
                        'Điện thoại của bạn đã được thêm vào giỏ hàng.',
                        'success'
                        );
            },
            error: function (e) {
                console.log(e);
            }
        });

    }
</script>