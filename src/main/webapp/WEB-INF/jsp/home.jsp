<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home</title>
        <meta charset="utf-8">
        <jsp:include page="include/user/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/user/header-area.jsp" />
        <div style="margin:10px;">
            <div class="container" >
                <div class="row">
                    <div class="col-sm-8">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel" >
                            <ol class="carousel-indicators" style="z-index: 1;position: absolute;">
                                <li data-target="#myCarousel"  data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="<c:url value="resources/images/banner/banner1.png" />" >
                                </div>
                                <div class="item">
                                    <img src="<c:url value="resources/images/banner/banner2.png" />">
                                </div>
                                <div class="item">
                                    <img src="<c:url value="resources/images/banner/banner3.png" />">
                                </div>
                                <div class="item">
                                    <img src="<c:url value="resources/images/banner/banner4.png" />">
                                </div>
                            </div>
                            <!-- Left and right controls -->
                            <a class="left carousel-control"  href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4" >
                        <div class="twobanner">
                            <a aria-label="slide" href="#" style="background: #fff;">
                                <img style="cursor:pointer;float: left;" src="<c:url value="resources/images/banner/banner-right3.png" />"
                                     alt="iphone" width="18%" height="60">
                                <span class="span-of-a">Năm sau, iPhone SE 2 giá rẻ có thể trình làng với tên gọi iPhone 9, đi kèm với vi xử</span>
                            </a>
                            <a aria-label="slide" href="#">
                                <img class="image-detail-s" src="<c:url value="resources/images/banner/banner-right1.png" />"
                                     alt="iphone" >
                            </a>
                            <a aria-label="slide" href="#" >
                                <img class="image-detail-s" src="<c:url value="resources/images/banner/banner-right2.png" />"
                                     alt="2017 - T9 - Big Samsung">
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin:10px;">
            <div class="container" >
                <div class="row">
                    <div class="col-sm-12">
                        <a aria-label="slide" href="#">
                            <img style="cursor:pointer;" src="<c:url value="resources/images/banner/banner-footer.png" />"
                                 alt="iphone" width="100%" >
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div style="margin:10px;">
            <div class="container" >
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-12 pr-title">
                            <div class="col-sm-12 title-content">
                                <h4><b>Sản phẩm mới</b></h4>
                            </div>
                            <c:forEach var="p" items="${listProduct}">
                                <div class="items-phone" onclick="window.location.href='<c:url value="/product-detail/${p.productDetail[0].id}" />'">
                                    <div class="div-of-items-phone">
                                        <div class="phone-div" >
                                            <img src="<c:url value="/resources/images/product_detail/${p.productDetail[0].image}" />">
                                        </div>
                                        <p class="name-phone">${p.name} Ram <span style="white-space: nowrap;">${p.ram}</span>/ 32GB</p>
                                        <p class="name-phone-price">
                                            <c:choose>
                                                <c:when test="${fn:length(p.promotion) >0}">
                                                    <c:forEach var="pr" items="${p.promotion }" end="0">
                                                     <b class="color-price">
                                                            <fmt:setLocale value = "vi_VN" scope="session"/>
                                                            <fmt:formatNumber value="${p.productDetail[0].price - (pr.discount* p.productDetail[0].price)/100}" type="currency" />
                                                        </b>
                                                    </c:forEach>
                                                    <span class="price"> ${p.productDetail[0].priceFormatted}</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <b class="color-price">
                                                        ${p.productDetail[0].priceFormatted}
                                                    </b>
                                                </c:otherwise>
                                            </c:choose>
                                        </p>
                                        <c:set var="count_review" value="0" />
                                        <c:forEach var="v" items="${p.review}">
                                            <c:set var="count_review" value="${v.vote + count_review}" />
                                        </c:forEach>
                                        <c:if test="${fn:length(p.review) == 0}">
                                            <c:set var="below" value="1" />
                                        </c:if>
                                        <c:if test="${fn:length(p.review) > 0}">
                                            <c:set var="below" value="${fn:length(p.review)}" />
                                        </c:if>
                                        <p class="evaluate">
                                            <fmt:formatNumber type="number" pattern="###.#" value="${count_review /below}" />/5 
                                            <span class="glyphicon glyphicon-star"></span> ${fn:length(p.review)} đánh giá
                                        </p>
                                    </div>
                                </div>
                            </c:forEach>
                            <div class="col-sm-12 title-content2">
                                <h4><b>Sản phẩm hot</b> </h4>
                            </div>
                            <c:forEach var="p" items="${listProduct2}">
                                <div class="items-phone" onclick="window.location.href='<c:url value="/product-detail/${p.productDetail[0].id}" />'">
                                    <div class="div-of-items-phone">
                                        <div class="phone-div" >
                                            <img src="<c:url value="/resources/images/product_detail/${p.productDetail[0].image}" />">
                                        </div>
                                        <p class="name-phone">${p.name} Ram <span style="white-space: nowrap;">${p.ram}</span>/ 32GB</p>
                                        <p class="name-phone-price">
                                            <c:choose>
                                                <c:when test="${fn:length(p.promotion) >0}">
                                                    <c:forEach var="pr" items="${p.promotion }" end="0">
                                                        <b class="color-price">
                                                            <fmt:formatNumber value="${p.productDetail[0].price - (pr.discount* p.productDetail[0].price)/100}" type="currency" />
                                                        </b>
                                                    </c:forEach>
                                                    <span class="price"> ${p.productDetail[0].priceFormatted}</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <b class="color-price">
                                                        ${p.productDetail[0].priceFormatted}
                                                    </b>
                                                </c:otherwise>
                                            </c:choose>
                                        </p>
                                        <c:set var="count_review" value="0" />
                                        <c:forEach var="v" items="${p.review}">
                                            <c:set var="count_review" value="${v.vote + count_review}" />
                                        </c:forEach>
                                        <c:if test="${fn:length(p.review) == 0}">
                                            <c:set var="below" value="1" />
                                        </c:if>
                                        <c:if test="${fn:length(p.review) > 0}">
                                            <c:set var="below" value="${fn:length(p.review)}" />
                                        </c:if>
                                        <p class="evaluate">
                                            <fmt:formatNumber type="number" pattern="###.#" value="${count_review /below}" />/5 
                                            <span class="glyphicon glyphicon-star"></span> ${fn:length(p.review)} đánh giá
                                        </p>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>
</body>
</html>