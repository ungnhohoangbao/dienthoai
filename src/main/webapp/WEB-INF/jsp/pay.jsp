
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Thanh toán</title>
  	<meta charset="utf-8">
  	<jsp:include page="include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="include/user/header-area.jsp" />
    
    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Thanh toán</p>
                        </div>
                        <div class="col-sm-12" style="margin-bottom: 20px;">
                            <div class="thanht col-sm-12">
                                <h3 style="text-align: center;color: red;">
                                    Thanh toán
                                </h3>
                                <div class="div-tt">
                                    <h4 style="text-align: center;">
                                        Thông tin thanh toán
                                    </h4>
                                    <mvc:form action="${pageContext.request.contextPath}/pay" 
                                              class="login-form form-horizontal" method="POST"
                                              modelAttribute="customer">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="email">Email:</label>
                                             <div class="col-sm-8">
                                                 <input id="email" type="email" name="email" placeholder="Nhập email" 
                                                        class="form-control" required value="${customer.email}">
                                                 <mvc:errors path="email" cssClass="requied" />
                                             </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="name">Họ tên:</label>
                                            <div class="col-sm-8">
                                                <input id="name" type="text" name="name" placeholder="Nhập họ tên" 
                                                       class="form-control" required value="${customer.name}">
                                                <mvc:errors path="name" cssClass="requied" />
                                            </div>
                                             <mvc:errors path="name" cssClass="requied" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="phone">Số điện thoại:</label>
                                            <div class="col-sm-8">
                                                <input id="phone" type="number" name="phoneNumber" placeholder="Nhập số điện thoại" 
                                                       class="form-control" required value="${customer.phoneNumber}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="address">Địa chỉ nhận hàng:</label>
                                            <div class="col-sm-8">
                                                <input id="address" type="text" name="address" placeholder="Nhập địa chỉ"  
                                                       class="form-control" required value="${customer.address}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button type="submit" id="submit" class="btn btn-login">Thanh toán</button>
                                            </div>
                                        </div>  
                                    </mvc:form>
                                </div>
                                <div class="div-tt">
                                    <h4 style="text-align: center;">
                                        Đơn hàng của bạn
                                    </h4>
                                    <table class="table table-hover login-form" style="margin-left: 40px;">
                                        <thead>
                                          <tr>
                                            <th>Sản phẩm</th>
                                            <th>Tổng</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="odt" items="${order.orderDetails}">
                                                <tr>
                                                    <td>
                                                        <p>${odt.product.name} Ram ${odt.product.ram} / ${odt.memory}</p>
                                                        <p>
                                                            Màu <span class="color-tt" style="background: ${odt.color}"></span>
                                                            <span class="glyphicon glyphicon-remove" style="margin-left: 10px;"></span> ${odt.quantity} sản phẩm
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p style="color:red;"><fmt:formatNumber value="${odt.price * odt.quantity}" type="currency"/></p>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            <tr>
                                                 <td><hp><b>Phương thức thanh toán</b></hp></td>
                                                 <td><p>Trả tiền mặt khi nhận hàng</p></td>
                                            </tr>
                                            <tr>
                                                 <td><h4><b>Tổng tiền</b></h4></td>
                                                 <td><p style="color:red;"><b>${order.totalPriceFormatted}</b></p></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>
    <script>
//        $(document).ready(function(){
//            $("#submit").click(function(){
////                
//            });
//        });
    </script>
</body>

