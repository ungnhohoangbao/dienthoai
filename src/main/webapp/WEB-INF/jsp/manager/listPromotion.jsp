<%-- 
    Document   : listPromotion
    Created on : Dec 17, 2019, 3:13:23 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
            <h1>Khuyến mãi!</h1>
            <div class="row">
                <div class="table-responsive">
                            <table class="table table-bordered">
                    <tr class="bg-success">
                        
                        <th scope="col">Tên khuyến mãi</th>
                        
                        
                        <th scope="col">Ngày bắt đầu</th>
                        <th scope="col">Ngày kết thúc</th>
                        <th scope="col">Giảm giá</th>
                        
                       
                        <th scope="col">Action</th>

                    </tr>
               
                <c:forEach items="${promotions}" var="p" >
                    <tr>
                    
                        <td><c:out value="${p.name}"/></td>
                        
                        
                        <td><fmt:formatDate pattern = "dd-MM-yyyy" 
                                                        value='${p.startDate}'/></td>
                        <td><fmt:formatDate pattern = "dd-MM-yyyy" 
                                                        value='${p.endDate}'/></td>
                        <td><c:out value="${p.discount}"/> %</td>
                        <td>
   
                        <button class="btn btn-success glyphicon glyphicon-wrench" 
                           onclick="location.href = '<c:url value="/manager/edit-promotion/${p.id}"/>'">
                       
                   </button>
                     <button class="btn btn-primary" 
                           onclick="location.href = '<c:url value="/manager/promotion-product/${p.id}"/>'">
                       Thêm Sản phẩm khuyến mãi
                   </button> 
                        <button class="btn btn-warning" 
                           onclick="location.href = '<c:url value="/manager/listpromotion-product/${p.id}"/>'">
                       sản phẩm
                   </button>
                            </td>
                           
                            </tr>
                </c:forEach>   
                   </table>  
                </div>
            </div>
            </div>
        </div>
         <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
