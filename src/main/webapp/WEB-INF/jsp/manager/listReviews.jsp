<%-- 
    Document   : listProducer
    Created on : Dec 16, 2019, 4:55:32 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px">
             <div class="row">
                        <div class="col-sm-12 col-lg-12" style="text-align: center">
                            <h2>Danh sách bình luận</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            
                        </div>
                        <div class="col-sm-6" style="text-align: right; padding-bottom: 10px;">
                             
                            <mvc:form action="${pageContext.request.contextPath}/manager/search-Review" class="form-inline"
                          method="post">
                        <div class="form-group">
                            <input name="searchText" class="form-control" placeholder="Search..."/>
                               <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                        </div>    
                    </mvc:form>
                        </div>
                    </div>
            <div class="row">
                <div class="table-responsive">
                            <table class="table table-bordered">
                    <tr class="bg-success">
                        
                        <th scope="col">Bình luận</th>
                        <th scope="col" class="list-review">Ngày</th>
                       <th scope="col" class="list-review">Sản phẩm</th>
                       <th scope="col" class="list-review">Tên tài khoản</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                <c:forEach items="${reviews}" var="r" >
                    <tr>
                        <td><c:out value="${r.content}"/></td>
                        <td><c:out value="${r.reviewDate}"/></td>
                        <td><c:out value="${r.product.name}"/></td>
                        <td><c:out value="${r.account.name}"/></td>
                        <td>
                            <button class="btn btn-danger glyphicon glyphicon-trash" 
                           onclick="location.href = '<c:url value="/manager/delete-Review/${r.id}"/>'">
                            </button>
                        </td>
                    </tr>
                </c:forEach>   
                   </table>   
                </div>
            </div>
            </div>
               
            
              
             
        </div>
         <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>

