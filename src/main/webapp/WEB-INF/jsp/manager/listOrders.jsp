f<%-- 
    Document   : listProducer
    Created on : Dec 16, 2019, 4:55:32 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Danh sách đơn hàng</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid">
                    <h1>Danh sách đơn hàng!</h1>
                    <div class="row">
                        <div class="col-sm-12" style="margin-bottom: 10px;">
                            <button type="button" class="btn btn-info"
                                    onclick="location.href = '${pageContext.request.contextPath}/manager/dowload-order'">Xuất file excel</button>
                        </div>
                        <div class="col-sm-5">
                            <mvc:form action="${pageContext.request.contextPath}/manager/listOrder-Done" class="form-inline">
                                <select name="SearchStatusOrder" class="form-control">
                                    <c:forEach items="${statusOrder}" var="s" >
                                            <option value="${s}">${s.statusOrder}</option>
                                    </c:forEach>
                                </select>
                                <button class="btn btn-primary" type="submit">
                                   <i class="fa fa-search"></i>
                                </button>

                            </mvc:form>
                        </div>
                        <div class="col-sm-7" 
                             style="text-align: right; margin-bottom: 10px">
                            <mvc:form action="${pageContext.request.contextPath}/manager/order-Search" class="form-inline">

                                <div class="form-group">
                                    <input type="date" class="form-control" name="startDate" value="${startDate}" placeholder="bắt đầu...">
                                    <input type="date" class="form-control" name="endDate" value="${endDate}" placeholder="kết thúc...">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </mvc:form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr class="bg-success">
                                    <th scope="col">Ngày đặt hàng</th>
                                    <th scope="col">Tên khách hàng</th>
                                    <th scope="col">Tổng tiền</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                <c:forEach items="${orders}" var="o" >
                                    <tr>
                                        <td><fmt:formatDate pattern = "dd-MM-yyyy" 
                                                        value='${o.orderDate}' /></td>

                                        <td><c:out value="${o.customer.name}"/></td>
                                        <td><c:out value="${o.totalPriceOrder()}"/></td>
                                        <td style="width: 220px;">
                                            <c:choose>
                                                <c:when test="${o.status == 'Done'}">
                                                    <p style="font-size: 20px; color: #007fff;margin-top: 5px; margin-left: 50px;">${o.status.statusOrder}</p>
                                                </c:when>
                                                <c:when test="${o.status == 'Cancel'}">
                                                    <p style="font-size: 20px; color: #e5101d;margin-top: 5px; margin-left: 50px;">${o.status.statusOrder}</p>
                                                </c:when>
                                                <c:otherwise>
                                                    <mvc:form action="${pageContext.request.contextPath}/manager/update-statusOrder/${o.id}" method="POST">
                                                        <div class="col-sm-9 col-lg-9">
                                                            <select name="status" class="form-control">
                                                                <c:forEach items="${statusOrder}" var="s" >
                                                                    <c:if test="${s == o.status}">
                                                                        <option value="${s}" selected>${s.statusOrder}</option>
                                                                    </c:if>
                                                                    <c:if test="${s!= o.status}">
                                                                        <option value="${s}">${s.statusOrder}</option>
                                                                    </c:if>
                                                                </c:forEach>
                                                            </select>
                                                        </div>
                                                        <div class="form-group" style="text-align: left">
                                                            <button type="submit" class="btn btn-warning glyphicon glyphicon-repeat"></button>
                                                        </div>
                                                    </mvc:form>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td style="width: 150px;">
                                            <button type="button" class="btn btn-success" style="margin-left: 30px; margin-top: 5px"
                                                    onclick="location.href = '${pageContext.request.contextPath}/manager/order-Detail/${o.id}'">Chi tiết</button>
                                        </td>
                                    </tr>
                                </c:forEach>   
                            </table>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>

