<%-- 
    Document   : productDetail
    Created on : Dec 16, 2019, 2:32:20 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>

    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12" style="text-align: center">
                            <h2>Thêm Bộ nhớ</h2>
                        </div>
                    </div>

                    <div class="row">
                        <mvc:form action="${pageContext.request.contextPath}/manager/${action}" method="post" 
                                  modelAttribute="product">
                            <c:if test="${action == 'addProductDetail'}">
                                <input hidden name="id" value="${product.id}" />
                            </c:if>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label for="memoryId">Bộ nhớ</label>
                                    <select name="memory" class="form-control" id="memoryId">
                                        <c:forEach items="${memorys}" var="m">
                                            <option value="${m.memory}">${m.memory}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label for="priceId">Giá</label>
                                    <input id="priceId" name="price" class="form-control" required
                                           value="" placeholder="Gía sản phẩm....."/>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="form-group">
                                    <label for="quantityInStoreId">Số lượng trong cửa hàng</label>
                                    <input id="quantityInStoreId" name="quantityInStore" required class="form-control" value="" placeholder="Nhập số lượng....."/>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label>
                                        Color
                                    </label>
                                    <div class="checkbox">
                                        <c:forEach items="${colors}" var="c">
                                            <label>
                                                <input type="checkbox"  name="colors" value="${c.color}" style="margin-left: -15px"/>
                                                <div class="col-sm-1" style=" width: 60px; height: 30px; box-shadow: 3px 3px #666; background:<c:out value="${c.color}"/>">
                                                </div>
                                            </label>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" style="text-align: center">
                                <button type="submit" class="btn btn-primary">Thêm</button>
                            </div>
                        </mvc:form>
                    </div> 
                    <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr class="bg-success">
                                        <th scope="col">Image</th>
                                        <th scope="col">QuantityInStore</th>
                                        <th scope="col">Memory</th>
                                        <th scope="col">Color</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">&nbsp;</th>
                                    </tr>

                                    <c:forEach items="${productDetails}" var="p" >
                                        <tr>
                                            <td>
                                                <div class="col-sm-5 col-lg-5">
                                                    <image src="<c:url value="/resources/images/product_detail/${p.image}"/>" 
                                                       style="width: 120px; height: 120px"/>
                                                </div>
                                                <div class="col-sm-7 col-lg-7">
                                                    <mvc:form action="${pageContext.request.contextPath}/manager/update-ImagePd/${p.id}" method="post" 
                                                              enctype="multipart/form-data"
                                                              class="form-horizontal">
                                                        <div class="form-group">
                                                            <input name="files" type="file" class="form-control" multiple="multiple"/>
                                                        </div>
                                                        <div class="form-group" style="text-align: left">
                                                            <button type="submit" class="btn btn-success">update</button>
                                                        </div>
                                                    </mvc:form> 
                                                </div>
                                            </td>
                                            <td><c:out value="${p.quantityInStore}"/></td>
                                            <td><c:out value="${p.memory.memory}"/></td>
                                            <td><div class="col-sm-1" style=" width: 60px; height: 30px; box-shadow: 3px 3px #666; background:<c:out value="${p.color.color}"/>">
                                                </div></td>
                                            <td><c:out value="${p.getPriceFormatted()}"/></td>
                                            <td>

                                                <button class="btn btn-danger glyphicon glyphicon-trash" 
                                                        onclick="location.href = '<c:url value="/manager/deleteProductDetail/${p.id}"/>'">

                                                </button>
                                            </td>

                                        </tr>
                                    </c:forEach>   
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
