<%-- 
    Document   : form-color
    Created on : Dec 19, 2019, 10:00:06 AM
    Author     : Hi_XV
--%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
             
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
                    <h2>Màu</h2>
            <div class="row">
                <mvc:form action="${pageContext.request.contextPath}/manager/save-Color" method="post" 
                          enctype="multipart/form-data"
                          modelAttribute="color" class="form-horizontal">
                    <div class="form-group">
                        <label for="nameId" class="control-label col-sm-2">
                            Màu
                        </label>
                        <div class="col-sm-3">
                            <input id="nameId" name="color" class="form-control" value=""/>

                        </div>
                    
                        <button type="submit" class="btn btn-primary">Thêm màu</button>
                    </div>
                </mvc:form>
            </div>
                    <div class="row">
                        <h2>Màu hiện có</h2>
                        <c:forEach items="${colors}" var="c">
                            <div class="col-sm-1" style=" width: 60px; height: 30px; border-width: 3px; border-style: solid; background:<c:out value="${c.color}"/>">
                            </div>
                            
                        </c:forEach>
                    </div>
        </div>
                </div>
            </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
