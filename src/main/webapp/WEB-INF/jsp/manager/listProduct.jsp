<%-- 
    Document   : viewProduct
    Created on : Nov 8, 2019, 1:02:07 PM
    Author     : Hi_XV
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%> 
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12" style="text-align: center">
                            <h2>Danh sách sản phẩm</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            
                        </div>
                        <div class="col-sm-6" 
                             style="text-align: right; margin-bottom: 10px">
                             <mvc:form action="${pageContext.request.contextPath}/manager/search-Product" class="form-inline">
                                 
                                 <div class="form-group">
                                     <input type="text" class="form-control" name="searchText" placeholder="Search...">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                            </mvc:form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr class="bg-success">
                                    
                                    <th scope="col">Tên sản phẩm</th>
                                    <th scope="col">Hình ảnh</th>
                                    <th scope="col">Thông tin sản phẩm</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>

                                </tr>

                                <c:forEach items="${products}" var="p" >
                                    <tr>
                                        
                                        <td><c:out value="${p.name}"/></td>
                                        <td>
                                            <%--<c:forEach items="${p.image}" var="i"  end="0">--%>
                                                <image src="<c:url value="/resources/images/product_detail/${p.productDetail[0].image}"/>" 
                                                       style="width: 120px; height: 120px"/>
                                            <%--</c:forEach>--%>
                                        </td>
                                        <td>
                                            <ul>
                                                <li>Screen: ${p.screen}</li>
                                                <li>OperatingSystem: ${p.operatingSystem}</li>
                                                <li>Camera truoc: ${p.rearCamera}</li>
                                                <li>Camera sau: ${p.frontCamera}</li>
                                                <li>BatteryCapacity: ${p.batteryCapacity}</li>
                                                <li>Ram: ${p.ram}</li>
                                            </ul>
                                        </td>
                                        <td>
                                            <c:if test="${p.status == '1'}">
                                                <mvc:form action="${pageContext.request.contextPath}/manager/update-Status/${p.id}" method="POST">
                                                    <input hidden name="status"  value="2"/>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-success">Còn hàng</button>
                                                    </div>
                                                </mvc:form>
                                            </c:if>
                                                    <c:if test="${p.status == '2'}">
                                                <mvc:form action="${pageContext.request.contextPath}/manager/update-Status/${p.id}" method="POST">
                                                    <input hidden name="status"  value="1"/>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-danger">Hết hàng</button>
                                                    </div>
                                                </mvc:form>
                                            </c:if>

                                        </td>
                                        <td style="width: 30%">
                                            <button class="btn btn-success" 
                                                    onclick="location.href = '<c:url value="/manager/addProducrDetail/${p.id}"/>'">
                                                Update Product Detail
                                            </button>
                                                <button class="btn btn-warning"
                                                    onclick="location.href = '<c:url value="/manager/edit-product/${p.id}"/>'">
                                                Update Product
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>   
                            </table>  
                        </div>
                    </div>
                </div>

            </div>



        </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>

    </body>
</html>
