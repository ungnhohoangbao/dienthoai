<%-- 
    Document   : productDetail
    Created on : Dec 16, 2019, 2:32:20 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>

    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
                     <div class="row">
                        <div class="col-sm-12 col-lg-12" style="text-align: center">
                            <h2>Thêm sản phẩm vào khuyến mãi</h2>
                        </div>
                    </div>
                    <div class="row">
                        <mvc:form action="${pageContext.request.contextPath}/manager/${action}" method="post" 
                                   class="form-horizontal">
                            <c:if test="${action == 'promotion-product'}">
                                <input hidden name="id" value="${promotion.id}" />

                            </c:if>

                                <div class="col-sm-12 col-lg-12" style="border-bottom: 1px solid #000">
                                <div class="form-group">
                                    <b><h3 style="color: #002a80">${promotion.name}</h3></b>
                                   <b><h4 style="color: #008000;">Thời gian khuyến mãi từ ngày ${promotion.startDate} đến ngày ${promotion.endDate}</h4></b>
                                    <b><h4 style="color: #008000">Giảm ${promotion.discount}%</h4></b>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12 col-lg-12">
                                    <h3 style="margin-left: 150px;"><b>Danh sách sản phẩm</b></h3>
                                </div>
                                    
                                
                                <div class="col-sm-12 checkbox">
                                    <c:forEach items="${products}" var="p">
                                        <div class="col-sm-3">
                                            <p style="width: 200px; background: #E6E6E6;padding: 5px; margin-top: 5px; box-shadow: 1px 1px 3px 1px  #2E2E2E;"><label class="container"><input type="checkbox" name="ListProductId" value="${p.id}"/><b>${p.name}</b></p><span class="checkmark"></span></label>
                                        </div>
                                           
                                        
                                    </c:forEach>

                                </div>
                                </div>
                            
                            <div class="form-group" style="text-align: center">
                                <button type="submit" class="btn btn-primary">Thêm sản phẩm</button>
                            </div>
                        </mvc:form>
                    </div> 
                  
                </div>

            </div>
        </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
