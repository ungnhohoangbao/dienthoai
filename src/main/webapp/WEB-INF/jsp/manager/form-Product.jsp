<%-- 
    Document   : form-product
    Created on : Dec 15, 2019, 9:42:11 AM
    Author     : Hi_XV
--%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
        <style>
            .required{
                color: red;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 30px">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12" style="text-align: center">
                            <h2>Cập nhập sản phẩm</h2>
                        </div>
                    </div>
                    <div class="row">
                        <c:if test="${action == 'edit-product'}">
                            <div class="col-sm-7 col-lg-7">
                                <mvc:form action="${pageContext.request.contextPath}/manager/${action}" method="post" 
                                          enctype="multipart/form-data"
                                          modelAttribute="product">
                                    <input hidden name="id" value="${product.id}" />
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="nameId">Tên sản phẩm</label>
                                            <input id="nameId" name="name" class="form-control" value="${product.name}" required placeholder="Nhập tên sản phẩm....."/>
                                            <mvc:errors path="name" cssClass="required" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="screenId">Kích thước màn hình</label>
                                            <input id="screenId" name="screen" class="form-control" value="${product.screen}" required placeholder="Nhập kích thước nàm hình....."/>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="descriptionId">Mô tả sản phẩm</label>
                                            <textarea id="descriptionId" name="content" 
                                                      class="form-control" required placeholder="Giới thiệu sản phẩm......">${product.content}</textarea>
                                                      <mvc:errors path="content" cssClass="required" />
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="urlImageId">
                                                Hệ điều hành
                                            </label>

                                            <input id="urlImageId" name="operatingSystem" class="form-control" value="${product.operatingSystem}" required placeholder="Nhập "/>

                                        </div>
                                    </div>
                                             <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="urlImageId" class="control-label">
                                                Dung lượng Pin
                                            </label>

                                            <input id="urlImageId" name="batteryCapacity" class="form-control" value="${product.batteryCapacity}" required placeholder="Nhập dung lượng Pin........"/>

                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="urlImageId" class="control-label">
                                                Camera trước
                                            </label>

                                            <input id="urlImageId" name="rearCamera" class="form-control" value="${product.rearCamera}" required placeholder="Nhập camera trước......"/>

                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="urlImageId" class="control-label">
                                                Camera sau
                                            </label>

                                            <input id="urlImageId" name="frontCamera" class="form-control" value="${product.frontCamera}" required placeholder="Nhập camera sau......"/>

                                        </div>
                                    </div>
                                   
                                    <div class="col-sm-6 col-lg-6"> 
                                        <div class="form-group">
                                            <label for="urlImageId" class="control-label">
                                                Ram
                                            </label>

                                            <input id="urlImageId" name="ram" class="form-control" value="${product.ram}" required placeholder="Nhập dung lượng Ram...."/>

                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Nhà sản xuất
                                            </label>

                                            <select name="producer.id" class="form-control">
                                                <c:forEach items="${producers}" var="c">
                                                    <c:if test="${c.id == product.producer.id}">
                                                        <option value="${c.id}" selected>${c.name}</option>
                                                    </c:if>
                                                    <c:if test="${c.id != product.producer.id}">
                                                        <option value="${c.id}">${c.name}</option>
                                                    </c:if>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>       
                                    <div class="form-group" style="text-align: center">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </mvc:form>
                            </div>
                            <div class="col-sm-5 col-lg-5">
                                <div class="col-sm-12 col-lg-12" style="text-align: center">
                                    <h4>Cập nhật hình ảnh</h4>
                                </div>
                                <div class="col-sm-12 col-lg-12">
                                    <mvc:form action="${pageContext.request.contextPath}/manager/edit-images" method="post" 
                                              enctype="multipart/form-data"
                                              modelAttribute="product">
                                        <input hidden name="id" value="${product.id}" />           
                                        <div class="col-sm-9 col-lg-9">
                                            <div class="input-group">
                                                <input name="files" type="file" class="form-control" multiple="multiple"/>
                                            </div> 
                                        </div>
                                        <div class="col-sm-3 col-lg-3">
                                            <div class="form-group" style="text-align: left">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </mvc:form>
                                </div>
                                <div class="col-sm-12 col-lg-12">
                                    <c:forEach items="${images}" var="i">
                                        <div class="col-sm-4">
                                            <img src="<c:url value="/resources/images/phone/${i.name}"/>" 
                                                 width="120px" height="80px">
                                            <button class="btn btn-danger glyphicon glyphicon-trash" 
                                                    onclick="location.href = '<c:url value="/manager/deleteImages/${i.id}"/>'"
                                                    style="margin:5px 0px 5px 0px ">
                                                
                                            </button>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </c:if>

                        <!-- add new -->
                        <c:if test="${action == 'add-product'}">
                            <mvc:form action="${pageContext.request.contextPath}/manager/${action}" method="post" 
                                      enctype="multipart/form-data"
                                      modelAttribute="product">

                                <c:if test="${action == 'edit-product'}">
                                    <input hidden name="id" value="${product.id}" />
                                </c:if>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="nameId">Tên sản phẩm</label>
                                        <input id="nameId" name="name" class="form-control" value="${product.name}" required placeholder="Nhập tên sản phẩm...."/>
                                        <mvc:errors path="name" cssClass="required" />
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="screenId">Kích thước màn hình</label>
                                        <input id="screenId" name="screen" class="form-control" value="${product.screen}" required placeholder="Nhập kích thuốc màn hình......"/>
                                        <mvc:errors path="screen" cssClass="required" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-12">
                                    <div class="form-group">
                                        <label for="descriptionId">Mô tả sản phẩm</label>
                                        <textarea id="descriptionId" name="content" 
                                                  class="form-control" required placeholder="Nhập gới thiệu về sản phẩm....">${product.content}</textarea>
                                                  <mvc:errors path="content" cssClass="required" />
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="urlImageId">
                                            Hệ điều hành
                                        </label>

                                        <input id="urlImageId" name="operatingSystem" class="form-control" value="${product.operatingSystem}" required placeholder="Nhập hệ điều hành....."/>
                                        <mvc:errors path="operatingSystem" cssClass="required" />
                                    </div>
                                </div>
                                 <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="urlImageId" class="control-label">
                                            Dung lượng Pin
                                        </label>

                                        <input id="urlImageId" name="batteryCapacity" class="form-control" value="${product.batteryCapacity}" required placeholder="Nhập dung lượng Pin...."/>
                                        <mvc:errors path="batteryCapacity" cssClass="required" />
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="urlImageId" class="control-label">
                                            Camera trước
                                        </label>

                                        <input id="urlImageId" name="rearCamera" class="form-control" value="${product.rearCamera}"required placeholder="Nhập camera trước....."/>

                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="urlImageId" class="control-label">
                                            Camera sau
                                        </label>

                                        <input id="urlImageId" name="frontCamera" class="form-control" value="${product.frontCamera}" required placeholder="Nhập camera sau....."/>

                                    </div>
                                </div>
                               
                                <div class="col-sm-6 col-lg-6"> 
                                    <div class="form-group">
                                        <label for="urlImageId" class="control-label">
                                            Ram
                                        </label>

                                        <input id="urlImageId" name="ram" class="form-control" value="${product.ram}" required placeholder="Nhập dung lượng Ram......"/>

                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <label class="control-label">
                                            Nhà sản xuất
                                        </label>

                                        <select name="producer.id" class="form-control">
                                            <c:forEach items="${producers}" var="c">
                                                <c:if test="${c.id == product.producer.id}">
                                                    <option value="${c.id}" selected>${c.name}</option>
                                                </c:if>
                                                <c:if test="${c.id != product.producer.id}">
                                                    <option value="${c.id}">${c.name}</option>
                                                </c:if>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>       
                                <div class="col-sm-12 col-lg-12">
                                    <div class="input-group">
                                        <label for="imageId" class="control-label">
                                            Hình ảnh sản phẩm
                                        </label>
                                        <input id="imageId" name="files" type="file" class="form-control" multiple="multiple"/>
                                    </div> 
                                </div> 
                                <div class="form-group" style="text-align: center">
                                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                                </div>
                            </mvc:form>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
