<%-- 
    Document   : listProducer
    Created on : Dec 16, 2019, 4:55:32 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
            <h1>Chi tiết đơn hàng!</h1>
            <div class="row">
               <div class="table-responsive">
                            <table class="table table-bordered">
                    <tr class="bg-success">
                        <th scope="col">Tên sản phẩm</th>
                        <th scope="col">Hình Ảnh</th>
                        <th scope="col">Màu sắc</th>
                        <th scope="col">Bộ nhớ</th>
                        <th scope="col">Đơn giá</th>
                        <th scope="col">Số lượng</th>
                        <th scope="col">Thành tiền</th>
                        <th scope="col">&nbsp;</th>

                    </tr>
               
                <c:forEach items="${orderDetails}" var="od" >
                    <tr>
                    <td><c:out value="${od.product.name}"/></td>
                    <td>
                          <c:forEach items="${od.product.productDetail}" var="pd">
                            <c:if test="${od.color == pd.color.color && od.memory == pd.memory.memory}">
                                <image src="<c:url value="/resources/images/product_detail/${pd.image}"/>" 
                                                       style="width: 120px; height: 120px"/>
                            </c:if>
                             
                        </c:forEach>
                    </td>
                        
                        <td> <div class="col-sm-1" style=" width: 60px; height: 30px; box-shadow: 3px 3px #666; background:<c:out value="${od.color}"/>">
                                                </div>
                        </td>
                        <td><c:out value="${od.memory}"/></td>
                        <td><c:out value="${od.getPriceFormatted()}"/></td>
                        <td><c:out value="${od.quantity}"/></td>
                        <td><c:out value="${od.getTotalPriceFormatted()}"/></td>
                           
                           
                            </tr>
                </c:forEach>   
                   </table> 
                   </div>
            </div>
            </div>
               
                </div>
            
              
             
        </div>
         <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>

