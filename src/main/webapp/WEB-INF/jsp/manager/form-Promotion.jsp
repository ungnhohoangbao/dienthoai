<%-- 
    Document   : form-Promotion
    Created on : Dec 17, 2019, 3:03:05 PM
    Author     : Hi_XV
--%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
        <style>
            .requied{
                color: red;
            }
        </style>
    </head>

    <body>
        <div id="wrapper">

            <jsp:include page="../include/manages/header-area-manages.jsp"/>

            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12" style="text-align: center">
                            <h2>Cập nhật khuyến mãi</h2>
                        </div>
                    </div>
                    <div class="row">
                        <mvc:form action="${pageContext.request.contextPath}/manager/${action}" method="post" 
                                  enctype="multipart/form-data"
                                  modelAttribute="promotion">

                            <c:if test="${action == 'edit-promotion'}">
                                <input hidden name="id" value="${promotion.id}" />

                            </c:if>
                                <div class="col-sm-12" style="padding:0px;">
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label for="nameId">
                                        Tên khuyến mãi 
                                    </label>                                    
                                    <input id="nameId" name="name" required class="form-control" value="${promotion.name}" placeholder="Nhập tên khuyến mãi......"/>
                                    <mvc:errors path="name" cssClass="requied" />

                                </div>
                            </div>
                             <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label for="nameId">
                                        Giảm giá
                                    </label>

                                    <input id="nameId" name="discount" required class="form-control" value="${promotion.discount}" placeholder="Nhập % giảm giá......."/>
                                <mvc:errors path="discount" cssClass="requied" />
                                </div>
                            </div> 
                                </div>  
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label for="nameId">
                                        Ngày bắt đầu
                                    </label>
                                    <input id="nameId" name="startDate" required type="date" class="form-control" value="${promotion.startDate}"/>
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">        
                                <div class="form-group">
                                    <label for="nameId">
                                        Ngày kết thúc
                                    </label>

                                    <input id="nameId" name="endDate" required type="date" class="form-control" value="${promotion.endDate}"/>
                                </div>
                            </div>
                           
                                 
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label for="descriptionId">
                                        Miêu tả khuyến mãi
                                    </label>

                                    <textarea id="descriptionId" required name="description" 
                                              class="form-control" placeholder="Nhập giới thiệu về khuyến mãi.....">${promotion.description}</textarea>
                                               <mvc:errors path="description" cssClass="requied" />
                                </div>
                            </div>
                           <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label for="nameId">
                                        Image 
                                    </label>


                                    <input name="files" type="file" class="form-control" multiple="multiple"/>


                                </div> 
                            </div>


                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group" style="text-align: center">
                                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                                </div>  
                            </div>

                        </mvc:form>
                    </div>


                </div>
            </div>
        </div>

        <jsp:include page="../include/manages/footer-manages.jsp"/>

    </body>
</html>
