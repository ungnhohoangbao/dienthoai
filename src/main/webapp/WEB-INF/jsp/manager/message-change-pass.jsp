
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Đổi mật khẩu</title>
  	<meta charset="utf-8">
  	<jsp:include page="../include/manages/css-header-manages.jsp" />
</head>
<body>
    <div id="wrapper">
             
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            
            <div id="page-wrapper">
                <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Đổi mật khẩu</p>
                            
                        </div>
                        <div class="col-sm-12" style="margin-bottom: 20px;">
                            <div class="login">
                                <div class="account-login">
                                    <p class="success-s" style="font-size: 30px; color: red; margin-top: 30px;">
                                        <span class="glyphicon glyphicon-ok"></span>
                                        Đổi mật khẩu thành công
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
    </div>
    <jsp:include page="../include/manages/footer-manages.jsp"/>
    <jsp:include page="../include/manages/script-footer.jsp"/>
    <script>
       
    </script>
</body>

