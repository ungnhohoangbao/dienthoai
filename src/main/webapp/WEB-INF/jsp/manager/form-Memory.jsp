<%-- 
    Document   : form-Memory
    Created on : Dec 19, 2019, 10:40:07 AM
    Author     : Hi_XV
--%>

<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
             
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
                    <h2>Bộ nhớ trong</h2>
            <div class="row">
                <mvc:form action="${pageContext.request.contextPath}/manager/save-Memory" method="post" 
                          enctype="multipart/form-data"
                          modelAttribute="memory" class="form-horizontal">
                    <div class="form-group">
                        <label for="nameId" class="control-label col-sm-2">
                            Bộ nhớ trong 
                        </label>
                        <div class="col-sm-3">
                            <input id="nameId" name="memory" class="form-control" value=""/>

                        </div>
                    
                        <button type="submit" class="btn btn-primary">Thêm bộ nhớ</button>
                    </div>
                </mvc:form>
            </div>
                    <div class="row">
                        <h4>Bộ nhớ trong hiện có</h4>
                        <c:forEach items="${memorys}" var="m">
                            <span class="color-memory"><c:out value="${m.memory}"/></span>
                        </c:forEach>
                    </div>
        </div>
                </div>
            </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>

