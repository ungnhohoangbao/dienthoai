<%-- 
    Document   : addProducer
    Created on : Dec 16, 2019, 4:14:08 PM
    Author     : Hi_XV
--%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
     <body>
        <div id="wrapper">
             
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
                    <h2>Thêm nhà sản xuất</h2>
            <div class="row">
                <mvc:form action="${pageContext.request.contextPath}/manager/${action}" method="post" 
                          enctype="multipart/form-data"
                          modelAttribute="producer" class="form-horizontal">

                    <c:if test="${action == 'edit-Producer'}">
                        <input hidden name="id" value="${producer.id}" />

                    </c:if>

                    <div class="form-group">
                        <label for="nameId" class="control-label col-sm-2">
                            Tên nhà sản xuất:
                        </label>
                        <div class="col-sm-6">
                            <input id="nameId" name="name" required class="form-control" value="${producer.name}" placeholder="Nhập tên nhà sản xuất....."/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="descriptionId" class="control-label col-sm-2">
                            Mô tả:
                        </label>
                        <div class="col-sm-6">
                        <textarea id="descriptionId" name="description" required
                                  class="form-control" placeholder="Nhập giới thiệu về nhà sản xuất.....">${producer.description}</textarea>
                                          </div>
                    </div>
                    <div class="form-group" style="margin-left: 400px;">
                        <button type="submit" class="btn btn-primary">Hoàn thành</button>
                    </div>
                </mvc:form>
            </div>
        </div>
                </div>
            </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
