<%-- 
    Document   : listPromotion
    Created on : Dec 17, 2019, 3:13:23 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
            <h1>Khuyến mãi!</h1>
            <div class="row">
                <table class="table">
                    <tr class="bg-success">
                       
                        <th scope="col">Tên sản phẩm</th>
                        <th scope="col">Ảnh</th>
                        <th scope="col">Giá hiện tại</th>
                        <th scope="col">Giá khuyến mãi</th>

                        
                       
                        <th scope="col">Action</th>

                    </tr>
               
                <c:forEach items="${productPromotion}" var="p" >
                    <tr>
                   
                        <td><c:out value="${p.name}"/></td>
                        <td>
                             <image src="<c:url value="/resources/images/product_detail/${p.productDetail[0].image}"/>" 
                                                       style="width: 120px; height: 120px"/>
                        </td>
                        <td>
                            <ul>
                                <c:forEach items="${p.productDetail}" var="pd">
                                    <li>  <div class="col-sm-1" style=" margin-right: 5px; width: 60px; height: 15px; box-shadow: 3px 3px #666; background:<c:out value="${pd.color.color}"/>">
                                        </div>  ${pd.memory.memory} ${pd.getPriceFormatted()}</li>
                                            </c:forEach>
                            </ul>
                             
                        </td>
                        <td>
                            <ul>
                                <c:forEach items="${p.productDetail}" var="pd">
                                    <li>  <div class="col-sm-1" style="  margin-right: 5px; width: 60px; height: 15px; box-shadow: 3px 3px #666; background:<c:out value="${pd.color.color}"/>">
                                        </div>  ${pd.memory.memory} ${pd.getDiscount()}</li>
                                            </c:forEach>
                            </ul>
                        </td>
                        <td>
                             <button class="btn btn-warning" 
                           onclick="location.href = '<c:url value="/manager/delete-productPromotion/${p.id}/${promotionId}"/>'">
                       Xóa
                   </button>
                        </td>
                            </tr>
                </c:forEach>   
                   </table>    
            </div>
            </div>
               
            
              
             
        </div>
         <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
