<%-- 
    Document   : listProducer
    Created on : Dec 16, 2019, 4:55:32 PM
    Author     : Hi_XV
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>
            <div id="page-wrapper">
                <div class="container-fluid" style="margin-top: 20px;">
                    <h1>Nhà sản xuất!</h1>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr class="bg-success">
                                    <th scope="col">Tên nhà sản xuất</th>
                                    <th scope="col">Miêu tả</th>
                                    <th scope="col">Action</th>
                                </tr>
                                <c:forEach items="${producers}" var="p" >
                                    <tr>
                                        <td><c:out value="${p.name}"/></td>
                                        <td><c:out value="${p.description}"/></td>
                                        <td>
                                            <button class="btn btn-warning glyphicon glyphicon-wrench" 
                                                    onclick="location.href = '<c:url value="/manager/edit-Producer/${p.id}"/>'">
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>   
                            </table>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
