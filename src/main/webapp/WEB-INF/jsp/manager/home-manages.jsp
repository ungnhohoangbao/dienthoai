<%-- 
    Document   : home-manages
    Created on : Dec 15, 2019, 10:58:32 AM
    Author     : Hi_XV
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <jsp:include page="../include/manages/css-header-manages.jsp"/>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="../include/manages/header-area-manages.jsp"/>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Manage</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-comments fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">${sumReview}</div>
                                            <div>Bình luận!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="${pageContext.request.contextPath}/manager/list-Review">
                                    <div class="panel-footer">
                                        <span class="pull-left">Hiển thị chi tiết</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-tasks fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">${sumPromotion}</div>
                                            <div>Khuyến mãi!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="${pageContext.request.contextPath}/manager/homeListPromotion">
                                    <div class="panel-footer">
                                        <span class="pull-left">Hiển thị chi tiết</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-shopping-cart fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">${sumOrder}</div>
                                            <div>Đơn hàng!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="${pageContext.request.contextPath}/manager/list-orders">
                                    <div class="panel-footer">
                                        <span class="pull-left">Hiển thị chi tiết</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-support fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><span style="font-size: 17px;">${sumOrderPriceDone}</span></div>
                                            <div>Doanh thu!</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="${pageContext.request.contextPath}/manager/list-orders">
                                    <div class="panel-footer">
                                        <span class="pull-left">Hiển thị chi tiết</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>

                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="../include/manages/footer-manages.jsp"/>
    </body>
</html>
