
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Đăng ký thành viên</title>
  	<meta charset="utf-8">
  	<jsp:include page="include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="include/user/header-area.jsp" />
    
    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Đăng ký thành viên</p>
                            
                        </div>
                        <div class="col-sm-12" style="margin-bottom: 20px;">
                            <div class="login">
                                <div class="account-login">
                                    <h3 style="text-align: center;color: red;">
                                        Đăng ký thành viên
                                    </h3>
                                    <form action="" class="login-form form-horizontal" method="POST"
                                        modelAttribute="account"  >
                                        <c:if test="${mess != null}">
                                            <p class="already_exist">
                                                <span class="glyphicon glyphicon-remove"></span>
                                                ${mess}
                                            </p>
                                        </c:if>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="email">Email:</label>
                                             <div class="col-sm-8">
                                                 <input id="email" type="email" name="email" placeholder="Nhập email" 
                                                   class="form-control" required value="${account.email}">
                                             </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="fullName">Họ tên:</label>
                                            <div class="col-sm-8">
                                                <input id="fullName" type="text" name="name" placeholder="Nhập họ tên" 
                                                   class="form-control" required value="${account.name}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="password">Mật khẩu:</label>
                                            <div class="col-sm-8">
                                                <input id="password" type="password" name="password" placeholder="Nhập mật khẩu"  
                                                   class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="againPassword">Nhập lại mật khẩu:</label>
                                            <div class="col-sm-8">
                                                <input id="againPassword" type="password" name="againPassword" placeholder="Nhập lại mật khẩu"  
                                                   class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-login">Đăng ký</button>
                                            </div>
                                        </div>  
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>
    <script>
 
    </script>
</body>

