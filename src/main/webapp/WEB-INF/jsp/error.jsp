<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home</title>
        <meta charset="utf-8">
        <jsp:include page="include/user/css-header.jsp" />
    </head>
    <body>
        <jsp:include page="include/user/header-area.jsp" />
        <div style="margin:10px;">
            <div class="container" >
                <h2> Thông báo lỗi: <%=exception.getMessage() %> </h2>
            </div>
        </div>
        <div style="margin:10px;">
            
        </div>
        <div style="margin:10px;">
            
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>
</body>
</html>