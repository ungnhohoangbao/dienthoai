
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Thông báo</title>
  	<meta charset="utf-8">
  	<jsp:include page="include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="include/user/header-area.jsp" />
    
    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Thông báo</p>
                        </div>
                        <div class="col-sm-12"  style="margin-bottom: 20px;">
                            <div class="login" style=" min-height: 200px;">
                                <div class="account-login">
                                    <p class="success-s">
                                        <span class="glyphicon glyphicon-ok"></span>
                                        Bạn đã mua hàng thành công tại shop, 
                                        vui lòng kiểm tra email của bạn để xem thông tin đơn hàng.
                                    </p>
                                    <button type="button" class="btn btn-danger" style="margin-left:180px;"
                                        onclick="window.location.href='<c:url value="/home" />'">
                                        Tiếp tục mua sắm
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>
    <script>
       
    </script>
</body>

