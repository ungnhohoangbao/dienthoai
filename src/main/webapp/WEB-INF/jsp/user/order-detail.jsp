
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Chi tiết đơn hàng</title>
    <meta charset="utf-8">
    <jsp:include page="../include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="../include/user/header-area.jsp" />

    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Chi tiết đơn hàng</p>
                            <h3 style="text-align: center;color: red;">
                                  Chi tiết đơn hàng
                            </h3>
                        </div>
                        <div class="col-sm-12" style="margin-bottom: 20px;">
                            <div class="thanht col-sm-12">
                                <div class="div-tt">
                                    <h4 style="text-align: center;">
                                        Thông tin
                                    </h4>
                                    <div class="login-form form-horizontal lo-right" >
                                        <input type="hidden" id="id_order" value="${order1.id}" />
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Mã đơn hàng: </b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>${order1.id}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Ngày đặt: </b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>${order1.orderDate}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Tổng tiền</b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>${order1.totalPriceFormatted}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Email:</b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>${order1.customer.email}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Họ Tên: </b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>${order1.customer.name}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Số điện thoại: </b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>${order1.customer.phoneNumber}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Trạng thái: </b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p id="status_od">${order1.status.statusOrder}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Địa chỉ nhận hàng</b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>${order1.customer.address}</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <p><b>Phương thức thanh toán</b></p>
                                            </div>
                                            <div class="col-sm-8">
                                                <p>Trả tiền mặt khi nhận hàng</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="div-tt">
                                    <h4 style="text-align: center;">
                                        Sản phẩm
                                    </h4>
                                    <table class="table table-hover login-form" style="margin-left: 40px;">
                                        <thead>
                                            <tr>
                                                <th>Sản phẩm</th>
                                                <th>Đơn giá</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="odt" items="${order1.orderDetails}">
                                                <tr>
                                                    <td>
                                                        <p>${odt.product.name} Ram ${odt.product.ram} / ${odt.memory}</p>
                                                        <p>
                                                            Màu <span class="color-tt" style="background: ${odt.color}"></span>
                                                            <span class="glyphicon glyphicon-remove" style="margin-left: 10px;"></span> ${odt.quantity} sản phẩm
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p style="color:red;"><fmt:formatNumber value="${odt.price * odt.quantity}" type="currency"/></p>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-12" style="margin-bottom: 10px;">
                                <button type="button" class="btn btn-success"
                                        onclick="window.history.go(-1)">Quay lại
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../include/user/footer.jsp"/>
    <jsp:include page="../include/user/script-footer.jsp"/>
    <script>
        $(document).ready(function(){
            $(".cancel-order").click(function(){
                var id = $("#id_order").val();
                let $this = $(this);
                $.ajax({
                    type : "GET",
                    url: "<c:url value="/user/cancle-order/" />"+id,
                    dataType : 'json',
                    timeout : 100000,
                    success :function (data){  
                        let status = JSON.parse(data);
                        if(status.status == "ok"){
                            $("#status_od").text("Đã hủy");
                            $this.remove();
                        }
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
            });
        });
    </script>
</body>

