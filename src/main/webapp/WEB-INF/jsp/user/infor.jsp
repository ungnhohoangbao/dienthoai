
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Thông tin cá nhân</title>
  	<meta charset="utf-8">
  	<jsp:include page="../include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="../include/user/header-area.jsp" />
    
    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content" >
                            <p>Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Thông tin cá nhân</p>
                            
                        </div>
                        <div class="col-sm-12" style="margin-bottom: 20px;">
                            <div class="login">
                                <div class="account-login">
                                    <h3 style="text-align: center;color: red;">
                                        Thông tin cá nhân
                                    </h3>
                                    <form action="${pageContext.request.contextPath}/user/update-infor" 
                                          class="login-form form-horizontal" method="POST"
                                          modelAttribute="account" class="form-update">
                                        <c:if test="${mess == 'success'}">
                                            <p class="success-s">
                                                <span class="glyphicon glyphicon-ok"></span>
                                                Cập nhật thông tin thành công
                                            </p>
                                        </c:if>
                                        <c:if test="${mess == 'already_exist'}">
                                            <p class="already_exist">
                                                <span class="glyphicon glyphicon-remove"></span>
                                                Email đã tồn tại
                                            </p>
                                        </c:if>
                                        <div class="form-group" id="top-u">
                                            <label class="control-label col-sm-4" for="email">Email:</label>
                                             <div class="col-sm-8">
                                                 <input id="email" type="email" name="email" placeholder="Nhập email" 
                                                   class="form-control" required value="${account.email}">
                                             </div>
                                             <mvc:errors path="email" cssClass="requied" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="fullName">Họ tên:</label>
                                            <div class="col-sm-8">
                                                <input id="fullName" type="text" name="name" placeholder="Nhập họ tên" 
                                                   class="form-control" required value="${account.name}">
                                            </div>
                                            <mvc:errors path="name" cssClass="requied" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="phone">Số điện thoại:</label>
                                            <div class="col-sm-8">
                                                <input id="phone" type="number" name="phoneNumber" placeholder="Nhập số điện thoại" 
                                                   class="form-control" required value="${account.phoneNumber}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="birthDate">Ngày sinh:</label>
                                            <div class="col-sm-8">
                                                <input id="birthDate" type="date" name="birthDate"
                                                   class="form-control"
                                                   style="height: 25px;" value="<fmt:formatDate pattern = "yyyy-MM-dd" 
                                                        value='${account.birthDate}' />"/>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label class="control-label col-sm-4">Giới tính:</label>
                                            <div class="col-sm-8 gender">
                                                <c:forEach var="g" items="${gender}">
                                                    <label class="radio-inline">
                                                        <c:choose>
                                                            <c:when test="${g == account.gender}">
                                                                <input type="radio"  name="gender" value="${g}" 
                                                                checked="checked" />${g.gender}
                                                            </c:when>
                                                            <c:otherwise>
                                                                <input type="radio"  name="gender" value="${g}" 
                                                                />${g.gender}
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </label>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="address">Địa chỉ:</label>
                                            <div class="col-sm-8">
                                                <input id="address" type="text" name="address" placeholder="Nhập địa chỉ"  
                                                   class="form-control" required value="${account.address}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4"></label>
                                            <div class="col-sm-8">
                                                <button type="submit" class="btn btn-login">Cập nhật</button>
                                            </div>
                                        </div>  
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../include/user/footer.jsp"/>
    <jsp:include page="../include/user/script-footer.jsp"/>
    <script>
        $(document).ready(function(){
            $("#email").change(function(){
                let email = $(this).val().trim();
                $.ajax({
//                    beforeSend: function(xhrObj){
//                       xhrObj.setRequestHeader("Accept","application/json;charset=utf-8");
//                   },
                   type : "POST",
                   contentType: 'application/json;charset=utf-8',
                   url: "<c:url value="/user/check-email" />",
                   data: email,
                   dataType : 'json',
                   timeout : 100000,
                   success :function (data){ 
                       if(data == "already_exist"){
                           $(".already_exist").remove();
                           $(".success-s").remove();
                           $("#top-u").before("<p class='already_exist'>\n\
                            <span class='glyphicon glyphicon-remove'></span>Email đã tồn tại</p>");
                       }
                       else{
                           $(".already_exist").remove();
                           $(".success-s").remove();
                           $("#top-u").before("<p class='success-s'>\n\
                                <span class='glyphicon glyphicon-ok'></span>\n\
                                Tiếp tục cập nhật</p>");
                       }
                   },
                   error:function(e){
                       console.log(e);
                   }
               });
            });
        });
    </script>
</body>

