
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Đơn hàng của tôi</title>
    <meta charset="utf-8">
    <jsp:include page="../include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="../include/user/header-area.jsp" />

    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title"> 
                        <div class="col-sm-12 title-content" >
                            <p style="margin-bottom:  40px;">Trang chủ <span class="glyphicon glyphicon-chevron-right"></span> Đơn hàng của tôi</p>
                            <div>
                                <button type="button" class="btn btn-info" 
                                    onclick="location.href = '<c:url value='/user/dowload-order' />'">Xuất file excel
                                </button>
                                <div class="div-s-or">
                                    <c:choose>
                                        <c:when test="${all == 'all'}">
                                            <c:set var="url" value="user/list-order/search-order" />
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="url" value="user/list-order/${tab}/search-order" />
                                        </c:otherwise>
                                    </c:choose>
                                    <form action="${pageContext.request.contextPath}/${url}" method="POST">
                                        <label>Từ :</label>
                                        <input id="startDate" type="date" value="${startDate}" name="startDate" required/>
                                        <label style="margin-left:10px;">Đến :</label>
                                        <input id="endDate" type="date" value="${endDate}" name="endDate" required/>
                                        <button type="submit" class="btn btn-primary search-order">Tìm kiếm</button>
                                    </form>
                                </div> 
                                <div class="div-s-tab">
                                    <c:choose>
                                        <c:when test="${all == 'all'}">
                                            <a href="<c:url value="/user/list-order" />" style="color: red;">Tất cả</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="<c:url value="/user/list-order" />">Tất cả</a>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:forEach var="s" items="${listStatus}">
                                        <c:choose>
                                            <c:when test="${s == tab}">
                                                <a href="<c:url value="/user/list-order/${s}" />" style="color: red;">${s.statusOrder}</a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="<c:url value="/user/list-order/${s}" />">${s.statusOrder}</a>
                                            </c:otherwise>
                                        </c:choose>

                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="margin-bottom: 20px;min-height: 300px;">
                            <c:choose>
                                <c:when test="${fn:length(orders) == 0}">
                                    <h4 class="text-color">Không có đơn hàng nào</h4>
                                </c:when>
                                <c:otherwise>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Mã ĐH</th>
                                                <th>Ngày đặt</th>
                                                <th>Tổng tiền</th>
                                                <th>Họ tên người nhận</th>
                                                <th>Email</th>
                                                <th>Số điện thoại</th>
                                                <th>Trạng thái</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="i" begin="1" end="${fn:length(orders)}" step="1">
                                                <c:set var="j" value="${fn:length(orders)-i}" scope="page"></c:set>
                                                <tr class="tr-list-order">
                                                    <input type="hidden" class="id_order" value="${orders[j].id}" />
                                                    <td class="cur-list">
                                                        ${orders[j].id} 
                                                    </td>
                                                    <td class="cur-list">
                                                        ${orders[j].orderDateFormatted}
                                                    </td>
                                                    <td class="cur-list">${orders[j].totalPriceFormatted}</td>
                                                    <td class="cur-list">
                                                        ${orders[j].customer.name}
                                                    </td>
                                                    <td class="cur-list">
                                                        ${orders[j].customer.email}
                                                    </td>
                                                    <td class="cur-list">
                                                        ${orders[j].customer.phoneNumber}
                                                    </td>
                                                    <td class="cur-list status">
                                                        ${orders[j].status.statusOrder}
                                                    </td>
                                                    <td>
                                                        <c:if test="${orders[j].status == 'Ordered'}">
                                                            <button type="button" class="btn btn-warning cancel-od">Hủy đơn hàng</button>
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="../include/user/footer.jsp"/>
    <jsp:include page="../include/user/script-footer.jsp"/>
    <script>
        $(document).ready(function () {
            $(".cur-list").click(function () {
                var id = $(this).parent().find(".id_order").val();
                location.href = '<c:url value="/user/order-detail/" />' + id;
            });

            // Hủy đơn hàng
            $(".cancel-od").click(function(){
                var id = $(this).parents("tr").find(".id_order").val();
                let $this = $(this);
                $.ajax({
                    type : "GET",
                    url: "<c:url value="/user/cancle-order/" />"+id,
                    dataType : 'json',
                    timeout : 100000,
                    success :function (data){  
                        let status = JSON.parse(data);
                        if(status.status == "ok"){
                            $this.parents("tr").find(".status").text("Đã hủy");
                            $this.remove();
                        }
                    },
                    error:function(e){
                        console.log(e);
                    }
                });
            });
        });
    </script>
</body>
</html>

