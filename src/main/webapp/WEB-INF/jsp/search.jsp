<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kết quả tìm kiếm</title>
    <meta charset="utf-8">
    <jsp:include page="include/user/css-header.jsp" />
</head>
<body>
    <jsp:include page="include/user/header-area.jsp" />
    <div style="margin:10px;">
        <div class="container" >
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-12 pr-title">
                        <div class="col-sm-12 title-content">
                            <c:choose>
                                <c:when test="${producer.id >0}">
                                    <h4><b>${producer.name}</b></h4>
                                </c:when>
                                <c:otherwise>
                                    <h4><b>Kết quả tìm kiếm "${search}"</b></h4>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <c:if test="${listProduct != null && fn:length(listProduct) ==0 && fn:length(search) > 0}">
                            <p class="text-color"><b>Không tìm thấy sản phẩm "${search}"</b></p>
                        </c:if>
                        <c:if test="${listProduct != null && fn:length(listProduct) ==0 && fn:length(search) == 0}">
                            <p class="text-color"><b>Hiện chưa có sản phẩm trong "${producer.name}"</b></p>
                        </c:if>
                        <div style="min-height: 300px;">
                            <c:forEach var="p" items="${listProduct}">
                                <div class="items-phone" onclick="window.location.href='<c:url value="/product-detail/${p.productDetail[0].id}" />'">
                                    <div class="div-of-items-phone">
                                        <div class="phone-div" >
                                            <img src="<c:url value="/resources/images/product_detail/${p.productDetail[0].image}" />">
                                             <!--<p class="discount ">Gi?m 3000000 VN?</p>--> 
                                        </div>
                                        <p class="name-phone">${p.name} Ram <span style="white-space: nowrap;">${p.ram}</span>/ 32GB</p>
                                        <p class="name-phone-price">
                                            <c:choose>
                                                <c:when test="${fn:length(p.promotion) >0}">
                                                    <c:forEach var="pr" items="${p.promotion }" end="0">
                                                        <b class="color-price">
                                                            <fmt:formatNumber value="${p.productDetail[0].price - (pr.discount* p.productDetail[0].price)/100}" type="currency" />
                                                        </b>
                                                    </c:forEach>
                                                    <span class="price"> ${p.productDetail[0].priceFormatted}</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <b class="color-price">
                                                        ${p.productDetail[0].priceFormatted}
                                                    </b>
                                                </c:otherwise>
                                            </c:choose>
                                        </p>
                                        <c:set var="count_review" value="0" />
                                        <c:forEach var="v" items="${p.review}">
                                            <c:set var="count_review" value="${v.vote + count_review}" />
                                        </c:forEach>
                                        <c:if test="${fn:length(p.review) == 0}">
                                            <c:set var="below" value="1" />
                                        </c:if>
                                        <c:if test="${fn:length(p.review) > 0}">
                                            <c:set var="below" value="${fn:length(p.review)}" />
                                        </c:if>
                                        <p class="evaluate">
                                            <fmt:formatNumber type="number" pattern="###.#" value="${count_review /below}" />/5 
                                            <span class="glyphicon glyphicon-star"></span> ${fn:length(p.review)} đánh giá
                                        </p>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="include/user/footer.jsp"/>
    <jsp:include page="include/user/script-footer.jsp"/>
</body>
</html>