<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources-management/js/jquery.min.js"/>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<c:url value="/resources-management/js/bootstrap.min.js"/>"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<c:url value="/resources-management/js/metisMenu.min.js"/>"></script>

        <!-- Morris Charts JavaScript -->
        <script src="<c:url value="/resources-management/js/raphael.min.js"/>"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<c:url value="/resources-management/js/startmin.js"/>"></script>
