<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>   
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        

        <!-- Bootstrap Core CSS -->
        <link href="<c:url value="/resources-management/css/bootstrap.min.css"/>" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<c:url value="/resources-management/css/metisMenu.min.css"/>" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="<c:url value="/resources-management/css/timeline.css"/>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<c:url value="/resources-management/css/startmin.css"/>" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<c:url value="/resources-management/css/morris.css"/>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<c:url value="/resources-management/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">
        
        <link href="<c:url value="/resources-management/css/custom/custom.css"/>" rel="stylesheet">