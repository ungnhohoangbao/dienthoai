<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<nav class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
                <span class="nav-text">
                    <i class="fa fa-phone" aria-hidden="true"></i>  +84 35300 1666
                    <i class="fa fa-envelope" aria-hidden="true"></i> ungnhohoangbao@gmail.com</span>
            </div>
            <div class="col-sm-3 text-center">
            </div>
            <div class="col-sm-5 text-right hidden-xs" style="padding-right: 5px;">
                <ul class="sss navbar-nav navbar-right navbar-right-t">
                    <sec:authorize access="isAuthenticated()">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <sec:authentication property="principal.name" />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                 <sec:authorize access="hasRole('ROLE_USER')">
                                <li>
                                    <a href="<c:url value="/user/infor"/>">
                                        <span class="glyphicon glyphicon-user"></span> 
                                        Thông tin cá nhân
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/user/change-password"/>">
                                        <span class="glyphicon glyphicon-lock"></span> 
                                        Đổi mật khẩu
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/user/list-order" />">
                                        <span class="glyphicon glyphicon-briefcase"></span> 
                                        Đơn hàng của tôi
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/user/favorite" />">
                                        <span class="glyphicon glyphicon-heart"></span> 
                                        Sản phẩm yêu thích
                                    </a>
                                </li>
                                </sec:authorize>
                                 <sec:authorize access="hasRole('ROLE_ADMIN')">
                                    <li>
                                    <a href="<c:url value="/admin/infor"/>">
                                        <span class="glyphicon glyphicon-user"></span> 
                                        Thông tin cá nhân
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/admin/change-password"/>">
                                        <span class="glyphicon glyphicon-lock"></span> 
                                        Đổi mật khẩu
                                    </a>
                                </li>
                                    <li>
                                        <a href="<c:url value="/admin/home-admin" />">
                                            <span class="glyphicon glyphicon-briefcase"></span> 
                                            Admin
                                        </a>
                                    </li>
                                </sec:authorize>
                                 <sec:authorize access="hasRole('ROLE_MANAGER')">
                                     <li>
                                    <a href="<c:url value="/manager/infor"/>">
                                        <span class="glyphicon glyphicon-user"></span> 
                                        Thông tin cá nhân
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/manager/change-password"/>">
                                        <span class="glyphicon glyphicon-lock"></span> 
                                        Đổi mật khẩu
                                    </a>
                                </li>
                                    <li>
                                        <a href="<c:url value="/manager/homeManager" />">
                                            <span class="glyphicon glyphicon-briefcase"></span> 
                                            Manager
                                        </a>
                                    </li>
                                </sec:authorize>
                                <li>
                                    <a href="<c:url value="/logout"/>">
                                        <span class="glyphicon glyphicon-log-out"></span> Đăng xuất
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="!isAuthenticated()">
                        <li><a href="<c:url value="/register" />" class="mau-chu2">
                                <span class="glyphicon glyphicon-user"></span> Đăng ký
                            </a>
                        </li>
                        <li class="menu-top-boder-right">
                            <a href="<c:url value="/login" />" class="mau-chu2">
                                <span class="glyphicon glyphicon-log-in"></span> Đăng nhập
                            </a>
                        </li>
                    </sec:authorize>
                </ul>
            </div>
        </div>
    </div>
</nav>
<nav class="navbar menu-top">
    <div class="container-fluid">
        <div class="menu-main">
            <ul class="sss navbar-nav ">
                <a class="navbar-brand" id="thiennguyen" href="<c:url value="/home" />" ><i>HB Mobile</i></a>
            </ul>
            <div class="input-group" style="width:360px;float: left;margin:7px;z-index: 1;">
                <form action="${pageContext.request.contextPath}/search" method="POST" > 
                    <div class="row">
                        <div class="col-sm-12 dropdown-search">
                            <div class="input-group">
                                <input type="text" name="search" value="${search}" class="form-control" 
                                       placeholder="Tìm kiếm" id="txtSearch"
                                       autocomplete="off"/>
                                <div class="input-group-btn">
                                    <button class="btn btn-warning submit-search tk" type="submit"
                                            style="border-radius: 3px;">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </div>
                            </div>
                            <div class="dropdown-content">
                                <c:forEach begin="1" end="${fn:length(listSearch)}" var="i" step="1">
                                    <c:set var="j" value="${fn:length(listSearch)-i}" scope="page"></c:set>
                                    <p class="p-search"><span class="glyphicon glyphicon-repeat"></span> <span class="click-search">${listSearch[j]}</span></p>
                                </c:forEach>
                            </div>
                            <div class="dropdown-search-aj"> 
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <ul class="ssss navbar-nav ">
                <a class="navbar-brand cart" href="<c:url value="/cart" />" >
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    <span id="of-cart">
                        <c:set var="count_cart" value="0" />
                        <c:forEach var="ods" items="${order.orderDetails}">
                            <c:set var="count_cart" value="${count_cart +ods.quantity}" />
                        </c:forEach>
                        ${count_cart}
                    </span>
                </a>
            </ul>
            <ul class=" navbar-nav menu-center">
                <c:forEach var="pdc" items="${listProducer}">
                    <li><a href="<c:url value="/producer/${pdc.id}" />">${pdc.name}</a></li>
                </c:forEach>
                <!--<li><a href="#"><i class="icon-gift"></i>KHUYẾN MÃI</a></li>-->
            </ul>
        </div>
    </div>
</nav>