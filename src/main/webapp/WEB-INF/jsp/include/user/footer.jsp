<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4 class="title">Hỏi đáp</h4>
                <p style="color: #fff;">Giới thiệu về cửa hàng (Đạt Bảo)</p>
                <p style="color: #fff;">Hệ thống cửa hàng</p>
                <p style="color: #fff;">Tuyển dụng</p>
                <p style="color: #fff;">Chính sách bảo mật</p>
            </div>
            <div class="col-sm-3">
                <h4 class="title">Tài khoản của tôi</h4>
                <span class="acount-icon">          
                    <a href="#"><i class="fa fa-heart" aria-hidden="true"></i>Thông tin cá nhân</a>
                    <a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i> Đổi mật khẩu</a>
                    <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Đơn hàng của bạn</a>
                    <a href="#"><i class="fa fa-globe" aria-hidden="true"></i> Đăng xuất</a>           
                </span>
            </div>
            <div class="col-sm-3">
                <h4 class="title">Các các loại điện thoại</h4>
                <div class="category">
                    <a href="#">Xiaomi</a>
                    <a href="#">Samsung</a>
                    <a href="#">Appo</a>
                    <a href="#">Redalme</a>
                    <a href="#">Iphone</a>
                </div>
            </div>
            <div class="col-sm-3">
                <h4 class="title">Liên hệ</h4>
                <p style="color: #fff;">Gọi mua hàng: 1900.6122 (8H00 - 21H30)</p>
                <p style="color: #fff;">Gọi bảo hành: 1800.6501 (8H00 - 21H30)</p>
                <p style="color: #fff;">Gọi khiếu nại: 1800.6502 (8H00 - 21H30)</p>
            </div>
        </div>
        <hr>

        <div class="row text-center"> © 2017. Cửa hàng điện thoại HB mobile.</div>
    </div>
</footer>	