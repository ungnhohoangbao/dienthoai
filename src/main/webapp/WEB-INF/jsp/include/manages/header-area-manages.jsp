<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>    

       <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <ul class="nav navbar-nav navbar-left navbar-top-links">
                    <li><a href="${pageContext.request.contextPath}/"><i class="fa fa-home fa-fw"></i> Website</a></li>
                </ul>
                <ul class="nav navbar-right navbar-top-links">
                    <sec:authorize access="isAuthenticated()">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                             Manager   <sec:authentication property="principal.name" />
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<c:url value="/manager/infor"/>">
                                        <span class="glyphicon glyphicon-user"></span> 
                                        Thông tin cá nhân
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/manager/change-password"/>">
                                        <span class="glyphicon glyphicon-lock"></span> 
                                        Đổi mật khẩu
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/logout"/>">
                                        <span class="glyphicon glyphicon-log-out"></span> Đăng xuất
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </sec:authorize>
                </ul>
                <!-- /.navbar-top-links -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="<c:url value="/manager/homeManager" />" class="active"><i class="fa fa-dashboard fa-fw"></i>Bảng điều khiển</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Sản phẩm<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/manager/viewlistProduct">Danh sách sản phẩm</a>
                                    </li>
                                     <li>
                                        <a href="${pageContext.request.contextPath}/manager/add-product">Thêm sản phẩm</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/manager/add-Memory">Bộ nhớ trong</a>
                                    </li>
                                     <li>
                                        <a href="${pageContext.request.contextPath}/manager/add-Color">Màu sản phẩm</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                             <li>
                                <a href=""><i class="fa fa-table fa-fw"></i> Nhà sản xuất<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/manager/homeListPrducer">Danh sách nhà sản xuất</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/manager/addProducer">Thêm nhà sản xuất</a>
                                    </li>
                                </ul>
                             </li>
                             <li>
                                <a href="${pageContext.request.contextPath}/"><i class="glyphicon glyphicon-shopping-cart"></i> Đơn hàng<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/manager/list-orders">Danh sách đơn hàng</a>
                                    </li>
                                </ul>
                             </li>
                             <li>
                                <a href=""><i class="glyphicon  glyphicon-comment"></i> Bình luận<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                    <li>
                                        <a href="${pageContext.request.contextPath}/manager/list-Review">Danh sách binh luận</a>
                                    </li>
                                </ul>
                             </li>
                            <li>
                                <a href=""><i class="fa fa-gift"></i> Khuyến mãi<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                     <li>
                                        <a href="${pageContext.request.contextPath}/manager/homeListPromotion">Danh sách khuyến mãi</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/manager/add-promotion">Thêm khuyến mãi</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
   